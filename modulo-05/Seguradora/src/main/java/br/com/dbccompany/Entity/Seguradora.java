package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Seguradora.class)
public class Seguradora extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "SEGURADORA_SEQ", sequenceName = "SEGURADORA_SEQ")
    @GeneratedValue(generator = "SEGURADORA_SEQ",strategy = GenerationType.SEQUENCE)
    private long id;

    private String nome = "";

    @Column(unique = true)
    private long cnpj;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_endereco_endereco_seguradora")
    private EnderecoSeguradora enderecoSeguradora;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "seguradora_servico",
            joinColumns = {
                    @JoinColumn( name = "id_seguradora")
            },
            inverseJoinColumns = {
                    @JoinColumn( name = "id_servico")
            }
    )
    private List<Servico> servicos = new ArrayList<>();

    @OneToMany(mappedBy = "seguradora")
    private List<ServicoContratado> servicosContratados;

    // Getters And Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCnpj() {
        return cnpj;
    }

    public void setCnpj(long cnpj) {
        this.cnpj = cnpj;
    }

    public EnderecoSeguradora getEnderecoSeguradora() {
        return enderecoSeguradora;
    }

    public void setEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
        this.enderecoSeguradora = enderecoSeguradora;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }

    public void pushServicos(Servico... servicos){
        this.servicos.addAll(Arrays.asList(servicos));
    }

    public List<ServicoContratado> getServicosContratados() {
        return servicosContratados;
    }

    public void setServicosContratados(List<ServicoContratado> servicosContratados) {
        this.servicosContratados = servicosContratados;
    }

    public void pushServicosContratados(ServicoContratado... servicosContratados){
        this.servicosContratados.addAll(Arrays.asList(servicosContratados));
    }
}
