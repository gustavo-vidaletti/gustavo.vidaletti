package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.EnderecoRepository;
import br.com.dbccompany.Service.EnderecoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "api/endereco")
public class EnderecoController extends AbstractController<Endereco, EnderecoRepository, EnderecoService> {

}
