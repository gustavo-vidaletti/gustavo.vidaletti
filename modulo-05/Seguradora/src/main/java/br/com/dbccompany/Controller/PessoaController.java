package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pessoa;
import br.com.dbccompany.Repository.PessoaRepository;
import br.com.dbccompany.Service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

public class PessoaController <
        TipoPessoa extends Pessoa,
        TipoPessoaRepository extends PessoaRepository<TipoPessoa>,
        TipoPessoaService extends PessoaService<TipoPessoa, TipoPessoaRepository>
        > extends AbstractController<TipoPessoa, TipoPessoaRepository, TipoPessoaService> {

    @GetMapping(value = "/cpf/{cpf}")
    @ResponseBody
    public TipoPessoa buscarPorCpf(@PathVariable long cpf){
        return entidadeService.buscarPorCPF(cpf);
    }

    @GetMapping(value = "/email/{email}")
    @ResponseBody
    public TipoPessoa buscarPorEmail(@PathVariable String email){
        return entidadeService.buscarPorEmail(email);
    }

    @PostMapping(value = "/adicionar")
    @ResponseBody
    public TipoPessoa adicionar(@RequestBody TipoPessoa entidade){
        return entidadeService.salvar(entidade);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoPessoa editar(@PathVariable long id, @RequestBody TipoPessoa entidade){
        return entidadeService.editarPorId(id, entidade);
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public boolean remover(@PathVariable long id) {
        return entidadeService.deletarPorId(id);
    }


}
