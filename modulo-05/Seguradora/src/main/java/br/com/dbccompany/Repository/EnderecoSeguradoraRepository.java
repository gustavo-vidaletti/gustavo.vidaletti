package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.EnderecoSeguradora;
import org.springframework.data.repository.CrudRepository;

public interface EnderecoSeguradoraRepository extends CrudRepository<EnderecoSeguradora, Long> {
    EnderecoSeguradora findById(long id);
}
