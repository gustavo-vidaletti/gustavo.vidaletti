package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.ServicoContratadoRepository;
import org.springframework.stereotype.Service;

@Service
public class ServicoContratadoService extends AbstractService<ServicoContratadoRepository, ServicoContratado> {
}
