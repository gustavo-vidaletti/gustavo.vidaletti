package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.SeguradoraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeguradoraService extends AbstractService<SeguradoraRepository, Seguradora> {

    public Seguradora buscarPeloCnpj(long cnpj){
        return repository.findByCnpj(cnpj);
    }


}
