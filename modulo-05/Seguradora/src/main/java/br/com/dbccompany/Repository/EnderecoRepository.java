package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Endereco;
import org.springframework.data.repository.CrudRepository;

public interface EnderecoRepository extends CrudRepository<Endereco, Long> {
    Endereco findById(long id);

}
