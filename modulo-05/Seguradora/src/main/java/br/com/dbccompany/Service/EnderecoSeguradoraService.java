package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.EnderecoSeguradora;
import br.com.dbccompany.Repository.EnderecoSeguradoraRepository;
import org.springframework.stereotype.Service;

@Service
public class EnderecoSeguradoraService extends AbstractService<EnderecoSeguradoraRepository, EnderecoSeguradora> {
}
