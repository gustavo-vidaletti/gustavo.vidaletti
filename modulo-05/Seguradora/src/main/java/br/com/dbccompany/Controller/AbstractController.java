package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.AbstractEntity;
import br.com.dbccompany.Service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class AbstractController
        <
            Entidade extends AbstractEntity,
            EntidadeRepository extends CrudRepository<Entidade, Long>,
            EntidadeService extends AbstractService<EntidadeRepository, Entidade>
        > {

    @Autowired
    EntidadeService entidadeService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Entidade> listarTodos(){
        return entidadeService.listarTodos();
    }

    @GetMapping(value = "/{param}")
    @ResponseBody
    public Entidade buscar(@PathVariable String param){
        try {
            long id = Long.parseLong(param);
            return entidadeService.buscarPorId(id);
        } catch (NumberFormatException e){
            System.err.println("Erro ao formatar o numero");
        }
        return null;
    }

    @PostMapping(value = "/adicionar")
    @ResponseBody
    public Entidade adicionar(@RequestBody Entidade entidade){
        return entidadeService.salvar(entidade);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Entidade editar(@PathVariable long id, @RequestBody Entidade entidade){
        return entidadeService.editarPorId(id, entidade);
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public boolean remover(@PathVariable long id) {
        return entidadeService.deletarPorId(id);
    }

    //TODO: ---------------------------------------
    //      @ControllerInterface
    //      @RequestMapping("/api/entidade")
    //          EntidadeController{}
    //      ---------------------------------------
    //      @Autowired
    //          EntidadeService entidadeService
    //      ---------------------------------------
    //      @GetMapping(value = "/")
    //      @ResponseBody
    //          listar todos
    //      ---------------------------------------
    //      @GetMapping(value = "/{param}")
    //      @ResponseBody
    //          buscar(@PathVariable long id)
    //          buscar por outros ids unicos
    //      ---------------------------------------
    //      @PostMapping(value = "/novo")
    //      @ResponseBody
    //          nova(@RequestBody Entidade ent)
    //      ---------------------------------------
    //      @PutMapping(value = "/adicionarNoPais/{id}")
    //      @ResponseBody
    //          adicionarNoPais
    //      ---------------------------------------
    //      @DeleteMapping(value = "/deletar/{id}")
    //      @ResponseBody
    //          remove por id
    //      ---------------------------------------
}
