package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Repository.BairroRepository;
import org.springframework.stereotype.Service;

@Service
public class BairroService extends AbstractService<BairroRepository, Bairro> {
}
