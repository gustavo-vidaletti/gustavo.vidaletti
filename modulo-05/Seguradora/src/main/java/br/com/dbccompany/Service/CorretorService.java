package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Repository.CorretorRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CorretorService extends PessoaService<Corretor, CorretorRepository> {
}
