package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "id_pessoa")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Corretor.class)
public class Corretor extends Pessoa{

    private String cargo = "";

    private double comissao = 0;

    @OneToMany(mappedBy = "pessoaCorretor")
    private List<ServicoContratado> servicosContratados = new ArrayList<>();

    // Getters And Setters

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getComissao() {
        return comissao;
    }

    public void setComissao(double comissao) {
        this.comissao = comissao;
    }

    public List<ServicoContratado> getServicosContratados() {
        return servicosContratados;
    }

    public void setServicosContratados(List<ServicoContratado> servicosContratados) {
        this.servicosContratados = servicosContratados;
    }

    public void pushServicosContratados(ServicoContratado... servicosContratados) {
        this.servicosContratados.addAll(Arrays.asList(servicosContratados));
    }
}
