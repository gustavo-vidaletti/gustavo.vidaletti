package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Corretor;
import org.springframework.data.repository.CrudRepository;

public interface CorretorRepository extends PessoaRepository<Corretor> {

    Corretor findAllByComissao(double comissao);
}
