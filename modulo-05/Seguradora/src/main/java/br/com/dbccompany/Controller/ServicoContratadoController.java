package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.ServicoContratadoRepository;
import br.com.dbccompany.Service.ServicoContratadoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "api/servico-contratado")
public class ServicoContratadoController extends AbstractController<ServicoContratado, ServicoContratadoRepository, ServicoContratadoService>{

}
