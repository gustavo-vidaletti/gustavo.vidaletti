package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Segurado;
import br.com.dbccompany.Repository.SeguradoRepository;
import org.springframework.stereotype.Service;

@Service
public class SeguradoService extends PessoaService<Segurado, SeguradoRepository> {

}