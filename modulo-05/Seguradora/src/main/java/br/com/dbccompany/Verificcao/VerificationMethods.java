package br.com.dbccompany.Verificcao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VerificationMethods {
    public static boolean isTruthy(Object object) {
        if(object == null) return false;
        switch (object.getClass().getSimpleName()){
            case "String": return !object.equals("");
            case "Long": return Long.parseLong(object.toString()) != 0;
            case "List": return ((List<Object>) object).size() > 0;
            case "ArrayList": return ((List<Object>) object).size() > 0;
            case "Double": return Double.parseDouble(object.toString()) != 0;
            case "Integer": return Integer.parseInt(object.toString()) != 0;
            default: return object != null;
        }
    }

    public static void main(String args[]){
        System.out.println("String \"a\" " + isTruthy("a") );
        System.out.println("String \"\" " + isTruthy("") );
        System.out.println("long 1 " + isTruthy(Long.parseLong("1")) );
        System.out.println("long 0 " + isTruthy(Long.parseLong("0")) );
        System.out.println("Lista vazia " + isTruthy(new ArrayList<>()) );
        System.out.println("Lista com 2 elementos " + isTruthy(Arrays.asList(2,3)));
    }


}
