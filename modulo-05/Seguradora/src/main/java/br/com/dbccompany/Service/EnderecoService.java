package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.EnderecoRepository;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService extends AbstractService<EnderecoRepository, Endereco> {
}
