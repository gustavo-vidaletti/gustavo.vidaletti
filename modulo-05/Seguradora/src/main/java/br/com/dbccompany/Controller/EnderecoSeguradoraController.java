package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.EnderecoSeguradora;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.EnderecoSeguradoraRepository;
import br.com.dbccompany.Service.BairroService;
import br.com.dbccompany.Service.EnderecoSeguradoraService;
import br.com.dbccompany.Service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "api/endereco-seguradora")
public class EnderecoSeguradoraController extends AbstractController<EnderecoSeguradora, EnderecoSeguradoraRepository, EnderecoSeguradoraService> {

}
