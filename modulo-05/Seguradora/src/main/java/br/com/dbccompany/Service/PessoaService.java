package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Pessoa;
import br.com.dbccompany.Repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class PessoaService <TipoPessoa extends Pessoa, TipoPessoaRepository extends PessoaRepository<TipoPessoa>> extends AbstractService<TipoPessoaRepository, TipoPessoa>{

    public TipoPessoa buscarPorCPF(long cpf){
        return this.repository.findByCpf(cpf);
    }

    public TipoPessoa buscarPorEmail(String email){
        return this.repository.findByEmail(email);
    }

}
