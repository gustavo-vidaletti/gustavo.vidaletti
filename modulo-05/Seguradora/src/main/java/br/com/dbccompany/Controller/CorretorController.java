package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Repository.CorretorRepository;
import br.com.dbccompany.Service.CorretorService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "api/corretor")
public class CorretorController extends PessoaController<Corretor, CorretorRepository, CorretorService> {
}
