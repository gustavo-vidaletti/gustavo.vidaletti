package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Segurado;
import br.com.dbccompany.Repository.SeguradoRepository;
import br.com.dbccompany.Service.SeguradoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "api/segurado")
public class SeguradoController extends PessoaController<Segurado, SeguradoRepository, SeguradoService> {

}
