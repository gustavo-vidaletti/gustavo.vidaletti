package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
@Table(name = "endereco_seguradora")
@PrimaryKeyJoinColumn(name = "id_endereco")
public class EnderecoSeguradora extends Endereco {

    @OneToOne(mappedBy = "enderecoSeguradora")
    @JoinColumn(name = "id_seguradora")
    private Seguradora seguradora;

    // Getters And Setters

    public Seguradora getSeguradora() {
        return seguradora;
    }

    public void setSeguradora(Seguradora seguradora) {
        this.seguradora = seguradora;
    }
}
