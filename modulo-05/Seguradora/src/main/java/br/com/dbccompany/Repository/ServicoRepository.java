package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Servico;
import org.springframework.data.repository.CrudRepository;

public interface ServicoRepository extends CrudRepository<Servico, Long> {
    Servico findById(long id);
}
