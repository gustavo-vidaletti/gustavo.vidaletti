package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.AbstractEntity;
import br.com.dbccompany.Verificcao.VerificationMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public abstract class AbstractService <Repository extends CrudRepository<Entidade, Long>, Entidade extends AbstractEntity> {

    @Autowired
    Repository repository;

    @Transactional(rollbackFor = Exception.class)
    public Entidade salvar(Entidade entidade) {
        return repository.save(entidade);
    }

    @Transactional(rollbackFor = Exception.class)
    public Entidade editarPorId(long id, Entidade entidade){

        Entidade nova = repository.findById(id).get();

        //Pra cada metodo da entidade
        for(Method method: entidade.getClass().getMethods()){

            String tipoRetornado = method.getReturnType().getSimpleName();
            String nomeDoMetodo = method.getName();

            // Verifica se o tipo do metodo nao eh void &&
            // Verifica se não é getClass &&
            // Verifica se o nome dele eh getQUALQUERCOISA
            if( !tipoRetornado.matches("void") &&
                !nomeDoMetodo.matches("getClass") &&
                nomeDoMetodo.matches("(get){1}.*"))
            {
                // Muda de getQUALQUERCOISA para setQUALQUERCOISA
                String setMetodo = nomeDoMetodo.replaceFirst("(get){1}", "set");

                try{
                    // Chama o método da entidade para guardar seu valor
                    Object valor = method.invoke(entidade);

                    // Verifica se o método é "truthy" (como JS)
                    if(VerificationMethods.isTruthy(valor)){

                        // Chama o setQUALQUERCOISA passando o valor recebido no get
                        // Somente se não for null
                        nova.getClass().getMethod(setMetodo, method.getReturnType()).invoke(nova, valor);
                    }

                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex){
                    ex.printStackTrace();
                }
            }
        }
        nova.setId(id);
        return repository.save(nova);
    }


    @Transactional(rollbackFor = Exception.class)
    public boolean deletarPorId(long id){
        Entidade entidade = repository.findById(id).get();
        boolean existia = VerificationMethods.isTruthy(entidade);
        repository.deleteById(id);
        return existia;
    }

    public Entidade buscarPorId(long id){
        return repository.findById(id).get();
    }

    public List<Entidade> listarTodos(){
        return (List<Entidade>) repository.findAll();
    }


    //TODO: ---------------------------------------------
    //      @Service
    //      ---------------------------------------------
    //      @Autowired -> declarar repository
    //      ---------------------------------------------
    //      @Transactional(rollbackFor = Exception.class)
    //      sempre que for mudar salvar ou deletar
    //      ---------------------------------------------
    //      salvar
    //      ---------------------------------------------
    //      editarPorId(long id, Entity)
    //      ---------------------------------------------
    //      deletarPorId(long id) -> retorna Entity
    //      ---------------------------------------------
    //      buuscarPorId(long id)
    //      ---------------------------------------------
    //      listarTodos()
    //      ---------------------------------------------

}
