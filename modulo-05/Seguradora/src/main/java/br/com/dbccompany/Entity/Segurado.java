package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "id_pessoa")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Segurado.class)
public class Segurado extends Pessoa {

    @Column(name = "qtd_servicos")
    private Integer qtdServicos = 0;

    @OneToMany(mappedBy = "pessoaSegurada")
    private List<ServicoContratado> servicosContratados = new ArrayList<>();

    // Getters And Setters

    public Integer getQtdServicos() {
        return qtdServicos;
    }

    public void setQtdServicos(Integer qtdServicos) {
        this.qtdServicos = qtdServicos;
    }

    public List<ServicoContratado> getServicosContratados() {
        return servicosContratados;
    }

    public void setServicosContratados(List<ServicoContratado> servicosContratados) {
        this.servicosContratados = servicosContratados;
    }

    public void pushServicosContratados(ServicoContratado... servicosContratados){
        this.servicosContratados.addAll(Arrays.asList(servicosContratados));
    }
}
