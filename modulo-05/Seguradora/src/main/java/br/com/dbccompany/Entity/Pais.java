package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pais.class)
public class Pais extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAIS_SEQ", sequenceName = "PAIS_SEQ")
    @GeneratedValue(generator = "PAIS_SEQ",strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(unique = true)
    private String nome;

    @OneToMany(mappedBy = "pais", cascade = CascadeType.ALL)
//    @JsonManagedReference
    private List<Estado> estados = new ArrayList<>();

    //Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public void pushEstados(Estado... estados){
        this.estados.addAll(Arrays.asList(estados));
    }

}
