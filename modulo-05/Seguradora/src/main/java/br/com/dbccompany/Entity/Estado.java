package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Estado.class)
public class Estado extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ")
    @GeneratedValue(generator = "ESTADO_SEQ",strategy = GenerationType.SEQUENCE)
    private long id;

    private String nome;

    @ManyToOne
    @JoinColumn(name = "id_pais")
//    @JsonBackReference
    private Pais pais;

    @OneToMany(mappedBy = "estado", cascade = CascadeType.MERGE)
    private List<Cidade> cidades = new ArrayList<>();

    //Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public void pushCidades(Cidade... cidades) {
        this.cidades.addAll(Arrays.asList(cidades));
    }

}
