package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.EnderecoSeguradora;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.SeguradoraRepository;
import br.com.dbccompany.Service.EnderecoSeguradoraService;
import br.com.dbccompany.Service.SeguradoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/seguradora")
public class SeguradoraController extends AbstractController<Seguradora, SeguradoraRepository, SeguradoraService> {

    @Autowired
    SeguradoraService seguradoraService;

    @Autowired
    EnderecoSeguradoraService enderecoSeguradoraService;

    @GetMapping(value = "/cnpj/{cnpj}")
    @ResponseBody
    public Seguradora buscarPorCnpj(@PathVariable long cnpj){
        return seguradoraService.buscarPeloCnpj(cnpj);
    }

//    @PostMapping(value = "/adicionar")
//    @ResponseBody
//    public Seguradora adicionar(@RequestBody Seguradora entidade){
//        Seguradora seg = seguradoraService.salvar(entidade);
//        if (new Long(seg.getEnderecoSeguradora().getId()) != null){
//            EnderecoSeguradora endS = enderecoSeguradoraService.buscarPorId(seg.getEnderecoSeguradora().getId());
//            endS.setSeguradora(seg);
//            enderecoSeguradoraService.salvar(endS);
//        }
//        return seg;
//    }
}
