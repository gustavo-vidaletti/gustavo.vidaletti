package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pais;
import br.com.dbccompany.Repository.PaisRepository;
import br.com.dbccompany.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/pais")
public class PaisController extends AbstractController<Pais, PaisRepository, PaisService>{

    @Autowired
    PaisService paisService;

    @GetMapping(value = "/{param}")
    @ResponseBody
    public Pais buscar(@PathVariable String param){
        try{
            long id = Long.parseLong(param);
            return paisService.buscarPorId(id);
        } catch (NumberFormatException e){
            return paisService.buscarPorNome(StringUtils.capitalize(param.toLowerCase()));
        }
    }

}
