package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Bairro extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "BAIRRO_SEQ", sequenceName = "BAIRRO_SEQ")
    @GeneratedValue(generator = "BAIRRO_SEQ",strategy = GenerationType.SEQUENCE)
    private long id;

    private String nome;

    @ManyToOne
    @JoinColumn(name = "id_cidade")
    private Cidade cidade;

    @OneToMany(mappedBy = "bairro", cascade = CascadeType.ALL)
    private List<Endereco> enderecos  = new ArrayList<>();

    // Getters And Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public void pushEnderecos(Endereco... enderecos){
        this.enderecos.addAll(Arrays.asList(enderecos));
    }

}
