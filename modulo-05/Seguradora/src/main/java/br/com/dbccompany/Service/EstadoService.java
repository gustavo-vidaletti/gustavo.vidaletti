package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;
import org.springframework.stereotype.Service;

@Service
public class EstadoService extends AbstractService<EstadoRepository, Estado> {

}
