package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Pessoa extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PESSOA_SEQ", sequenceName = "PESSOA_SEQ")
    @JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pessoa.class)
    @GeneratedValue(generator = "PESSOA_SEQ",strategy = GenerationType.SEQUENCE)
    private long id;

    private String nome;

    @Column(unique = true)
    private long cpf;

    private String pai;

    private String mae;

    private long telefone;

    @Column(unique = true)
    private String email;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable( name = "endereco_pessoa",
        joinColumns = {
            @JoinColumn( name = "id_pessoa")
        },
        inverseJoinColumns = {
            @JoinColumn( name = "id_endereco")
        }
    )
    private List<Endereco> enderecos = new ArrayList<>();

    //Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public String getPai() {
        return pai;
    }

    public void setPai(String pai) {
        this.pai = pai;
    }

    public String getMae() {
        return mae;
    }

    public void setMae(String mae) {
        this.mae = mae;
    }

    public long getTelefone() {
        return telefone;
    }

    public void setTelefone(long telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public void pushEnderecos(Endereco... enderecos) {
        this.enderecos.addAll(Arrays.asList(enderecos));
    }


}
