package br.com.dbccompany.Security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String URL_LOGIN = "/login";
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
        .antMatchers(HttpMethod.POST, URL_LOGIN).permitAll() // login permitido
        .antMatchers(HttpMethod.GET, "/api/seguradora").permitAll() //metodo get da seguradora
        .antMatchers("/api/pais").permitAll() //TODOS METODOS dessa url
        .anyRequest().authenticated() // Para qualquer outra request!
        .and()
        .addFilterBefore(new JWTLoginFilter(URL_LOGIN, authenticationManager()), UsernamePasswordAuthenticationFilter.class)
        .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

//        .anyRequest().permitAll(); // COMENTAR QUANDO FOR PRA NAO DAR AUTORIZAÇÃO
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password("{noop}password")
                .roles("ADMIN");
    }
}
