package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Servico.class)
public class Servico extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "SERVICO_SEQ", sequenceName = "SERVICO_SEQ")
    @GeneratedValue(generator = "SERVICO_SEQ",strategy = GenerationType.SEQUENCE)
    private long id;

    private String nome = "";

    private String descricao = "";

    @Column(name = "valor_padrao")
    private double valorPadrao = 100;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "seguradora_servico",
            joinColumns = {
                    @JoinColumn( name = "id_servico")
            },
            inverseJoinColumns = {
                    @JoinColumn( name = "id_seguradora")
            }
    )
    private List<Seguradora> seguradoras = new ArrayList<>();

    @OneToMany(mappedBy = "servico")
    private List<ServicoContratado> servicosContratados = new ArrayList<>();

    // Getters And Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValorPadrao() {
        return valorPadrao;
    }

    public void setValorPadrao(double valorPadrao) {
        this.valorPadrao = valorPadrao;
    }

    public List<Seguradora> getSeguradoras() {
        return seguradoras;
    }

    public void setSeguradoras(List<Seguradora> seguradoras) {
        this.seguradoras = seguradoras;
    }

    public void pushSeguradoras(Seguradora... seguradoras){
        this.seguradoras.addAll(Arrays.asList(seguradoras));
    }

    public List<ServicoContratado> getServicosContratados() {
        return servicosContratados;
    }

    public void setServicosContratados(List<ServicoContratado> servicosContratados) {
        this.servicosContratados = servicosContratados;
    }

    public void pushServicosContratados(ServicoContratado... servicosContratados){
        this.servicosContratados.addAll(Arrays.asList(servicosContratados));
    }
}
