package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Pais;
import br.com.dbccompany.Repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class PaisService extends AbstractService<PaisRepository, Pais> {

    public Pais salvar(Pais pais){
        pais.setNome(StringUtils.capitalize(pais.getNome().toLowerCase()));
        return repository.save(pais);
    }

    public Pais buscarPorNome(String nome){
        return repository.findByNome(nome);
    }
}
