package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Entity.Pais;
import br.com.dbccompany.Repository.EstadoRepository;
import br.com.dbccompany.Service.EstadoService;
import br.com.dbccompany.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/estado")
public class EstadoController extends AbstractController<Estado, EstadoRepository, EstadoService> {
}
