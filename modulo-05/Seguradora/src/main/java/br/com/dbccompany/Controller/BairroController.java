package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Repository.BairroRepository;
import br.com.dbccompany.Service.BairroService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "api/bairro")
public class BairroController extends AbstractController<Bairro, BairroRepository, BairroService> {
}
