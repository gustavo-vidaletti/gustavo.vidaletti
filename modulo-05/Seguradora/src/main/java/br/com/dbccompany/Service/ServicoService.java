package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Repository.ServicoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ServicoService extends AbstractService<ServicoRepository, Servico> {

}
