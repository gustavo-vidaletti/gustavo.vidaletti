package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table(name = "servico_contratado")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Servico.class)
public class ServicoContratado extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "SERVICO_CONTRATADO_SEQ", sequenceName = "SERVICO_CONTRATADO_SEQ")
    @GeneratedValue(generator = "SERVICO_CONTRATADO_SEQ",strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_servico")
    private Servico servico;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_seguradora")
    private Seguradora seguradora;

    private String descricao;

    private double valor = 100;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_pessoa_segurado")
    private Segurado pessoaSegurada;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_pessoa_corretor")
    private Corretor pessoaCorretor;

    // Getters And Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public Seguradora getSeguradora() {
        return seguradora;
    }

    public void setSeguradora(Seguradora seguradora) {
        this.seguradora = seguradora;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Segurado getPessoaSegurada() {
        return pessoaSegurada;
    }

    public void setPessoaSegurada(Segurado pessoaSegurada) {
        this.pessoaSegurada = pessoaSegurada;
    }

    public Corretor getPessoaCorretor() {
        return pessoaCorretor;
    }

    public void setPessoaCorretor(Corretor pessoaCorretor) {
        this.pessoaCorretor = pessoaCorretor;
    }

}
