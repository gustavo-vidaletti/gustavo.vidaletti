package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Repository.ServicoRepository;
import br.com.dbccompany.Service.SeguradoraService;
import br.com.dbccompany.Service.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "api/servico")
public class ServicoController extends AbstractController<Servico, ServicoRepository, ServicoService> {

    @Autowired
    SeguradoraService seguradoraService;

    @PostMapping(value = "/adicionar")
    @ResponseBody
    public Servico adicionar(@RequestBody Servico entidade){
        return entidadeService.salvar(entidade);
    }

}
