package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Pessoa;
import org.springframework.data.repository.CrudRepository;

public interface PessoaRepository <TipoPessoa extends Pessoa> extends CrudRepository<TipoPessoa, Long> {

    TipoPessoa findByCpf(long cpf);
    TipoPessoa findByEmail(String email);


}
