package br.com.dbccompany.Entity;


public abstract class AbstractEntity {
    public abstract long getId();

    public abstract void setId(long id);

}
