package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CidadeService extends AbstractService<CidadeRepository, Cidade> {

}
