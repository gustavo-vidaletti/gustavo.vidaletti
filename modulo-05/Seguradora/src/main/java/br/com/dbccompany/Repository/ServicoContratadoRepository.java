package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.ServicoContratado;
import org.springframework.data.repository.CrudRepository;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Long> {
    ServicoContratado findById(long id);
}
