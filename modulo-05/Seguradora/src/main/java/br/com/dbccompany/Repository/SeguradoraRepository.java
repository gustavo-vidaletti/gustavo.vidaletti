package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Seguradora;
import org.springframework.data.repository.CrudRepository;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Long> {
    Seguradora findById(long id);

    Seguradora findByCnpj(long cnpj);
}
