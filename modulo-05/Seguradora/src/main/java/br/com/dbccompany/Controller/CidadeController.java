package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Repository.CidadeRepository;
import br.com.dbccompany.Service.CidadeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cidade")
public class CidadeController extends AbstractController<Cidade, CidadeRepository, CidadeService> {

}
