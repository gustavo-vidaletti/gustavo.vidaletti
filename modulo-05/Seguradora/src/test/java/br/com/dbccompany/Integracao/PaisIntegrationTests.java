package br.com.dbccompany.Integracao;

import br.com.dbccompany.Entity.Pais;
import br.com.dbccompany.Repository.PaisRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PaisIntegrationTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PaisRepository paisRepository;

    @Test
    public void acharPaisPorNome(){
        Pais pais = new Pais();
        pais.setNome("Brasil");
        entityManager.persist(pais);
        entityManager.flush();

        Pais found = paisRepository.findByNome(pais.getNome());

        assertEquals(pais.getNome(),found.getNome());
    }
}

