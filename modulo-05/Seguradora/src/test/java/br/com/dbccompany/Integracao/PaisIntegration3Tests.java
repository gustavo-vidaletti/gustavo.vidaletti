package br.com.dbccompany.Integracao;

import br.com.dbccompany.Controller.PaisController;
import br.com.dbccompany.SeguradoraApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class PaisIntegration3Tests extends SeguradoraApplicationTests {

    private MockMvc mvc;

    @Autowired
    private PaisController controller;

    @Before
    public void setUp(){
        this.mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void statusOk() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/api/pais/")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
