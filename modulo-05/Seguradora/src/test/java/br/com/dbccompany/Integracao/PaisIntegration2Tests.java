package br.com.dbccompany.Integracao;

import br.com.dbccompany.Entity.Pais;
import br.com.dbccompany.Repository.PaisRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)

public class PaisIntegration2Tests {

    @MockBean
    private PaisRepository paisRepository;

    @Autowired
    ApplicationContext context;

    @Before
    public void setUp(){
        Pais pais = new Pais();
        pais.setNome("Brasil");

        Mockito.when(paisRepository.findByNome(pais.getNome())).thenReturn(pais);
    }

    @Test
    public void acharPaisPorNome2(){
        String nome = "Brasil";
        Pais found = paisRepository.findByNome(nome);

        assertEquals(nome, found.getNome());
    }
}

