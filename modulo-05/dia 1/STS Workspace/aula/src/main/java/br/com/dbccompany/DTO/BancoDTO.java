package br.com.dbccompany.DTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BancoDTO {

    private long codigo;

    private String nome;

    private List<AgenciaDTO> agencias = new ArrayList<>();

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<AgenciaDTO> getAgencias() {
        return agencias;
    }

    public void setAgencias(List<AgenciaDTO> agencias) {
        this.agencias = agencias;
    }

    public void pushAgencias(AgenciaDTO... dtos){ this.agencias.addAll(Arrays.asList(dtos)); }

    public BancoDTO(){}

    public BancoDTO(long codigo, String nome){
        this.codigo = codigo;
        this.nome = nome;
//        this.agencias.addAll(Arrays.asList(agenciasDTO));
    }


}
