package br.com.dbccompany.Controllers;

import br.com.dbccompany.DTO.AgenciaDTO;
import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Service.AgenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/agencia")
public class AgenciaController {

    @Autowired
    AgenciaService agenciaService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<AgenciaDTO> listarAgencias(){
        List<AgenciaDTO> lista = new ArrayList<>();
        for(Agencia ag: agenciaService.allAgencias()){
            lista.add(ag.geraDTO());
        }
        return lista;
    }

    @GetMapping(value = "/{codigoAgencia}/{codigoBanco}")
    @ResponseBody
    public AgenciaDTO buscarAgenciaPorCodigo(@PathVariable long codigoAgencia, @PathVariable long codigoBanco){ return null; } //TODO: BUSCAR!

    @PostMapping(value = "/adicionar/{idBanco}")
    @ResponseBody
    public AgenciaDTO adicionarAgencia(@RequestBody Agencia agencia, @PathVariable long idBanco){
        return agenciaService.salvar(agencia, idBanco).geraDTO();
    }


}
