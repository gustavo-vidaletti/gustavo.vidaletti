package br.com.dbccompany.Entity;

import br.com.dbccompany.DTO.AgenciaDTO;

import javax.persistence.*;

@Entity
public class Agencia {

    @EmbeddedId
    private AgenciaBancoId codigo;

    @Column(name = "nome", unique = true)
    private String nome;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_endereco_comercial")
    private EnderecoComercial enderecoComercial;

    public EnderecoComercial getEnderecoComercial() {
        return enderecoComercial;
    }

    public void setEnderecoComercial(EnderecoComercial enderecoComercial) {
        this.enderecoComercial = enderecoComercial;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public AgenciaBancoId getCodigo() {
        return codigo;
    }

    public void setCodigo(AgenciaBancoId codigo) {
        this.codigo = codigo;
    }

    public AgenciaDTO geraDTO(){
        AgenciaDTO dto = new AgenciaDTO(this.codigo.getCodigoAgencia(), this.codigo.getBanco().getCodigo(), this.nome);
        return dto;
    }
}
