package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
@Table(name = "endereco_comercial")
@PrimaryKeyJoinColumn(name = "id_endereco")
public class EnderecoComercial extends Endereco {

    @OneToOne(mappedBy = "enderecoComercial")
    @JoinColumns( {
            @JoinColumn( name = "codigo_agencia", referencedColumnName = "codigo"),
            @JoinColumn( name = "codigo_banco_agencia", referencedColumnName = "codigo_banco")
    })
    private Agencia agencia;

}
