package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class AgenciaBancoId implements Serializable {

    @Column(name = "codigo_agencia")
    private long codigoAgencia;

    @ManyToOne
    @JoinColumn(name = "codigo_banco")
    private Banco banco;

    public AgenciaBancoId(){}

    public AgenciaBancoId(Banco banco, long codigoAgencia){
        this.setBanco(banco);
        this.setCodigoAgencia(codigoAgencia);
    }

    public long getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(long codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }
}
