package br.com.dbccompany;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Entity.Pais;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AulaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AulaApplication.class, args);

	}

}
