package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
public class Pais {

    public Pais(){}

    public Pais(String nome){
        this.setNome(nome);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @OneToMany(mappedBy = "pais", cascade = CascadeType.ALL)
    private List<Estado> estados = new ArrayList<>();

    private String nome;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void pushEstados(Estado... estados) {
        this.estados.addAll(Arrays.asList(estados));
    }

    public List<Estado> getEstados() {
        return estados;
    }
}
