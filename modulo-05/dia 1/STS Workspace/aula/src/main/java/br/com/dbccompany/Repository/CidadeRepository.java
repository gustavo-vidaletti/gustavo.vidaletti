package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cidade;
import org.springframework.data.repository.CrudRepository;

public interface CidadeRepository extends CrudRepository<Cidade, Long> {
    Cidade findById(long id);
}
