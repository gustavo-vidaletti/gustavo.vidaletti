package br.com.dbccompany.Controllers;

import br.com.dbccompany.DTO.BancoDTO;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/banco")
public class BancoController {

    @Autowired
    BancoService bancoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<BancoDTO> listarBancos(){
        List<BancoDTO> lista = new ArrayList<>();
        for(Banco banco: bancoService.allBancos()){
            lista.add(banco.geraDTO());
        }
        return lista;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public BancoDTO novoBanco(@RequestBody Banco banco){
        return bancoService.salvar(banco).geraDTO();
    }

    @GetMapping(value = "/{codigo}")
    @ResponseBody
    public BancoDTO buscarBancoPorCodigo(@PathVariable long codigo){
        return bancoService.buscarPorCodigo(codigo).geraDTO();
    }

    @PutMapping(value = "/editar/{codigo}")
    @ResponseBody
    public BancoDTO editarPorCodigo(@PathVariable long codigo, @RequestBody Banco banco){
        return bancoService.editarPorCodigo(codigo, banco).geraDTO();
    }

}
