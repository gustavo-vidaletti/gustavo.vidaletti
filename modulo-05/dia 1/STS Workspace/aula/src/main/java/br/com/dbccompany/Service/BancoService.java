package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService {

    @Autowired
    private BancoRepository bancoRepository;

    @Transactional(rollbackFor = Exception.class)
    public Banco salvar(Banco banco){
        return bancoRepository.save(banco);
    }

//    public Banco buscarPorId(long id){
//        Optional<Banco> opcional = bancoRepository.findById(id);
//        if( opcional.isPresent() ) return opcional.get();
//        return null;
//    }

    public Banco buscarPorNome(String nome){
        return bancoRepository.findByNome(nome);
    }

    public Banco buscarPorCodigo(long codigo) {
        return bancoRepository.findByCodigo(codigo);
    }

    @Transactional(rollbackFor = Exception.class)
    public Banco editarPorCodigo(long codigo, Banco novoBanco){
        novoBanco.setCodigo(codigo);
        return bancoRepository.save(novoBanco);
    }

    @Transactional(rollbackFor = Exception.class)
    public Banco editarPorNome(String nome, Banco novoBanco){
        long cod = this.buscarPorNome(nome).getCodigo();
        novoBanco.setCodigo(cod);
        return bancoRepository.save(novoBanco);
    }

    public List<Banco> allBancos(){
        return (List<Banco>) bancoRepository.findAll();
    }

}
