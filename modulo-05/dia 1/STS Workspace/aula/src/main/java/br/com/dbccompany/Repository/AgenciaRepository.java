package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Agencia;
import org.springframework.data.repository.CrudRepository;

public interface AgenciaRepository extends CrudRepository<Agencia, Long> {
    Agencia findByNome(String nome);

    Agencia findAgenciaByCodigoCodigoAgenciaAndCodigoBanco(long codigoAgencia, long codigoBanco);
}
