package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.AgenciaBancoId;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AgenciaService {

    @Autowired
    private AgenciaRepository agenciaRepository;

    @Autowired
    BancoService bancoService;

    @Transactional(rollbackFor = Exception.class)
    public Agencia salvar(Agencia agencia, long idBanco){
        Banco banco = bancoService.buscarPorCodigo(idBanco);
        agencia.setCodigo(new AgenciaBancoId(banco, agencia.getCodigo().getCodigoAgencia()));
        return agenciaRepository.save(agencia);
    }

    public Agencia buscarPorNome(String nome){
        return agenciaRepository.findByNome(nome);
    }

    public Agencia buscarPorCodigo(long codigoAgencia, long codigoBanco){ return  agenciaRepository.findAgenciaByCodigoCodigoAgenciaAndCodigoBanco(codigoAgencia, codigoBanco);}

    public List<Agencia> allAgencias(){
        return (List<Agencia>) agenciaRepository.findAll();
    }


}
