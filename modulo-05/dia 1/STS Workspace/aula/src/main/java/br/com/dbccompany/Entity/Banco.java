package br.com.dbccompany.Entity;

import br.com.dbccompany.DTO.BancoDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
//@SequenceGenerator(allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
public class Banco {
	@Id
	@Column(name = "CODIGO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long codigo;

	@Column(unique = true)
	private String nome;
	
	@OneToMany(mappedBy = "codigo.banco")
	private List<Agencia> agencias = new ArrayList<>();

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Agencia> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<Agencia> agencias) {
		this.agencias = agencias;
	}

	public void pushAgencias(Agencia... agencias) {
		this.agencias.addAll(Arrays.asList(agencias));
	}

	public BancoDTO geraDTO(){
		BancoDTO dto = new BancoDTO(this.codigo, this.nome);
		for( Agencia ag: this.agencias){
			dto.pushAgencias( ag.geraDTO() );
		}

		return dto;
	}
}
