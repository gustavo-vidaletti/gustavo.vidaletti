package br.com.dbccompany.Entity;


import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "endereco_comercial")
@PrimaryKeyJoinColumn(name = "id_endereco")
public class EnderecoResidencial extends Endereco {

    //Many to many com usuarios
}
