package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Pais;
import org.springframework.data.repository.CrudRepository;

public interface PaisRepository extends CrudRepository<Pais, Long> {
    Pais findById(long id);
}
