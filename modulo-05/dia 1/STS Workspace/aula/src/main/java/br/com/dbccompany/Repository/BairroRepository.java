package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Bairro;
import org.springframework.data.repository.CrudRepository;

public interface BairroRepository extends CrudRepository<Bairro, Long> {
    Bairro findById(long id);
}
