package br.com.dbccompany.DTO;

import br.com.dbccompany.Entity.AgenciaBancoId;

public class AgenciaDTO {

    public AgenciaDTO(){}

    public AgenciaDTO(long codigoAgencia, long codigoBanco, String nome){
        this.codigoAgencia = codigoAgencia;
        this.codigoBanco = codigoBanco;
        this.nome = nome;
    }

    private long codigoAgencia;

    private String nome;

    private long codigoBanco;

    private long idEndereco;

    public long getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(long codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(long codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public long getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(long idEndereco) {
        this.idEndereco = idEndereco;
    }
}
