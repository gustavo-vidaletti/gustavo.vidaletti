package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Estado;
import org.springframework.data.repository.CrudRepository;

public interface EstadoRepository extends CrudRepository<Estado, Long> {
    Estado findById(long id);
}
