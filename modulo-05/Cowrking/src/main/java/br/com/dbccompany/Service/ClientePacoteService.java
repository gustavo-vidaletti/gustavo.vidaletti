package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Repository.ClientePacoteRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientePacoteService extends AbstractService<ClientePacoteRepository, ClientePacote> {
}
