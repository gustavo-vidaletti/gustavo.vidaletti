package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Operacoes.VerificationMethods;
import br.com.dbccompany.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ClienteService extends AbstractService<ClienteRepository, Cliente> {

    @Autowired
    TipoContatoService tipoContatoService;

    @Autowired
    ContatoService contatoService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Cliente salvar(Cliente cliente) throws Exception {

        if(!VerificationMethods.isCPFValid(cliente.getCpf())){
            throw new Exception("CPF Não é válido");
        }

        if(cliente.getId() == 0 && repository.findByCpf(cliente.getCpf()) != null){
            throw new Exception("CPF já registrado");
        }

        List<String> requisitos = new ArrayList<>(Arrays.asList("telefone","email"));

        for(Contato contato: cliente.getContatos()) {

            String nome = contato.getTipoContato().getNome().toLowerCase();
            contato.setTipoContato(tipoContatoService.buscarPorNome(nome));

            if(requisitos.contains(nome)) {
                requisitos.remove(nome);
            }

            repository.save(cliente);
            contato.setCliente(cliente);
            contatoService.salvar(contato);
        }

        if(requisitos.size() > 0) {
            throw new Exception("Deve ter um telefone e um email");
        }

        return super.salvar(cliente);
    }
}
