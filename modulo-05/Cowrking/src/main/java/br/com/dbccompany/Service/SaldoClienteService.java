package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoClienteID;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.EspacoRepository;
import br.com.dbccompany.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    SaldoClienteRepository repository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente sc){
        return repository.save(sc);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editarPorIds(long idCliente, long idEspaco, SaldoCliente sc){
        SaldoClienteID pk = new SaldoClienteID();
        pk.setCliente(clienteRepository.findById(idCliente).get());
        pk.setEspaco(espacoRepository.findById(idEspaco).get());
        sc.setId(pk);
        return repository.save(sc);
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean removerPorId(long idCliente, long idEspaco){
        SaldoClienteID id = new SaldoClienteID();
        id.setCliente(clienteRepository.findById(idCliente).get());
        id.setEspaco(espacoRepository.findById(idEspaco).get());
        boolean existia = (repository.findById(id).isPresent());
        repository.deleteById(id);
        return existia;
    }

    public SaldoCliente buscarPorId(long idCliente, long idEspaco){
        SaldoClienteID id = new SaldoClienteID();
        id.setCliente(clienteRepository.findById(idCliente).get());
        id.setEspaco(espacoRepository.findById(idEspaco).get());
        return repository.findById(id).get();
    }

    public List<SaldoCliente> listarTodos(){
        return (List<SaldoCliente>) repository.findAll();
    }

    public List<SaldoCliente> listarTodosDoEspaco(long idEspaco){
        return repository.findAllById_Espaco(idEspaco);
    }

    public List<SaldoCliente> listarTodosDoCliente(long idCliente){
        return repository.findAllById_Cliente(idCliente);
    }
}
