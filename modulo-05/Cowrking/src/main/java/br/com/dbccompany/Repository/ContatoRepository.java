package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Contato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository<Contato, Long> {
}
