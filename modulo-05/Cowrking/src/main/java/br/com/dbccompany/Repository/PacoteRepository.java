package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;

public interface PacoteRepository extends CrudRepository<Pacote, Long> {

    Pacote findById(long id);
}
