package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contratacao> listarTodos(){
        return service.listarTodos();
    }

    @GetMapping(value = "/{param}")
    @ResponseBody
    public Contratacao buscar(@PathVariable String param){
        try {
            long id = Long.parseLong(param);
            return service.buscarPorId(id);
        } catch (NumberFormatException e){
            System.err.println("Erro formatando o numero");
        }
        return null;
    }

    @PostMapping(value = "/adicionar")
    @ResponseBody
    public String adicionar(@RequestBody Contratacao contratacao) {
        return service.salvarRetornarValor(contratacao);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contratacao editar(@PathVariable long id, @RequestBody Contratacao entidade) throws Exception{
        return service.editarPorId(id, entidade);
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public boolean remover(@PathVariable long id) {
        return service.deletarPorId(id);
    }


}
