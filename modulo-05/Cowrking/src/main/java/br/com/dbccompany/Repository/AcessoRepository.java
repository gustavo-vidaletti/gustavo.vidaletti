package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Acesso;
import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Espaco;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    List<Acesso> findAllBySaldoCliente_Id_ClienteAndSaldoCliente_Id_Espaco(Cliente cliente, Espaco espaco);

}
