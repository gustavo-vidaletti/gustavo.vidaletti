package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Service.ClienteService;
import br.com.dbccompany.Service.ContatoService;
import br.com.dbccompany.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController extends AbstractController<Cliente, ClienteRepository, ClienteService> {

    @Autowired
    ContatoService contatoService;

    @Autowired
    TipoContatoService tipoContatoService;
}
