package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Repository.PacoteRepository;
import br.com.dbccompany.Service.PacoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/pacote")
public class PacoteController extends AbstractController<Pacote, PacoteRepository, PacoteService> {
}
