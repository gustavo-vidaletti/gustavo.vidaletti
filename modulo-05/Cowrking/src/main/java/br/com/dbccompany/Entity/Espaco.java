package br.com.dbccompany.Entity;

import br.com.dbccompany.Operacoes.ReaisParaValor;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Espaco.class)
public class Espaco extends AbstractEntity{
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(unique = true, nullable = false)
    private String nome;

    @Column(name = "qtd_pessoas", nullable = false)
    private int qtdPessoas = 0;

    @Column(nullable = false)
    private double valor = 0.0;

    @Transient
    private String valorReais = "";

    @OneToMany(mappedBy = "espaco")
    private List<EspacoPacote> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "id.espaco")
    private List<SaldoCliente> saldosClientes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco")
    private List<Contratacao> contratacoes = new ArrayList<>();

    // Getters and Setters

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
        this.valorReais = ReaisParaValor.getReais(valor);
    }

    public List<EspacoPacote> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacote> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<SaldoCliente> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoCliente> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public void pushContratacoes(Contratacao... contratacoes){
        this.contratacoes.addAll(Arrays.asList(contratacoes));
    }

    public void pushSaldosClientes(SaldoCliente... saldosClientes){
        this.saldosClientes.addAll(Arrays.asList(saldosClientes));
    }

    public void pushEspacosPacotes(EspacoPacote... espacosPacotes){
        this.espacosPacotes.addAll(Arrays.asList(espacosPacotes));
    }

    public String getValorReais() {
        return (valorReais != null)? valorReais : ReaisParaValor.getReais(valor);
    }

    public void setValorReais(String valorReais) {
        this.valorReais = valorReais;
        this.valor = ReaisParaValor.getValor(valorReais);
    }
}
