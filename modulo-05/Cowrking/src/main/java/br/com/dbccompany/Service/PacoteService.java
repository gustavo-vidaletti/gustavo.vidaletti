package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Operacoes.ReaisParaValor;
import br.com.dbccompany.Repository.PacoteRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PacoteService extends AbstractService<PacoteRepository, Pacote> {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Pacote salvar(Pacote pacote) throws Exception{
        pacote.setValor(ReaisParaValor.getValor(pacote.getValorReais()));
        return super.salvar(pacote);
    }

    @Override
    public Pacote buscarPorId(long id) {
        Pacote pacote = super.buscarPorId(id);
        pacote.setValorReais(ReaisParaValor.getReais(pacote.getValor()));
        return pacote;
    }
}
