package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class SaldoClienteID implements Serializable {

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_espaco")
    private Espaco espaco;

    // Getters and Setters

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    @Override
    public boolean equals(Object obj) {
        SaldoClienteID outro = (SaldoClienteID) obj;
        return (this.cliente.getId() == outro.getCliente().getId() &&
                this.espaco.getId() == outro.getEspaco().getId());
    }
}
