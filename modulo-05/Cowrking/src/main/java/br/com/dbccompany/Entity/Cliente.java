package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Cliente.class)
public class Cliente extends AbstractEntity{
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(nullable = false)
    private String nome;

    @Column(unique = true, nullable = false)
    private String cpf;

    @Column(name = "data_nascimento", nullable = false)
    private Date dataNascimento;

    @OneToMany(mappedBy = "cliente")
    private List<Contato> contatos = new ArrayList<>();

    @OneToMany(mappedBy = "cliente")
    private List<ClientePacote> clientesPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "cliente")
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany(mappedBy = "id.cliente")
    private List<SaldoCliente> saldosClientes = new ArrayList<>();

    // Getters And Setters

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ClientePacote> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacote> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<SaldoCliente> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoCliente> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public void pushSaldosClientes(SaldoCliente... saldos){
        this.saldosClientes.addAll(Arrays.asList(saldos));
    }

    public void pushContratacoes(Contratacao... contratacoes){
        this.contratacoes.addAll(Arrays.asList(contratacoes));
    }

    public void pushClintesPacotes(ClientePacote... clientesPacotes){
        this.clientesPacotes.addAll(Arrays.asList(clientesPacotes));
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }

    public void pushContatos(Contato... contatos){
        this.contatos.addAll(Arrays.asList(contatos));
    }
}
