package br.com.dbccompany.Operacoes;

import java.text.NumberFormat;
import java.util.Locale;

public class ReaisParaValor {
    public static double getValor(String reais){
        if(!reais.matches("(R)(\\$)(\\s)+(\\d){1,3}((\\.)(\\d){3})*(,)(\\d){2}")){
            return -1;
        }
        // R$ 300.000,00
        String apagarMoeda = reais.replaceFirst("(R\\$)\\s*", "");
        // 300.000,00
        String tirarPontos = apagarMoeda.replaceAll("\\.", "");
        // 300000,00
        String virgulaPraPonto = tirarPontos.replaceAll(",", ".");
        // 300000.00
        double valor = 0;
        try{
            valor = Double.parseDouble(virgulaPraPonto);
        } catch (NumberFormatException e){
            System.err.println("Erro ao formatar o número informado");
        }
        return valor;
    }

    public static String getReais(double valor){
        NumberFormat df = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        return df.format(valor);
    }
}
