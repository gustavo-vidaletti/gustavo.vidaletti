package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;
import br.com.dbccompany.Service.TipoContatoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/tipo-contato")
public class TipoContatoController extends AbstractController<TipoContato, TipoContatoRepository, TipoContatoService> {
}
