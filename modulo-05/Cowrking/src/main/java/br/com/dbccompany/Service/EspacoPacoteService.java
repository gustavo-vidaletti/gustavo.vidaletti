package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Repository.EspacoPacoteRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspacoPacoteService extends AbstractService<EspacoPacoteRepository, EspacoPacote> {

    public List<EspacoPacote> buscarTodosPorPacote(Pacote pacote){
        return (List<EspacoPacote>) repository.findAllByPacote(pacote);
    }
}
