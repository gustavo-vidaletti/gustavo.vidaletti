package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pagamento;
import br.com.dbccompany.Repository.PagamentoRepository;
import br.com.dbccompany.Service.PagamentoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/pagamento")
public class PagamentoController extends AbstractController<Pagamento, PagamentoRepository, PagamentoService> {
}
