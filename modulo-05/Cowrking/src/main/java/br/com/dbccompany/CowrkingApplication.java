package br.com.dbccompany;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.UsuarioRepository;
import br.com.dbccompany.Service.UsuarioService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CowrkingApplication {
	public static void main(String[] args) {
		SpringApplication.run(CowrkingApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(UsuarioService service){
			Usuario principal = new Usuario();
			principal.setEmail("dbc@admin.com");
			principal.setNome("Administrador");
			principal.setUsername("dbccompany");
			principal.setPassword("dbccompany");
		return args -> {
			service.salvar(principal);
		};
	}
}

