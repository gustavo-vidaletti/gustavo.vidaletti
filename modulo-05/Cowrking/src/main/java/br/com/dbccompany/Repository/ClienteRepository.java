package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

    Cliente findByCpf(String cpf);
}
