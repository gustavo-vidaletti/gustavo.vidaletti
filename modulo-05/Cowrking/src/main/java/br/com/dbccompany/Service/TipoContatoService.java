package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends AbstractService<TipoContatoRepository, TipoContato> {
    public TipoContato buscarPorNome(String nome){
        return repository.findByNome(nome);
    }
}
