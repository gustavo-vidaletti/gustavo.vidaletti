package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "tipo_contato")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TipoContato.class)
public class TipoContato extends AbstractEntity{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(unique = true)
    private String nome;

    @OneToMany(mappedBy = "tipoContato")
    private List<Contato> contatos = new ArrayList<>();

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }
}
