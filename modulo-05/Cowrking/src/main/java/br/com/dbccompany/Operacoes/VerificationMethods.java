package br.com.dbccompany.Operacoes;

import br.com.dbccompany.Entity.Contato;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VerificationMethods {
    public static boolean isTruthy(Object object) {
        if(object == null) return false;
        switch (object.getClass().getSimpleName()){
            case "String": return !object.equals("");
            case "Long": return Long.parseLong(object.toString()) != 0;
            case "List": return ((List<Object>) object).size() > 0;
            case "ArrayList": return ((List<Object>) object).size() > 0;
            case "Double": return Double.parseDouble(object.toString()) != 0;
            case "Integer": return Integer.parseInt(object.toString()) != 0;
            default: return object != null;
        }
    }

    public static boolean isContatoValid(Contato contato){
        switch (contato.getTipoContato().getNome().toLowerCase()){
            case "telefone": return contato.getValor().matches("((\\d){8,12}|(\\+(\\d{13})))");
            case "email": return isEmailValid(contato.getValor());
            default: return true;
        }
    }

    public static boolean isEmailValid(String email){
        return email.matches("(\\w)+((\\.|(_)|(-))(\\w)+)*(@)(\\w)+((\\.)(\\w)+)+");
    }

    public static boolean isCPFValid(String cpf){
        return cpf.matches("(\\d){11}");
    }
}
