package br.com.dbccompany.Security;

import br.com.dbccompany.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userDetailsService")
    UsuarioService usuarioService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
        .disable()
        .authorizeRequests()
        .antMatchers(HttpMethod.POST, "/login").permitAll() // login permitido
        .antMatchers(HttpMethod.POST, "/usuario/**").permitAll()
        .anyRequest().authenticated()
        .and()
        .addFilterAfter(new JWTLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
        .addFilterAfter(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

//        .antMatchers(HttpMethod.GET, "/api/seguradora").permitAll() //metodo get da seguradora
//        .antMatchers("/api/pais").permitAll() //TODOS METODOS dessa url
//        .anyRequest().authenticated() // Para qualquer outra request!
//        .anyRequest().permitAll(); // COMENTAR QUANDO FOR PRA NAO DAR AUTORIZAÇÃO
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService( usuarioService )
                .passwordEncoder( encoder() );
    }

    @Bean
    public PasswordEncoder encoder(){
        return new BCryptPasswordEncoder(6);
    }
}
