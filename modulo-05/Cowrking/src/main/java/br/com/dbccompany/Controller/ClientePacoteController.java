package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Repository.ClientePacoteRepository;
import br.com.dbccompany.Service.ClientePacoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cliente-pacote")
public class ClientePacoteController extends AbstractController<ClientePacote, ClientePacoteRepository, ClientePacoteService> {
}
