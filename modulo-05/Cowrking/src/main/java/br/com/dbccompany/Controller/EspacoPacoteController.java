package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Repository.EspacoPacoteRepository;
import br.com.dbccompany.Service.EspacoPacoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/espaco-pacote")
public class EspacoPacoteController extends AbstractController<EspacoPacote, EspacoPacoteRepository, EspacoPacoteService> {
}
