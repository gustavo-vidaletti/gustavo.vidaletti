package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PagamentoService extends AbstractService<PagamentoRepository, Pagamento> {

    @Autowired
    ClienteService clienteService;

    @Autowired
    ContratacaoService contratacaoService;

    @Autowired
    ClientePacoteService clientePacoteService;

    @Autowired
    EspacoService espacoService;

    @Autowired
    SaldoClienteService saldoClienteService;

    @Autowired
    EspacoPacoteService espacoPacoteService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Pagamento salvar(Pagamento pagamento) throws Exception{
        if(((pagamento.getContratacao() == null) && (pagamento.getClientePacote() == null)) ||
            ((pagamento.getContratacao() != null) && (pagamento.getClientePacote() != null))) {
            throw new Exception("Deve ter Contratacao ou ClientePacote");
        }

        Contratacao contratacao = null;
        ClientePacote clientePacote = null;

        if(pagamento.getContratacao() != null) { // É UMA CONTRATACAO
            pagamento.setContratacao(contratacaoService.buscarPorId(pagamento.getContratacao().getId()));
            contratacao = pagamento.getContratacao();

            long idEspaco = contratacao.getEspaco().getId();
            long idCliente = contratacao.getCliente().getId();

            SaldoCliente saldoCliente = new SaldoCliente();
            SaldoClienteID id = new SaldoClienteID();

            id.setEspaco(espacoService.buscarPorId(idEspaco));
            id.setCliente(clienteService.buscarPorId(idCliente));

            saldoCliente.setTipoContratacao(contratacao.getTipoContratacao());
            saldoCliente.setQuantidade(contratacao.getQuantidade());

            Date date = new Date(System.currentTimeMillis() + (contratacao.getPrazo() * 86400000));
            saldoCliente.setVencimento(date);
            saldoCliente.setId(id);
            if(saldoClienteService.buscarPorId(idCliente, idEspaco) != null){
            saldoClienteService.salvar(saldoCliente);
            } else {
                saldoClienteService.editarPorIds(idCliente,idEspaco,saldoCliente);
            }
        }

        else if(pagamento.getClientePacote() != null) { // É UM PACOTE
            clientePacote = clientePacoteService.buscarPorId(pagamento.getClientePacote().getId());
            Cliente cliente = clientePacote.getCliente();
            Pacote pacote = clientePacote.getPacote();
            List<EspacoPacote> espacoPacotes = espacoPacoteService.buscarTodosPorPacote(pacote);
            for(EspacoPacote ep: espacoPacotes){
                SaldoClienteID id = new SaldoClienteID();
                id.setCliente(cliente);
                id.setEspaco(ep.getEspaco());
                SaldoCliente sc = new SaldoCliente();
                sc.setId(id);
                sc.setQuantidade(clientePacote.getQuantidade());
                sc.setVencimento(new Date(System.currentTimeMillis() + (ep.getPrazo() * 86400000)));
                sc.setTipoContratacao(ep.getTipoContratacao());
                saldoClienteService.salvar(sc);
            }
        }
        return super.salvar(pagamento);
    }
}
