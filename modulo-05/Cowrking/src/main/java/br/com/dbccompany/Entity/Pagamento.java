package br.com.dbccompany.Entity;

import br.com.dbccompany.Enum.TipoPagamento;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pagamento.class)
public class Pagamento extends AbstractEntity{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_cliente_pacote")
    private ClientePacote clientePacote;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_contratacao")
    private Contratacao contratacao;

    @Column(name = "tipo_pagamento")
    private TipoPagamento tipoPagamento;

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClientePacote getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacote clientePacote) {
        this.clientePacote = clientePacote;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

}

