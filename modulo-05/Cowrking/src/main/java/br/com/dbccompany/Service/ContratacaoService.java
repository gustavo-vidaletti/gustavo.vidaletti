package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Operacoes.ReaisParaValor;
import br.com.dbccompany.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.NumberFormat;
import java.util.Locale;

@Service
public class ContratacaoService extends AbstractService<ContratacaoRepository, Contratacao> {

    @Autowired
    EspacoService espacoService;

    @Transactional(rollbackFor = Exception.class)
    public String salvarRetornarValor(Contratacao entidade) {
        entidade.setDesconto(ReaisParaValor.getValor(entidade.getDescontoReais()));
        Espaco espaco = null;
        if(entidade.getEspaco().getId() != 0){
            espaco = espacoService.buscarPorId(entidade.getEspaco().getId());
        } else if(entidade.getEspaco().getNome() != null) {
            espaco = espacoService.buscarPorNome(entidade.getEspaco().getNome());
        }

        Contratacao contratacao = repository.save(entidade);
        double diariaEspaco = 0;
        if(espaco != null) {
            diariaEspaco = espaco.getValor();
        }

        double valorPorTipo;
        switch (entidade.getTipoContratacao()){
            case MES: valorPorTipo = diariaEspaco * 30;
                break;
            case SEMANA: valorPorTipo = diariaEspaco * 7;
                break;
            case DIARIA: valorPorTipo = diariaEspaco;
                break;
            case TURNO: valorPorTipo = diariaEspaco * 5 / 24;
                break;
            case HORA: valorPorTipo = diariaEspaco / 24;
                break;
            case MINUTO: valorPorTipo = diariaEspaco / 1440;
                break;
            default: return "";
        }

        double custo = (1.0 * valorPorTipo * contratacao.getQuantidade()) - contratacao.getDesconto();

        NumberFormat df = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        return df.format(custo);

    }

    @Override
    public Contratacao buscarPorId(long id) {
        Contratacao contratacao = super.buscarPorId(id);
        contratacao.setDescontoReais(ReaisParaValor.getReais((contratacao.getDesconto())));
        return contratacao;
    }
}
