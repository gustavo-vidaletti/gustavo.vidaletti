package br.com.dbccompany.Entity;

import br.com.dbccompany.Operacoes.ReaisParaValor;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pacote.class)
public class Pacote extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    private double valor = 0.0;

    @Transient
    private String valorReais = "";

    @OneToMany(mappedBy = "pacote")
    private List<EspacoPacote> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacote")
    private List<ClientePacote> clientesPacotes = new ArrayList<>();

    // Getters and Setters

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
        this.valorReais = ReaisParaValor.getReais(valor);
    }

    public List<EspacoPacote> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacote> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientePacote> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacote> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public void pushClientesPacotes(ClientePacote... clientesPacotes){
        this.clientesPacotes.addAll(Arrays.asList(clientesPacotes));
    }

    public void pushEspacosPacotes(EspacoPacote... espacosPacotes){
        this.espacosPacotes.addAll(Arrays.asList(espacosPacotes));
    }

    public String getValorReais() {
        return valorReais;
    }

    public void setValorReais(String valorReais) {
        this.valorReais = valorReais;
        this.valor = ReaisParaValor.getValor(valorReais);
    }
}
