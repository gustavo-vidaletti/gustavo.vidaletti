package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Operacoes.Criptografia;
import br.com.dbccompany.Operacoes.VerificationMethods;
import br.com.dbccompany.Repository.UsuarioRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("userDetailsService")
public class UsuarioService extends AbstractService<UsuarioRepository, Usuario> implements UserDetailsService {

    @Override
    public Usuario salvar(Usuario usuario) throws Exception {
        String senha = usuario.getPassword();
        if(senha.length() < 6){
            throw new Exception("Senha inválida");
        }

        if(!VerificationMethods.isEmailValid(usuario.getEmail())){
            throw new Exception("Email inválido");
        }

        usuario.setPassword(Criptografia.criptografarSenha(usuario.getPassword()));
        return repository.save(usuario);
    }

    public Usuario buscarPorUsername(String username){
        return repository.findByUsername(username);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = repository.findByUsername(username);

        if( usuario == null ){
            throw new UsernameNotFoundException("login " + username + "não registrado");
        }
        Set<GrantedAuthority> permissoes = Stream
                .of( new SimpleGrantedAuthority( usuario.getNome() ) )
                .collect(Collectors.toSet() );
        return new User( usuario.getUsername(), usuario.getPassword(), permissoes);
    }
}
