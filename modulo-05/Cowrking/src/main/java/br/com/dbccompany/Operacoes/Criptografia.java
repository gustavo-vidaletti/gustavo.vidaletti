package br.com.dbccompany.Operacoes;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Criptografia {
    public static String md5 (String input){
        if(input == null) return null;
        String md5 = null;
        try{
            MessageDigest digest = MessageDigest.getInstance("MD5");

            digest.update(input.getBytes(), 0, input.length());

            md5 = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Erro ao criar criptografia");
            return  null;
        }
        return md5;
    }

    public static String criptografarSenha(String senha){
        return new BCryptPasswordEncoder(6).encode(senha);
    }

    public static BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder(6);
    }
}