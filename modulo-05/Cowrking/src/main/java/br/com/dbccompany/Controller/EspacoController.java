package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Repository.EspacoRepository;
import br.com.dbccompany.Service.EspacoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/espaco")
public class EspacoController extends AbstractController<Espaco, EspacoRepository, EspacoService> {

    @Override
    @GetMapping(value = "/{param}")
    public Espaco buscar(@PathVariable String param) {
        try{
            long id = Long.parseLong(param);
            return entidadeService.buscarPorId(id);
        } catch (NumberFormatException erro){
            return entidadeService.buscarPorNome(param);
        }
    }
}
