package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.ClientePacote;
import org.springframework.data.repository.CrudRepository;

public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Long> {
}
