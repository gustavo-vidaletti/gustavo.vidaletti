package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoClienteID;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, SaldoClienteID> {

    List<SaldoCliente> findAllById_Espaco(long idEspaco);

    List<SaldoCliente> findAllById_Cliente(long idCliente);
}
