package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Enum.TipoContratacao;
import br.com.dbccompany.Enum.TipoPagamento;
import br.com.dbccompany.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AcessoService extends AbstractService<AcessoRepository, Acesso> {

    @Autowired
    ClienteService clienteService;

    @Autowired
    EspacoService espacoService;

    @Autowired
    SaldoClienteService saldoClienteService;

    @Autowired
    ContratacaoService contratacaoService;

    @Autowired
    PagamentoService pagamentoService;

    @Transactional(rollbackFor = Exception.class)
    public String acessar(Acesso acesso) throws Exception {
        long idCliente = acesso.getSaldoCliente().getId().getCliente().getId();
        long idEspaco = acesso.getSaldoCliente().getId().getEspaco().getId();
        SaldoCliente sc;
        List<Acesso> acessos;
        if(acesso.getData() == null) {
            acesso.setData(new Date(System.currentTimeMillis()));
        }

        if(acesso.getIsExcecao() && acesso.getIsEntrada()){
            SaldoClienteID id = new SaldoClienteID();
            id.setCliente(clienteService.buscarPorId(idCliente));
            id.setEspaco(espacoService.buscarPorId(idEspaco));
            sc = new SaldoCliente();
            sc.setId(id);
            sc.setQuantidade(999);
            sc.setVencimento(new Date(System.currentTimeMillis() + 86400000));
            sc.setTipoContratacao(TipoContratacao.ESPECIAL);
            if(saldoClienteService.buscarPorId(idCliente, idEspaco) != null){
                sc = saldoClienteService.salvar(sc);
            } else {
                sc = saldoClienteService.editarPorIds(idCliente,idEspaco,sc);
            }

            acesso.setSaldoCliente(sc);
            repository.save(acesso);
            return "Especial!";
        }
        //TODO: -------------------------------------------------------------------------------------------------------
        sc = saldoClienteService.buscarPorId(idCliente, idEspaco);

        if(sc == null) {
            return "Não encontrado cadastro!";
        }

        acesso.setSaldoCliente(sc);

        acessos = this.buscarTodosDoMesmoClienteEEspaco(
                clienteService.buscarPorId(idCliente),
                espacoService.buscarPorId(idEspaco)
        );
        Acesso ultimo = null;
        if(acesso.getIsEntrada()){ //TODO: se for entrada
            if(acessos.size() > 0) {
                ultimo = acessos.get(acessos.size() - 1);
                if(ultimo.getIsEntrada()){
                    return "Entrada duplicada!";
                }
            }

            if(sc.getVencimento().before(new Date(System.currentTimeMillis()))){
                sc.setQuantidade(0);
                saldoClienteService.editarPorIds(idCliente,idEspaco,sc);
                return "Saldo Vencido.";
            } else if(sc.getQuantidade() == 0) {
                return "Saldo Insuficiente.";
            }

            repository.save(acesso);
            return "Saldo atual: " + sc.getQuantidade() + " acessos do tipo " + sc.getTipoContratacao() + ".";
            //TODO: ---------------------------------------------------------------------------------------------------
        } else { //TODO: se for saída
            if(acessos.size() > 0) {
                ultimo = acessos.get(acessos.size() -1);
                if(!ultimo.getIsEntrada()){
                    return "Entrada não registrada!";
                }
            }

            long diferencaEmMillis = acesso.getData().getTime() - ultimo.getData().getTime();
            double diferencaEmMinutos = diferencaEmMillis / 60000.0;
            double diferencaEmDias = diferencaEmMinutos / 1440.0;
            int desconto = 0;
            switch (sc.getTipoContratacao()){
                case MINUTO:
                    desconto = (int)Math.ceil(diferencaEmMinutos);
                    break;
                case HORA:
                    desconto = (int)Math.ceil(diferencaEmMinutos / 60.0);
                    break;
                case TURNO:
                    desconto = (int)Math.ceil(diferencaEmMinutos / (60 * 5.0));
                    break;
                case DIARIA:
                    desconto = (int)Math.ceil(diferencaEmDias);
                    break;
                case SEMANA:
                    desconto = (int)Math.ceil(diferencaEmDias / 7.0);
                    break;
                case MES:
                    desconto = (int)Math.ceil(diferencaEmDias / 30.0);
                    break;
                case ESPECIAL:
                    TipoContratacao tc = TipoContratacao.MINUTO;
                    desconto = (int)Math.ceil(diferencaEmMinutos);
                    if(desconto > 40){
                        tc = TipoContratacao.HORA;
                        desconto = (int)Math.ceil(diferencaEmMinutos / 60.0);
                    }
                    sc.setTipoContratacao(tc);
                    sc.setVencimento(new Date());
                    sc.setQuantidade(0);
                    saldoClienteService.editarPorIds(idCliente,idEspaco,sc);

                    Contratacao contratacao = new Contratacao();
                    contratacao.setCliente(clienteService.buscarPorId(idCliente));
                    contratacao.setEspaco(espacoService.buscarPorId(idEspaco));
                    contratacao.setTipoContratacao(tc);
                    contratacao.setQuantidade(Math.abs(desconto));
                    contratacao.setDesconto(0);
                    contratacao.setPrazo(1);
                    String valor = contratacaoService.salvarRetornarValor(contratacao);
                    List<Contratacao> contratacoes = contratacaoService.listarTodos();
                    contratacao = contratacoes.get(contratacoes.size() - 1);
                    Pagamento pagamento = new Pagamento();
                    pagamento.setContratacao(contratacao);
                    pagamento.setTipoPagamento(TipoPagamento.DEBITO);
                    pagamentoService.salvar(pagamento);
                    return valor;
            }

            sc.setQuantidade(sc.getQuantidade() - desconto);
            saldoClienteService.editarPorIds(idCliente,idEspaco,sc);
            repository.save(acesso);
            return "Saldo atual: " + sc.getQuantidade() + " do tipo: " + sc.getTipoContratacao() + ".";
            //TODO: ---------------------------------------------------------------------------------------------------
        }  // É saída
    }

    public List<Acesso> buscarTodosDoMesmoClienteEEspaco(Cliente cliente, Espaco espaco){
        return repository.findAllBySaldoCliente_Id_ClienteAndSaldoCliente_Id_Espaco(cliente, espaco);
    }
}
