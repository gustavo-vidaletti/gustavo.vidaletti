package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ContatoRepository;
import br.com.dbccompany.Service.ContatoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/contato")
public class ContatoController extends AbstractController<Contato, ContatoRepository, ContatoService> {
}
