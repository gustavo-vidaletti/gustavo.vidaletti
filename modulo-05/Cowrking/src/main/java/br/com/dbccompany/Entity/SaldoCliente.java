package br.com.dbccompany.Entity;

import br.com.dbccompany.Enum.TipoContratacao;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "saldo_cliente")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SaldoCliente.class)
public class SaldoCliente {

    @EmbeddedId
    private SaldoClienteID id;

    @Column(name = "tipo_contratacao", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    private Date vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    private List<Acesso> acessos = new ArrayList<>();

    // Getters and Setters

    public SaldoClienteID getId() {
        return id;
    }

    public void setId(SaldoClienteID id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acesso> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acesso> acessos) {
        this.acessos = acessos;
    }

    public void pushAcessos(Acesso... acessos) {
        this.acessos.addAll(Arrays.asList(acessos));
    }
}
