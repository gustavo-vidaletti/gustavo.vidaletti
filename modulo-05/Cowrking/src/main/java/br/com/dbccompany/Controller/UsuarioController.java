package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.UsuarioRepository;
import br.com.dbccompany.Service.UsuarioService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/usuario")
public class UsuarioController extends AbstractController<Usuario, UsuarioRepository, UsuarioService>{

    @Override
    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public boolean remover(@PathVariable long id) throws Exception{
        if(id != 1) return super.remover(id);
        else return false;
    }

    @Override
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Usuario editar(@PathVariable long id, @RequestBody Usuario entidade) throws Exception{
        if(id == 1) return null;
        return entidadeService.editarPorId(id, entidade);
    }
}
