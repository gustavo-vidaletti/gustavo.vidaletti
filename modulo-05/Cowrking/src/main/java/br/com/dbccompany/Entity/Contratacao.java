package br.com.dbccompany.Entity;

import br.com.dbccompany.Enum.TipoContratacao;
import br.com.dbccompany.Operacoes.ReaisParaValor;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Contratacao.class)
public class Contratacao extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_espaco")
    private Espaco espaco;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    @Column(name = "tipo_contratacao")
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private int quantidade = 0;

    private double desconto = 0.0;

    @Transient
    private String descontoReais = "";

    @Column(nullable = false)
    private int prazo = 0;

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
        this.descontoReais = ReaisParaValor.getReais(desconto);
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public String getDescontoReais() {
        return descontoReais;
    }

    public void setDescontoReais(String descontoReais) {
        this.descontoReais = descontoReais;
        this.desconto = ReaisParaValor.getValor(descontoReais);
    }
}
