package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Operacoes.VerificationMethods;
import br.com.dbccompany.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContatoService extends AbstractService<ContatoRepository, Contato> {

    @Autowired
    TipoContatoService tipoContatoService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Contato salvar(Contato contato) throws Exception{
        if(contato.getTipoContato() == null) {
            throw new Exception("Contato sem tipo!");
        }
        if (VerificationMethods.isTruthy(contato.getTipoContato().getId())) {
            long id = contato.getTipoContato().getId();
            contato.setTipoContato(tipoContatoService.buscarPorId(id));
        } else if (contato.getTipoContato().getNome() != null) {
            String nome = contato.getTipoContato().getNome();
            contato.setTipoContato(tipoContatoService.buscarPorNome(nome));
        }

        if(!VerificationMethods.isContatoValid(contato)){
            throw new Exception("Contato do tipo " + contato.getTipoContato().getNome() + " inválido" );
        }

        return super.salvar(contato);
    }
}


