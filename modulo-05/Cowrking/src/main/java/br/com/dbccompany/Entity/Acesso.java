package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Acesso.class)
public class Acesso extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns({
            @JoinColumn(name = "id_espaco_saldo_cliente", referencedColumnName = "id_espaco"),
            @JoinColumn(name = "id_cliente_saldo_cliente", referencedColumnName = "id_cliente")
    })
    private SaldoCliente saldoCliente;

    //@JsonProperty("excecao")
    @Column(name = "is_entrada")
    private boolean isEntrada = false;

//    @JsonProperty("excecao")
    @Column(name = "is_excecao")
    private boolean isExcecao = false;

    private Date data = new Date();

    //Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean getIsEntrada() {
        return isEntrada;
    }

    public void setIsEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public boolean getIsExcecao() {
        return isExcecao;
    }

    public void setIsExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
