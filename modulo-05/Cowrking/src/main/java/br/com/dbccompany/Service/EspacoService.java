package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Operacoes.ReaisParaValor;
import br.com.dbccompany.Repository.EspacoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EspacoService extends AbstractService<EspacoRepository, Espaco> {

    public Espaco buscarPorNome(String nome){
        Espaco espaco = repository.findByNome(nome);
        espaco.setValorReais(ReaisParaValor.getReais(espaco.getValor()));
        return espaco;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Espaco salvar(Espaco espaco) throws Exception{
        espaco.setValor(ReaisParaValor.getValor(espaco.getValorReais()));
        return super.salvar(espaco);
    }

    @Override
    public Espaco buscarPorId(long id) {
        Espaco espaco =  super.buscarPorId(id);
        espaco.setValorReais(ReaisParaValor.getReais(espaco.getValor()));
        return espaco;
    }
}
