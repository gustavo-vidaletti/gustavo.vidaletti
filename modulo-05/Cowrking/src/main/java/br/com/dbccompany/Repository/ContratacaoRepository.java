package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Long> {
}
