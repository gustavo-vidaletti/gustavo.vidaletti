package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Long> {

    List<EspacoPacote> findAllByPacote(Pacote pacote);
}
