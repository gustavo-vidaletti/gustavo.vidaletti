package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Espaco;
import org.springframework.data.repository.CrudRepository;

public interface EspacoRepository extends CrudRepository<Espaco, Long> {
    Espaco findByNome(String nome);
}
