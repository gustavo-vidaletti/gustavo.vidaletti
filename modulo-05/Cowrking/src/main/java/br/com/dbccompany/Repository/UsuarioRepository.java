package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findById(long id);

    Usuario findByEmail(String email);

    List<Usuario> findAllByNome(String nome);

    Usuario findByUsername(String login);

}
