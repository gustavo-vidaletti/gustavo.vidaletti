package br.com.dbccompany.Requisitos;

import br.com.dbccompany.CowrkingApplicationTests;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Service.EspacoService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class EspacoTests extends CowrkingApplicationTests {

    @Autowired
    private EspacoService espacoService;

    @Autowired
    ApplicationContext context;

    @Test
    public void nomeUnico(){
        Espaco espaco1 = new Espaco();
        espaco1.setValor(20);
        espaco1.setNome("mesa 1");
        espaco1.setQtdPessoas(2);
        try {
            espacoService.salvar(espaco1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Espaco espaco2 = new Espaco();
        espaco2.setValor(20);
        espaco2.setNome("mesa 1");
        espaco2.setQtdPessoas(2);
        try {
            espacoService.salvar(espaco2);
        } catch (Exception e) {
            Assert.assertEquals(DataIntegrityViolationException.class, e.getClass());
        }
    }

    @Test
    public void todosOsCamposObrigatorios(){
        Espaco espaco = new Espaco();
        try {
            espacoService.salvar(espaco);
        } catch (Exception e) {
            Assert.assertEquals(DataIntegrityViolationException.class, e.getClass());
        }
    }
}
