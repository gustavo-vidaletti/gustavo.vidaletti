package br.com.dbccompany.Requisitos;


import br.com.dbccompany.CowrkingApplicationTests;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Service.ContatoService;
import br.com.dbccompany.Service.EspacoService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ContatoTests extends CowrkingApplicationTests {

    @Autowired
    private ContatoService contatoService;

    @Autowired
    ApplicationContext context;

    @Test
    public void verificacaoTelefone(){
        TipoContato telefone = new TipoContato();
        telefone.setNome("telefone");
        Contato contato = new Contato();
        contato.setTipoContato(telefone);
        contato.setValor("13456");
        try {
            contatoService.salvar(contato);
        } catch (Exception e) {
            Assert.assertEquals("Contato do tipo " + contato.getTipoContato().getNome() + " inválido", e.getMessage());
        }

    }

    @Test
    public void verificacaoEmail(){
        TipoContato email = new TipoContato();
        email.setNome("email");
        Contato contato = new Contato();
        contato.setTipoContato(email);
        contato.setValor("23098147");
        try {
            contatoService.salvar(contato);
        } catch (Exception e) {
            Assert.assertEquals("Contato do tipo " + contato.getTipoContato().getNome() + " inválido", e.getMessage());
        }
    }
}
