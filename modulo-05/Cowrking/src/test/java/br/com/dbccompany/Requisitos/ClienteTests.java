package br.com.dbccompany.Requisitos;


import br.com.dbccompany.CowrkingApplicationTests;
import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Service.ClienteService;
import br.com.dbccompany.Service.TipoContatoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
public class ClienteTests extends CowrkingApplicationTests {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Autowired
    ApplicationContext context;

    @Test
    public void todosOsCamposObrigatorios() {
        Cliente cliente1 = new Cliente();
        cliente1.setNome("Gustavo");
        try {
            clienteRepository.save(cliente1);
            List<Cliente> clientes = (List<Cliente>) clienteRepository.findAll();
            Assert.assertEquals(0, clientes.size());
        } catch (DataIntegrityViolationException ex){
            System.err.println("Erro de chave");
            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void validacaoCPF(){
        Cliente cliente1 = new Cliente();
        cliente1.setNome("Gustavo");
        cliente1.setDataNascimento(new Date());
        cliente1.setCpf("0412881");

        try {
            clienteService.salvar(cliente1);
        } catch (Exception e){
            Assert.assertEquals("CPF Não é válido", e.getMessage());
        }
    }

    @Test
    public void cpfUnico(){
        TipoContato tel = new TipoContato();
        tel.setNome("telefone");
        TipoContato email = new TipoContato();
        email.setNome("email");
        Cliente cliente1 = new Cliente();
        Cliente cliente2 = new Cliente();

        cliente1.setNome("Gustavo");
        cliente2.setNome("Antônio");

        cliente1.setDataNascimento(new Date());
        cliente2.setDataNascimento(new Date());

        cliente1.setCpf("04129574001");
        cliente2.setCpf("04129574001");

        Contato e = new Contato();
        Contato t = new Contato();
        e.setTipoContato(tel);
        e.setValor("09128301");
        t.setTipoContato(email);
        t.setValor("email@email.com");
        cliente1.pushContatos(e);
        cliente2.pushContatos(e);
        cliente2.pushContatos(t);
        cliente1.pushContatos(t);

        try {
            clienteService.salvar(cliente1);
            clienteService.salvar(cliente2);
        } catch (Exception ex){
            Assert.assertEquals("CPF já registrado", ex.getMessage());
        }
    }

    @Test
    public void peloMenosUmEmailEUmTelefone(){
        TipoContato tel = new TipoContato();
        tel.setNome("telefone");
        TipoContato email = new TipoContato();
        email.setNome("email");
        try{
            tipoContatoService.salvar(tel);
            tipoContatoService.salvar(email);
        } catch(Exception e){
            e.printStackTrace();
        }
        Cliente cliente1 = new Cliente();
        Cliente cliente2 = new Cliente();
        cliente1.setNome("Gustavo");
        cliente2.setNome("Antônio");
        cliente1.setDataNascimento(new Date());
        cliente2.setDataNascimento(new Date());
        cliente1.setCpf("04104104122");
        cliente2.setCpf("04122204100");
        Contato e = new Contato();
        Contato t = new Contato();
        e.setTipoContato(tel);
        e.setValor("09128301");
        t.setTipoContato(email);
        t.setValor("email@email.com");
        cliente1.pushContatos(e);
        cliente2.pushContatos(t);

        try{
            clienteService.salvar(cliente1);
        }catch (Exception erro){
            Assert.assertEquals("Deve ter um telefone e um email", erro.getMessage());
        }

        try{
            clienteService.salvar(cliente2);
        }catch (Exception erro){
            Assert.assertEquals("Deve ter um telefone e um email", erro.getMessage());
        }
    }
}