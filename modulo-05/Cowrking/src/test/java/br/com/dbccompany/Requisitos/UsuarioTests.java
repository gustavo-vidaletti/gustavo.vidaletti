package br.com.dbccompany.Requisitos;

import br.com.dbccompany.CowrkingApplicationTests;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Operacoes.Criptografia;
import br.com.dbccompany.Operacoes.VerificationMethods;
import br.com.dbccompany.Service.UsuarioService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLIntegrityConstraintViolationException;

@RunWith(SpringRunner.class)
public class UsuarioTests extends CowrkingApplicationTests {
    @Autowired
    UsuarioService usuarioService;

    @Test
    public void senhaMaiorQueSeisDigitos(){
        Usuario usuario = new Usuario();
        usuario.setNome("teste");
        usuario.setPassword("teste");
        usuario.setUsername("teste");
        usuario.setEmail("teste@teste.com");
        try {
            usuarioService.salvar(usuario);
        } catch (Exception e) {
            Assert.assertEquals("Senha inválida", e.getMessage());
        }
    }

    @Test
    public void emailValid(){
        Usuario usuario = new Usuario();
        usuario.setNome("teste");
        usuario.setPassword("testeee");
        usuario.setUsername("testeee");
        usuario.setEmail("teste");
        try {
            usuarioService.salvar(usuario);
        } catch (Exception e) {
            Assert.assertEquals("Email inválido", e.getMessage());
        }
    }

    @Test
    public void senhaCriptografada(){
        Usuario usuario = new Usuario();
        usuario.setNome("teste");
        usuario.setPassword("testezinho");
        usuario.setUsername("testes");
        usuario.setEmail("teste@test.com");
        try {
            usuario = usuarioService.salvar(usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertTrue(Criptografia.encoder().matches("testezinho", usuario.getPassword() ));

    }

    @Test
    public void emailUnico(){
        Usuario usu1 = new Usuario();
        usu1.setNome("teste");
        usu1.setPassword("testezinho");
        usu1.setUsername("teste1");
        usu1.setEmail("teste@teste.com");
        try {
            usuarioService.salvar(usu1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Usuario usu2 = new Usuario();
        usu2.setNome("teste");
        usu2.setPassword("testezinho");
        usu2.setUsername("teste2");
        usu2.setEmail("teste@teste.com");
        try {
            usuarioService.salvar(usu2);
        } catch (Exception e) {
            Assert.assertEquals(DataIntegrityViolationException.class, e.getClass());
        }
    }

    @Test
    public void loginUnico(){
        Usuario usu1 = new Usuario();
        usu1.setNome("teste");
        usu1.setPassword("testezinho");
        usu1.setUsername("teste");
        usu1.setEmail("teste1@teste.com");
        try {
            usuarioService.salvar(usu1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Usuario usu2 = new Usuario();
        usu2.setNome("teste");
        usu2.setPassword("testezinho");
        usu2.setUsername("teste");
        usu2.setEmail("teste2@teste.com");
        try {
            usuarioService.salvar(usu2);
        } catch (Exception e) {
            Assert.assertEquals(DataIntegrityViolationException.class, e.getClass());
        }
    }



}
