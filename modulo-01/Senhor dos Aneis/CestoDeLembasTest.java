
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CestoDeLembasTest
{
    @Test
    public void conferirAdicionarLembas(){
        CestoDeLembas teste = new CestoDeLembas(4);
        teste.adicionarLembas(1);
        assertEquals(5, teste.getQtd());
        teste.adicionarLembas(95);
        assertEquals(100, teste.getQtd());
        teste.adicionarLembas(4);
        assertEquals(100, teste.getQtd());
        teste.adicionarLembas(-4);
        assertEquals(100, teste.getQtd());

    }

    @Test
    public void conferirRemoverLembas(){
        CestoDeLembas teste = new CestoDeLembas(4);
        teste.retirarLembas(4);

        assertTrue(teste.taVazio());        
        assertFalse(teste.retirarLembas(1));

    }

    @Test
    public void conferirPodeDividirPares(){
        CestoDeLembas teste;
        teste = new CestoDeLembas(1);
        assertFalse(teste.podeDividirEmPares()); //1
        teste.adicionarLembas(1);
        assertFalse(teste.podeDividirEmPares()); //2
        teste.adicionarLembas(1);
        assertFalse(teste.podeDividirEmPares()); //3
        teste.adicionarLembas(1);
        assertTrue(teste.podeDividirEmPares()); //4
        teste.adicionarLembas(1);
        assertFalse(teste.podeDividirEmPares()); //5
        teste.adicionarLembas(1);
        assertTrue(teste.podeDividirEmPares()); //6
        teste.adicionarLembas(1);
        assertFalse(teste.podeDividirEmPares()); //7
        teste.adicionarLembas(1);
        assertTrue(teste.podeDividirEmPares()); //8
        teste.adicionarLembas(4);
        assertTrue(teste.podeDividirEmPares()); //12
        teste.adicionarLembas(12);
        assertTrue(teste.podeDividirEmPares()); //24

        teste = new CestoDeLembas(20);
        assertTrue(teste.podeDividirEmPares()); //20

        teste = new CestoDeLembas(100);
        assertTrue(teste.podeDividirEmPares()); // 100

    }
}
