import java.util.*;

public class NoturnosPorUltimo extends OperacoesTaticas {
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> resultado = new ArrayList<>();
        this.lista = this.removerInaptos(atacantes);
        
        Elfo elfo = getPrimeiroVerde();
        while(elfo != null){
            resultado.add(elfo);
            elfo = this.getPrimeiroVerde();
        }
        elfo = getPrimeiroNoturno();
        while(elfo != null){
            resultado.add(elfo);
            elfo = this.getPrimeiroNoturno();
        }
        return resultado;
    }
}


