import java.util.ArrayList;
public class PaginadorInventario
{
    private Inventario inv;
    private int marcadorInicial;
    
    public PaginadorInventario(Inventario inv)
    {
        this.inv = inv;
        this.marcadorInicial = 0;
    }
    
    public void pular(int n){
        this.marcadorInicial = n > 0 ? n : 0;
    }
    
    public ArrayList<Item> limitar(int n){
        ArrayList<Item> pagina = new ArrayList<>();
        for (int i = this.marcadorInicial; (i < n + this.marcadorInicial) && (i < inv.getQtdItens()) ; i++){
            pagina.add(this.inv.obter(i));
        }
        return pagina;
    }
}