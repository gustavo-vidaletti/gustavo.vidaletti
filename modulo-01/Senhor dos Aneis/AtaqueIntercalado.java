import java.util.ArrayList;

public class AtaqueIntercalado extends OperacoesTaticas{
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> resultado = new ArrayList<>();
        this.lista = this.removerInaptos(atacantes);

        Elfo elfoVerde = this.getPrimeiroVerde();
        Elfo elfoNoturno = this.getPrimeiroNoturno();
        while(elfoVerde != null || elfoNoturno != null){
            if(elfoVerde != null) resultado.add(elfoVerde);
            if(elfoNoturno != null) resultado.add(elfoNoturno);
            elfoVerde = this.getPrimeiroVerde();
            elfoNoturno = this.getPrimeiroNoturno();
        }
        return resultado;
    }
}