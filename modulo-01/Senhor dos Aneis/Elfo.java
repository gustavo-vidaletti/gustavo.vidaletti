public class Elfo extends Personagem {
    {
        this.quantidadeVida = 100;
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
    }

    public Elfo(String nome) {
        super(nome);
        Contador.contadorDeElfo++;
    }

    public void atirarFlecha(Dwarf alvo){
        this.atirarFlechaGanharExp(alvo,1, "Flecha", "Arco");
    }

    protected void atirarFlechaGanharExp(Dwarf alvo, int exp, String descricaoFlecha, String descricaoArco){
        Item flecha = this.inventario.buscar(descricaoFlecha);
        if(flecha == null) return;
        int qtdAtual = flecha.getQuantidade();
        if(this.podeAtirarFlechaPorDescricao(descricaoFlecha, descricaoArco) && alvo.tomouFlechada()){
            flecha.setQuantidade(--qtdAtual);
            if(flecha.getQuantidade() == 0) this.perdeItem(flecha);
            this.ganharExp(exp);
        }
    }
    //DRY - Don't Repeat Yourself

    public String imprimirResumo(){
        return "Elfo";
    }
}
