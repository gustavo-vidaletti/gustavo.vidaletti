public class ElfoDaLuz extends Elfo {
    /*
     *  permita que eles nasçam com uma espada de Galvorn 
     *  (resistente metal élfico criado nas Montanhas Azuis, durante a Primeira Era) 
     *  e que possam atacar dwarves com essa espada, sem que ela quebre ou seja perdida 
     *  do inventário. Porém, a cada ataque de número ímpar, o ElfoDaLuz perde 21 unidades 
     *  de vida, e cada ataque de número par, ganha 10 unidades de vida.
     */
    boolean ataquePar = false;
    
    {
        this.inventario.adicionar(new Item(1, "Espada de Galvorn"));
    }
    
    public ElfoDaLuz(String nome){
        super(nome);
    }
    
    public boolean atacarComEspada(Dwarf alvo){
        if(this.podeAtacarComEspada() && alvo.tomouEspadada()){
            this.ganharExp(1); // <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- 
            double danoATomar = (this.ataquePar) ? -10.0 : 21.0;
            this.sofrerDano(danoATomar);
            this.toggleAtaque();
            return true;
        }
        return false;
    }
    
    private void toggleAtaque(){
        this.ataquePar = !this.ataquePar;
    }
}
