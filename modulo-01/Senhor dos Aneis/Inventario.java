import java.util.ArrayList;

public class Inventario
{
    private int ultimoNull = 0;
    private ArrayList<Item> inventario;

    public Inventario(){
        this.inventario = new ArrayList<>();
    }

    //Getters

    public int getQtdItens(){
        return this.inventario.size();
    }

    public ArrayList<Item> getItens(){
        return this.inventario;
    }

    private boolean contem(Item i){
        for(Item item: this.getItens())if(item.equals(i)) return true;
        return false;
    }

    //Outros Metodos

    public boolean adicionar(Item item){
        Item itemComMesmaDescricao = this.buscar(item.getDescricao());
        if(itemComMesmaDescricao != null){
            itemComMesmaDescricao.setQuantidade(itemComMesmaDescricao.getQuantidade() + item.getQuantidade());
            return true;
        }
        return this.inventario.add(item);
    }

    public Item obter(int posicao){
        if(posicao >= this.inventario.size() || posicao < 0) return null;
        return this.inventario.get(posicao);
    }

    public Item buscar(String descricao){
        for(Item i: this.inventario) if(i.getDescricao().equals(descricao)) return i;
        return null;
    }

    public boolean remover(int posicao){
        if(posicao < 0 || posicao > this.inventario.size()) return false;
        this.inventario.remove(posicao);
        return true;
    }

    public boolean remover(Item item){
        return this.inventario.remove(item);
    }

    public Item getItemComMaiorQuantidade(){
        Item aux = new Item(0, "Teste");
        for (Item i: this.inventario){
            if(i.getQuantidade() > aux.getQuantidade()) aux = i;
        }
        return aux;
    }

    public Item getItemComMenorQuantidade(){
        Item aux = new Item(Integer.MAX_VALUE, "Teste");
        for (Item i: this.inventario){
            if(i.getQuantidade() < aux.getQuantidade()) aux = i;
        }
        return aux;
    }

    public String getDescricoesItens(){
        StringBuilder ab = new StringBuilder();
        for(Item i: this.inventario){
            ab.append(i.getDescricao());
            if(this.inventario.indexOf(i) < this.inventario.size()-1) ab.append(", ");
        }
        return ab.toString();
    }

    public ArrayList<Item> inverter(){
        ArrayList<Item> aux = new ArrayList<>();
        for(int i = this.inventario.size() - 1; i >= 0; i--){
            aux.add(this.inventario.get(i));
        }
        return aux;
    }

    public void ordenarItens(TipoOrdenacao tipo){
        ArrayList<Item> aux = new ArrayList();
        int tamanho = this.inventario.size();
        for(int i = 0; i < tamanho; i++){
            Item itemAux = null;
            switch (tipo){
                case ASC: itemAux = this.getItemComMenorQuantidade();
                break;
                case DESC: itemAux = this.getItemComMaiorQuantidade();
                break;
            }
            aux.add(itemAux);
            this.inventario.remove(itemAux);
        }
        this.inventario = aux;
    }

    public void ordenarItens(){ //ASCENDENTE
        this.ordenarItens(TipoOrdenacao.ASC);
    }

    public void ordenarItensBubble(TipoOrdenacao ordenacao){
        for(int i = 0; i < this.inventario.size(); i++){
            for(int j = 0; j < this.inventario.size() - 1; j++){
                Item atual = this.inventario.get(j);
                Item proximo = this.inventario.get(j + 1);

                boolean deveTrocar;

                switch(ordenacao){
                    case ASC: deveTrocar = (atual.getQuantidade() > proximo.getQuantidade());
                    break;
                    case DESC: deveTrocar = (atual.getQuantidade() < proximo.getQuantidade());
                    break;

                    default: deveTrocar = false;
                }

                if(deveTrocar){
                    //Item laranja = atual
                    this.inventario.set(j,proximo);
                    this.inventario.set(j + 1, atual);
                }
            }
        }
    }

    public Inventario unir(Inventario outro){
        Inventario resultado = new Inventario();
        for(Item item: this.getItens()) resultado.adicionar(item);
        for(Item item: outro.getItens()) resultado.adicionar(item);
        resultado.ordenarItens(TipoOrdenacao.ASC);
        return resultado;
    }

    public Inventario diferenciar(Inventario outro){
        Inventario resultado = new Inventario();
        for(Item item: this.getItens()){
            boolean adiciono = true;
            for(Item i: outro.getItens()){
                if(item.equals(i)){
                    adiciono = false;
                    break;
                }
            }
            if(adiciono) resultado.adicionar(item);
        }
        return resultado;
    }

    public Inventario cruzar(Inventario outro){
        Inventario resultado = new Inventario();
        for(Item item: this.getItens()){
            for(Item i: outro.getItens()){
                if(item.equals(i)){
                    resultado.adicionar(item);
                    break;
                }
            }
        }
        resultado.ordenarItens(TipoOrdenacao.ASC);
        return resultado;
    }

    //DRY - Don't Repeat Yourself

}