import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
public class PaginadorInventarioTest
{
    @Test
    public void pularLimitarDentroDosLimites(){
        Inventario inventario = new Inventario();
        Item it1 = new Item(1, "Espada");
        Item it2 = new Item(2, "Escudo de metal");
        Item it3 = new Item(3, "Poção de HP");
        Item it4 = new Item(4, "Bracelete");
        inventario.adicionar(it1);
        inventario.adicionar(it2);
        inventario.adicionar(it3);
        inventario.adicionar(it4);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> esperado = new ArrayList<>();
        esperado.add(it1);
        esperado.add(it2);
        assertEquals(esperado, paginador.limitar(2));
        //Teste 2
        esperado = new ArrayList<>();
        esperado.add(it3);
        esperado.add(it4);
        paginador.pular(2);
        assertEquals(esperado, paginador.limitar(2));
        assertEquals(esperado, paginador.limitar(3));
    }

    @Test
    public void pularLimitarSemItens(){
        Inventario inventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertTrue(primeiraPagina.isEmpty());
    }

    @Test
    public void pularLimitarComUmItem(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(2, "Hand Grenade"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertEquals(1, primeiraPagina.size());
    }

    @Test
    public void pularLimitarComDoisItens(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(2, "Hand Grenade"));
        inventario.adicionar(new Item(1, "M4A4"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        assertEquals(2,primeiraPagina.size());
    }

    @Test
    public void pularLimitarForaDosLimites(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(2, "Hand Grenade"));
        inventario.adicionar(new Item(1, "M4A4"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> primeiraPagina = paginador.limitar(3);
        assertEquals(2,primeiraPagina.size());
    }
    
    @Test
    public void pularNegativo(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(2, "Hand Grenade"));
        inventario.adicionar(new Item(1, "M4A4"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(-2);
        ArrayList<Item> primeiraPagina = paginador.limitar(3);
        assertEquals(2,primeiraPagina.size());
    }
}