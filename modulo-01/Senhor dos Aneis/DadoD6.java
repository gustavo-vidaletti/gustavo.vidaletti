import java.util.Random;

public class DadoD6 {
    public static int girarDado(){
        Random rand = new Random();
        return rand.nextInt(6) + 1;
    }
    
    public static boolean apostar(int numero){
        return (girarDado() == numero);
    }
    
    public static boolean apostar(int n1, int n2){
        return (girarDado() == n1) || (girarDado() == n2);
    }
    
    public static boolean apostarUmTerco(){
        int n1 = girarDado();
        int n2 = n1;
        while(n2==n1) n2 = girarDado();
        int dado = girarDado();
        return (dado == n1 || dado == n2);
    }
}
