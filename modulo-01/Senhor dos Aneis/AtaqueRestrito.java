import java.util.ArrayList;

public class AtaqueRestrito extends OperacoesTaticas {
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> resultado = new ArrayList<>();
        this.lista = this.removerInaptos(atacantes); // <- JA REMOVE OS SEM FLECHA
        int totalNoturnos = this.calcularQuantidadeDeNoturnos();
        
        Elfo elfo = this.getPrimeiroVerde();
        while(elfo != null){
            resultado.add(elfo);
            elfo = this.getPrimeiroVerde();
        }
        this.ordenarPorQuantidadeDeFlecha(TipoOrdenacao.DESC);
        //pq? pq quando tu adicionar, 
        //tu vai adicionar o Noturno com maior qtdFlechas
        for(int i = 0; i < totalNoturnos; i++){
            elfo = this.getPrimeiroNoturno();
            if(elfo == null)  break;
            resultado.add(elfo);
        }
        
        this.ordenarPorQuantidadeDeFlecha(TipoOrdenacao.DESC);
        return resultado;
    }
    
    protected ArrayList<Elfo> removerInaptos(ArrayList<Elfo> lista){
        ArrayList<Elfo> resultado = new ArrayList<>();
        for(Elfo elfo: lista){
            if(elfo.getStatus() != Status.MORTO || elfo.getQtdFlechas() == 0)resultado.add(elfo);
        }
        return resultado;        
    }
    
    private int calcularQuantidadeDeNoturnos(){
        return (int) Math.ceil(this.lista.size() * 0.3);
    }
}
