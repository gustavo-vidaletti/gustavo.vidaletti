import java.util.ArrayList;

public abstract class OperacoesTaticas implements Estrategia{

    protected ArrayList<Elfo> lista;

    public OperacoesTaticas(){
        lista = new ArrayList<>();   
    }

    public abstract ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);

    protected ArrayList<Elfo> removerInaptos(ArrayList<Elfo> lista){
        ArrayList<Elfo> resultado = new ArrayList<>();
        for(Elfo elfo: lista){
            if(elfo.getStatus() != Status.MORTO)resultado.add(elfo);
        }
        return resultado;
    }

    protected void ordenarPorQuantidadeDeFlecha(TipoOrdenacao ordenacao){
        boolean houverTroca = true;
        while(houverTroca){
            houverTroca = false;
            for(int j = 0; j < this.lista.size() - 1; j++){
                Elfo elfoAtual = this.lista.get(j);
                Elfo proximo = this.lista.get(j + 1);

                boolean deveTrocar;
                switch(ordenacao){
                    case ASC: deveTrocar = elfoAtual.getQtdFlechas() > proximo.getQtdFlechas();
                    break;
                    case DESC: deveTrocar = elfoAtual.getQtdFlechas() < proximo.getQtdFlechas();
                    break;

                    default: deveTrocar = false;
                }

                if(deveTrocar){
                    this.lista.set(j, proximo);
                    this.lista.set(j + 1, elfoAtual);
                    houverTroca = true;
                }
            }
        }
    }

    protected Elfo getPrimeiroVerde(){
        for(Elfo elfo: lista){
            if(elfo instanceof ElfoVerde) return this.lista.remove(lista.indexOf(elfo));
        }
        return null;
    }

    protected Elfo getPrimeiroNoturno(){
        for(Elfo elfo: lista){
            if(elfo instanceof ElfoNoturno) return this.lista.remove(lista.indexOf(elfo));
        }
        return null;
    }

}