import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest
{
    @Test
    public void testarDanoDwarf(){
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("Raimundo");
        int cont = 0;
        double vidaEsperada = 110;
        while(dwarf.getVida() >= 110){
            dwarf.sofrerDano(10);
            cont++;
        }
        assertEquals(100, dwarf.getVida(), 1e-9);
        System.out.println("Tentativas até tomar o primeiro dano: " + cont);
        cont = 0;

        while(dwarf.getVida() >= 100){
            dwarf.sofrerDano(10);
            cont++;
        }
        System.out.println("Tentativas até tomar o segundo dano: " + cont);
        assertEquals(90, dwarf.getVida(), 1e-9);
    }

    @Test
    public void testarDanoDiferenteDwarf(){
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("Raimundo");
        assertTrue(this.testarAqui(dwarf, 100, 0));
    }

    private boolean testarAqui(Dwarf dwarf, double vidaEsperada, int cont){
        if(vidaEsperada == 0) return true;
        try{
            dwarf.sofrerDano(10);
            assertEquals(vidaEsperada, dwarf.getVida(), 1e-8);
            return this.testarAqui(dwarf,vidaEsperada - 10, 0);
        } catch(AssertionError e){
            return this.testarAqui(dwarf,vidaEsperada, cont++);
        }
    }
}
