import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoDeElfosTest
{
    private Status[] status = Status.values();

    @Test
    public void adicionarElfoNoturno(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        assertTrue(exercito.adicionar(new ElfoNoturno("Lentidao")));
    }

    @Test
    public void adicionarElfoVerde(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        assertTrue(exercito.adicionar(new ElfoVerde("Lentidao")));
    }

    @Test
    public void getTodosElfosPorStatus(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo ev,en;
        for(int i = 0; i < 10; i++){
            ev = new ElfoVerde("Esverdeado " + i);
            en = new ElfoNoturno("Escurecido " + i);
            assertTrue(exercito.adicionar((ElfoNoturno)en));
            assertTrue(exercito.adicionar((ElfoVerde)ev));
        }

        assertEquals(20, exercito.getArrayElfosStatus(Status.RECEM_CRIADO).size());
    }
}