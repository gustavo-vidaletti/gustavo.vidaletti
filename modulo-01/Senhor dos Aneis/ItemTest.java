import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemTest
{
    @Test
    public void verificarIgualdadeMesmoItem(){
        Item it = new Item(3, "M4A4");
        assertTrue(it.equals(it));
    }

    @Test
    public void verificarIgualdadeDeItensIguais(){
        Item it = new Item(3, "M4A4");
        Item it2 = new Item(3, "M4A4");
        assertTrue(it.equals(it2));
    }

    @Test
    public void verificarIgualdadeDeItensDiferentes(){
        Item it = new Item(3, "M4A4");
        Item it2 = new Item(4, "M4A4");
        Item it3 = new Item(3, "AK47");
        
        assertFalse(it.equals(it2));
        
        assertFalse(it.equals(it3));
        
        assertFalse(it2.equals(it3));
    }
}
