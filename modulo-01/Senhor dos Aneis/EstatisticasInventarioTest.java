import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{
    @Test
    public void testaMediaComNenhumItem(){
        Inventario inv = new Inventario();
        EstatisticasInventario ei = new EstatisticasInventario(inv);
        double resultado = ei.calcularMedia();
        assertTrue(Double.isNaN(resultado));
    }

    @Test
    public void testaMedia(){
        Inventario inv = new Inventario();
        EstatisticasInventario ei;

        inv.adicionar(new Item(1, "Ak-47"));
        ei = new EstatisticasInventario(inv);
        assertEquals(1.0, ei.calcularMedia(), 0.0001);

        inv.adicionar(new Item(2, "Ak-48"));
        assertEquals(1.5, ei.calcularMedia(), 0.0001);

        inv.adicionar(new Item(3, "Ak-49"));
        assertEquals(2.0, ei.calcularMedia(), 0.0001);

        inv.adicionar(new Item(4, "Ak-50"));
        assertEquals(2.5, ei.calcularMedia(), 0.0001);
    }

    @Test
    public void testaMedianaQuantoTaVazio(){
        Inventario inv = new Inventario();
        EstatisticasInventario ei = new EstatisticasInventario(inv);
        assertTrue(Double.isNaN(ei.calcularMediana()));
    }

    @Test
    public void testaMedianaQuantoTemUmItem(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(4, "Teste"));
        EstatisticasInventario ei = new EstatisticasInventario(inv);
        assertEquals(4, ei.calcularMediana(), 1e-8);
    }

    @Test
    public void testaMedianaQuandoEhPar(){
        //{1, 2, 3, 4, 5, 6, 8, 9}, a mediana é 4.5
        Inventario inv = new Inventario();
        EstatisticasInventario ei;
        inv.adicionar(new Item(1, "Ak-43"));
        inv.adicionar(new Item(2, "Ak-44"));
        inv.adicionar(new Item(3, "Ak-45"));
        inv.adicionar(new Item(4, "Ak-46"));
        inv.adicionar(new Item(5, "Ak-47"));
        inv.adicionar(new Item(6, "Ak-48"));
        inv.adicionar(new Item(8, "Ak-49"));
        inv.adicionar(new Item(9, "Ak-50"));
        ei = new EstatisticasInventario(inv);
        assertEquals(4.5, ei.calcularMediana(),0.0002); 
    }

    @Test
    public void testaMedianaQuandoEhImpar(){
        //1, 3, 3, 6, 7, 8, 9}, a mediana é 6
        Inventario inv = new Inventario();
        EstatisticasInventario ei;
        inv.adicionar(new Item(1, "Ak-44"));
        inv.adicionar(new Item(3, "Ak-45"));
        inv.adicionar(new Item(3, "Ak-46"));
        inv.adicionar(new Item(6, "Ak-47"));
        inv.adicionar(new Item(7, "Ak-48"));
        inv.adicionar(new Item(8, "Ak-49"));
        inv.adicionar(new Item(9, "Ak-50"));
        ei = new EstatisticasInventario(inv);

        assertEquals(6.0, ei.calcularMediana(),0.0002);
    }

    @Test
    public void testaQtdItensAcimaDaMediaInvVazio(){
        Inventario inv = new Inventario();
        EstatisticasInventario ei = new EstatisticasInventario(inv);
        assertEquals(0, ei.qtdItensAcimaDaMedia());
    }

    @Test
    public void testaQtdItensAcimaDaMediaInvUnitario(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(1, "Ak-47"));
        EstatisticasInventario ei = new EstatisticasInventario(inv);
        assertEquals(0, ei.qtdItensAcimaDaMedia());
    }

    @Test
    public void testaQtdItensAcimaDaMedia(){
        Inventario inv = new Inventario();
        EstatisticasInventario ei;
        inv.adicionar(new Item(1, "Ak-45"));
        inv.adicionar(new Item(1, "Ak-46"));
        inv.adicionar(new Item(5, "Ak-47"));
        inv.adicionar(new Item(5, "Ak-48"));
        ei = new EstatisticasInventario(inv);
        assertEquals(2, ei.qtdItensAcimaDaMedia());

        inv.adicionar(new Item(5, "Ak-49"));
        assertEquals(3, ei.qtdItensAcimaDaMedia());
    }
}
