public class Dwarf extends Personagem{
    private boolean escudoEquipado;

    {
        this.escudoEquipado = false;
        this.quantidadeVida = 110.0;
        inventario.adicionar(new Item(1, "Escudo"));
    }

    public Dwarf(String nome){
        super(nome);
    }

    //Getters

    public boolean temEscudo(){
        return this.escudoEquipado || this.inventario.buscar("Escudo") != null;
    }

    public boolean escudoEstaEquipado(){
        return this.escudoEquipado;
    }

    //Outros metodos

    public void equiparEscudo(){
        boolean removeuDoInventario = this.inventario.remover(inventario.buscar("Escudo"));
        if(removeuDoInventario)this.escudoEquipado = true;
    }

    public void guardarEscudo(){
        if(!escudoEquipado) return;
        this.inventario.adicionar(new Item(1, "Escudo"));
        this.escudoEquipado = false;
    }
    
    public boolean tomouEspadada(){
        return tomou(20);
    }

    public boolean tomouFlechada(){
        return tomou(10);
    }
    
    private boolean tomou(double dano){
        if(this.estaVivo()){
            this.sofrerDano(dano);
            return true;
        }
        return false;
    }

    //DRY - Don't Repeat Yourself

    protected void sofrerDano(double dano){
        if(this.escudoEquipado) dano = dano / 2.0;
        super.sofrerDano(dano);
    }
    
    public String imprimirResumo(){
        return "Dwarf";
    }
    
}
