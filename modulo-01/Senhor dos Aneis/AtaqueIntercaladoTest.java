import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class AtaqueIntercaladoTest
{
    @Test
    public void VerificarOrdemINTERCALADAElfos(){
        AtaqueIntercalado tatica = new AtaqueIntercalado();
        ArrayList<Elfo> lista = new ArrayList<>();
        Elfo marlon = new ElfoVerde("Marlon");
        Elfo gustavo = new ElfoNoturno("Gustavo");

        lista.add(marlon);
        lista.add(gustavo);
        lista.add(marlon);
        lista.add(marlon);
        lista.add(gustavo);
        lista.add(gustavo);
        
        ArrayList<Elfo> resultado = new ArrayList<>();
        
        resultado.add(marlon);
        resultado.add(gustavo);
        resultado.add(marlon);
        resultado.add(gustavo);
        resultado.add(marlon);
        resultado.add(gustavo);

        assertEquals(resultado, tatica.getOrdemDeAtaque(lista));
    }
    
    @Test
    public void VerificarOrdemINTERCALADAElfosIMPAR(){
        AtaqueIntercalado tatica = new AtaqueIntercalado();
        ArrayList<Elfo> lista = new ArrayList<>();
        Elfo marlon = new ElfoVerde("Marlon");
        Elfo gustavo = new ElfoNoturno("Gustavo");

        lista.add(marlon);
        lista.add(marlon);
        lista.add(gustavo);
        lista.add(gustavo);
        lista.add(gustavo);
        
        ArrayList<Elfo> resultado = new ArrayList<>();
        
        resultado.add(marlon);
        resultado.add(gustavo);
        resultado.add(marlon);
        resultado.add(gustavo);
        resultado.add(gustavo);

        assertEquals(resultado, tatica.getOrdemDeAtaque(lista));
    }
}
