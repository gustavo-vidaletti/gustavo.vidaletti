public class Item{
    protected int quantidade;
    protected String descricao;
    
    public Item(int quantidade, String descricao){
        this.quantidade = quantidade;
        this.descricao = descricao;
    }
    
    //Getters
    
    public String getDescricao(){
        return this.descricao;
    }
    
    public int getQuantidade(){
        return this.quantidade;
    }
    
    //Setters
    
    public void setDescricao(String descricao){
        this.descricao = descricao;
    }
    
    protected void setQuantidade(int quantidade){
        this.quantidade = quantidade;
    }
    
    //Dry
    
    public boolean equals(Object obj){
        Item outroItem = (Item)obj;
        return (this.getDescricao().equals(outroItem.getDescricao()) && this.getQuantidade() == outroItem.getQuantidade());
    }
    
    public String toString(){
        return "Item: " + this.getDescricao() + ", " + this.getQuantidade() + " Unidade(s)";
    }
    
}