import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
    @Test
    public void conferirPerdaDeVidaAoAtirarFlecha(){
        ElfoNoturno elfo = new ElfoNoturno("Airdan Venmaris");
        Dwarf dwarf = new Dwarf("Gimli");
        
        elfo.atirarFlecha(dwarf);
        assertEquals(85, elfo.getVida(), 1e-8);
        
        elfo.atirarFlecha(dwarf);
        assertEquals(70, elfo.getVida(), 1e-8);
    }
    
    @Test
    public void conferirExperienciaAoAtirarFlecha(){
        ElfoNoturno elfo = new ElfoNoturno("Airdan Venmaris");
        Dwarf dwarf = new Dwarf("Gimli");
        
        elfo.atirarFlecha(dwarf);        
        assertEquals(3, elfo.getExperiencia(), 1e-8);
        
        elfo.atirarFlecha(dwarf);        
        assertEquals(6, elfo.getExperiencia(), 1e-8);
        
    }
    
    @Test 
    public void elfoNoturnoMorreAoAtirarSeteFlechas(){
        ElfoNoturno elfo = new ElfoNoturno("Airdan Venmaris");
        elfo.getInventario().buscar("Flecha").setQuantidade(10000);
        for(int i = 0; i < 8; i++) elfo.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(0, elfo.getVida(), 1e-9);
        assertEquals(Status.MORTO, elfo.getStatus());
        assertEquals(21, elfo.getExperiencia());
    }
}
