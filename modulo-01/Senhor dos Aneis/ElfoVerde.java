import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerde extends Elfo{
    private static final ArrayList<String> descricoesValidas = new ArrayList<>
        (Arrays.asList("Arco de Vidro","Flecha de Vidro","Espada de Aço Valiriano"));
    
    public ElfoVerde(String nome){
        super(nome);
    }

    //Getters
    
    public int getQtdFlechasDeVidro(){
        return this.inventario.buscar("Flecha de Vidro").getQuantidade();
    }

    public boolean ganharItem(Item item){
        if (!this.descricoesValidas.contains(item.getDescricao())) return false;
        return this.inventario.adicionar(item);
    }

    public boolean perdeItem(Item item){
        if (!this.descricoesValidas.contains(item.getDescricao())) return false;
        return this.inventario.remover(item);
    }

    public void atirarFlecha(Dwarf alvo){
        if(this.podeAtirarFlechaDeVidro()){
            this.atirarFlechaGanharExp(alvo,2,"Flecha de Vidro", "Arco de Vidro");
        } else if(this.podeAtirarFlecha()){
            this.atirarFlechaGanharExp(alvo, 2, "Flecha", "Arco");
        } 
    }
}