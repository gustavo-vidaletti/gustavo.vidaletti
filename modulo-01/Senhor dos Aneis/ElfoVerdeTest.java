import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    @Test
    public void testarRecebeItensRestringidos(){
        ElfoVerde ev = new ElfoVerde("Loi");
        assertTrue(ev.ganharItem(new Item(1, "Arco de Vidro")));
        assertFalse(ev.ganharItem(new Item(2, "Pedra Obsidiana")));
        assertTrue(ev.ganharItem(new Item(20, "Flecha de Vidro")));
    }

    @Test
    public void testarPerdeItensRestringidos(){
        ElfoVerde ev = new ElfoVerde("Loi");
        ev.ganharItem(new Item(1, "Arco de Vidro"));
        ev.ganharItem(new Item(20, "Flecha de Vidro"));

        assertFalse(ev.perdeItem(new Item(2,"Flecha")));
        assertTrue(ev.perdeItem(new Item(20, "Flecha de Vidro")));
        assertTrue(ev.perdeItem(new Item(1,"Arco de Vidro")));
    }

    @Test
    public void testarPodeAtirarFlechaDeVidro(){
        ElfoVerde ev = new ElfoVerde("Loi");
        Dwarf anao = new Dwarf("Gimli");
        assertFalse(ev.podeAtirarFlechaDeVidro());
        ev.ganharItem(new Item(1, "Arco de Vidro"));
        ev.ganharItem(new Item(20, "Flecha de Vidro"));
        assertTrue(ev.podeAtirarFlechaDeVidro());
    }

    @Test
    public void AtirarFlechaDeVidroEGanharDoisDeExp(){
        ElfoVerde ev = new ElfoVerde("Loi");
        Dwarf anao = new Dwarf("Gimli");
        ev.ganharItem(new Item(1, "Arco de Vidro"));
        ev.ganharItem(new Item(20, "Flecha de Vidro"));

        ev.atirarFlecha(anao);
        assertEquals(2, ev.getExperiencia());
        assertEquals(19, ev.getInventario().buscar("Flecha de Vidro").getQuantidade());

        ev.atirarFlecha(anao);
        assertEquals(4, ev.getExperiencia());
        assertEquals(18, ev.getInventario().buscar("Flecha de Vidro").getQuantidade());
    }

    @Test
    public void atirarFlechaNormalEGanharDoisDeExp(){
        ElfoVerde ev = new ElfoVerde("Loi");
        Dwarf anao = new Dwarf("Gimli");

        ev.atirarFlecha(anao);
        assertEquals(2, ev.getExperiencia());
        assertEquals(1, ev.getInventario().buscar("Flecha").getQuantidade());

        ev.atirarFlecha(anao);
        assertEquals(4, ev.getExperiencia());
        assertEquals(0, ev.getInventario().buscar("Flecha").getQuantidade());
    }
}
