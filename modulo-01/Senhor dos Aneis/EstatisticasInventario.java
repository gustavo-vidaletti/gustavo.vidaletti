public class EstatisticasInventario
{   
    private Inventario inventario;

    public EstatisticasInventario(Inventario inv){
        this.inventario = inv;
    }

    //Getters

    //Setters

    //Other Methods

    public double calcularMedia(){
        if(this.inventario.getItens().isEmpty())return Double.NaN;
        double soma = 0.0;
        for(Item i: this.inventario.getItens())soma += i.getQuantidade();
        int total = this.inventario.getQtdItens();
        return (double) soma / total;
    }

    public double calcularMediana(){
        if(this.inventario.getItens().isEmpty()) return Double.NaN;
        this.inventario.ordenarItens(TipoOrdenacao.ASC);
        int total = this.inventario.getQtdItens();
        int elementoDoMeio = (int) total / 2;
        int qtdMeio = inventario.obter(elementoDoMeio).getQuantidade();
        if(total % 2 == 1){ //impar
            return qtdMeio;
        } else { //par
            int qtd1 = qtdMeio;
            int qtd2 = inventario.obter(elementoDoMeio - 1).getQuantidade();
            return (qtd1 + qtd2) / 2.0;
        }
    }

    public int qtdItensAcimaDaMedia(){
        if(this.inventario.getItens().isEmpty()) return 0;
        int cont = 0;
        double media = this.calcularMedia();
        for(Item i: this.inventario.getItens()) if(i.getQuantidade() > media) cont++;
        return cont;
    }

    //DRY - Don't Repeat Yourself

}
