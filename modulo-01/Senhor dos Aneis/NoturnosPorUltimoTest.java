import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class NoturnosPorUltimoTest
{   
    @Test
    public void testarRemoverMortos(){
        NoturnosPorUltimo tatica = new NoturnosPorUltimo();
        ArrayList<Elfo> lista = new ArrayList<>();
        Elfo marlon = new ElfoVerde("Marlon");
        Elfo gustavo = new ElfoNoturno("Gustavo");

        ElfoVerde elfo = new ElfoVerde("Lentidao");
        elfo.sofrerDano(1010);

        ElfoNoturno elfo2 = new ElfoNoturno("Escuridao");
        elfo2.sofrerDano(1010);

        lista.add(gustavo);
        lista.add(elfo);
        lista.add(elfo2);
        lista.add(marlon);

        ArrayList<Elfo> resultado = new ArrayList<>();

        resultado.add(marlon);
        resultado.add(gustavo);

        assertEquals(resultado, tatica.getOrdemDeAtaque(lista));
    }

    @Test
    public void VerificarOrdemElfos(){
        NoturnosPorUltimo tatica = new NoturnosPorUltimo();
        ArrayList<Elfo> lista = new ArrayList<>();
        Elfo marlon = new ElfoVerde("Marlon");
        Elfo gustavo = new ElfoNoturno("Gustavo");

        lista.add(marlon);
        lista.add(gustavo);
        lista.add(marlon);
        lista.add(marlon);
        lista.add(gustavo);

        ArrayList<Elfo> resultado = new ArrayList<>();
        resultado.add(marlon);
        resultado.add(marlon);
        resultado.add(marlon);
        resultado.add(gustavo);
        resultado.add(gustavo);

        assertEquals(resultado, tatica.getOrdemDeAtaque(lista));
    }
}
