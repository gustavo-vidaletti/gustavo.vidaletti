import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{    
    @Test
    public void conferirCemDeVidaAoNascer(){
        Elfo elfo = new Elfo("Legolas");
        assertEquals(100, elfo.getVida(), 1e-8);
    }

    @Test
    public void atirarFlechaDevePerderFlechaAumentarExpDiminuirVidaDwarf(){
        // AAA : Arranjo (Arrange), Ação (Action), Assertion(do ingles)

        //Arrange
        Elfo elfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Gimli");

        //Action
        elfo.atirarFlecha(dwarf);

        //Assertion
        assertEquals(1, elfo.getExperiencia());
        assertEquals(1, elfo.getQtdFlechas());
        assertEquals(100.0, dwarf.getVida(), 0.0001);
        //assertEquals(41, elfo.getFlecha().getQuantidade());
    }

    @Test
    public void tentarAtirar3FlechasTendoApenasDuas(){
        // AAA : Arranjo (Arrange), Ação (Action), Assertion(do ingles)

        //Arrange
        Elfo elfo = new Elfo("Legolas");
        Dwarf gimli = new Dwarf("Gimli");
        //Action
        elfo.atirarFlecha(gimli);
        elfo.atirarFlecha(gimli);
        elfo.atirarFlecha(gimli);
        //Assertion

        assertEquals(2, elfo.getExperiencia());
        assertEquals(0, elfo.getQtdFlechas());
        //assertEquals(41, elfo.getFlecha().getQuantidade());
    }

    @Test
    public void garantir2FlechasElfoAoNascer(){
        Elfo elfo = new Elfo("Legolas");
        assertEquals(2, elfo.getQtdFlechas());
    }

    @Test
    public void statusRecemCriadoParaNovoElfo(){
        Elfo elfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, elfo.getStatus());
    }

    @Test
    public void verificarAdicionarNovoPackDeFlechas(){
        Elfo elfo = new Elfo("Gustavo");
        elfo.ganharItem(new Item(4, "Flecha"));

        assertEquals(6, elfo.getInventario().buscar("Flecha").getQuantidade());       
    }

    @Test
    public void verificarSomadorDeElfos(){
        Contador contador = new Contador();
        Elfo elfo1 = new Elfo("Legolas");
        assertEquals(1, Contador.contadorDeElfo);

        ElfoNoturno en = new ElfoNoturno("Escurecido");
        assertEquals(2, Contador.contadorDeElfo);        
    }
}
