import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    @Test
    public void cofereVidaAoNascer(){
        Dwarf gimli = new Dwarf("Gimli");
        assertEquals(110.0, gimli.getVida(),0.0001);
    }

    @Test
    public void confereStatusRecemCriadoAoNovoDwarf(){
        Dwarf dwarf = new Dwarf("gimli");
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }

    @Test
    public void confereStatusAoTomarDano(){
        Dwarf dwarf = new Dwarf("gimli");
        dwarf.tomouFlechada();
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }

    @Test
    public void confereMorteDoDwarf(){
        Dwarf dwarf = new Dwarf("gimli");
        for(int i=0; i< 14; i++) dwarf.tomouFlechada();
        assertEquals(0.0, dwarf.getVida(), 0.0001);
        assertEquals(Status.MORTO, dwarf.getStatus());
    }

    @Test
    public void confereVidaAoTomarUmaFlechada(){
        Dwarf gimli = new Dwarf("Gimli");
        gimli.tomouFlechada();
        assertEquals(100.0, gimli.getVida(),0.0001);
    }

    @Test
    public void confereVidaAoTomarDuasFlechadas(){
        Dwarf gimli = new Dwarf("Gimli");
        gimli.tomouFlechada();
        gimli.tomouFlechada();
        assertEquals(90.0, gimli.getVida(),0.0001);
    }

    @Test
    public void confereVidaAoTomarDozeFlechadas(){
        Dwarf gimli = new Dwarf("Gimli");
        for(int i=0; i< 12; i++) gimli.tomouFlechada();
        assertEquals(0.0, gimli.getVida(),0.0001);
    }

    @Test 
    public void receberEscudoAoNascer(){
        Dwarf gimli = new Dwarf("Gimli");        
        assertTrue(gimli.temEscudo());
        
        gimli.equiparEscudo();
        assertTrue(gimli.temEscudo());
    }

    @Test 
    public void confereEquiparEscudo(){
        Dwarf gimli = new Dwarf("Gimli");        
        assertFalse(gimli.escudoEstaEquipado());

        gimli.equiparEscudo();
        assertTrue(gimli.escudoEstaEquipado());

        gimli.guardarEscudo();        
        assertFalse(gimli.escudoEstaEquipado());
    }

    @Test 
    public void confereDanoSofridoComEscudoNoInventario(){
        Dwarf gimli = new Dwarf("Gimli");

        gimli.tomouFlechada();
        assertEquals(100, gimli.getVida(), 1e-8);

        gimli.tomouFlechada();
        assertEquals(90, gimli.getVida(), 1e-8);
    }

    @Test 
    public void confereDanoSofridoComEscudoEquipado(){
        Dwarf gimli = new Dwarf("Gimli");
        gimli.equiparEscudo();

        gimli.tomouFlechada();
        assertEquals(105, gimli.getVida(), 1e-8);

        gimli.tomouFlechada();
        assertEquals(100, gimli.getVida(), 1e-8);
    }    
}
