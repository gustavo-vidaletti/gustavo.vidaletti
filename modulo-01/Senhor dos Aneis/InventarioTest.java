import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class InventarioTest
{
    @Test
    public void conferirGetQtdItens(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(1,"Teste 1"));
        assertEquals(1, inv.getQtdItens());
        inv.adicionar(new Item(1,"Teste 2"));
        assertEquals(2, inv.getQtdItens());
        inv.adicionar(new Item(1,"Teste 3"));
        assertEquals(3, inv.getQtdItens());
        inv.adicionar(new Item(1,"Teste 4"));
        assertEquals(4, inv.getQtdItens());
    }

    @Test
    public void conferirObterAoTerUmArrayVazio(){
        Inventario inv = new Inventario();
        assertNull(inv.obter(0));
        assertNull(inv.obter(1));
    }

    @Test
    public void conferirObterAoIndicarPosicaoForaDoLimite(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(1, "Espada"));
        inv.adicionar(new Item(1, "Granada"));
        assertNull(inv.obter(2));
        assertNull(inv.obter(4));
    }

    @Test
    public void conferirGetDescricoesItens(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(1,"Item 1"));
        assertEquals("Item 1", inv.getDescricoesItens());
        inv.adicionar(new Item(1,"Item 2"));
        assertEquals("Item 1, Item 2", inv.getDescricoesItens());
        inv.adicionar(new Item(1,"Item 3"));
        assertEquals("Item 1, Item 2, Item 3", inv.getDescricoesItens());
        inv.adicionar(new Item(1,"Item 4"));
        assertEquals("Item 1, Item 2, Item 3, Item 4", inv.getDescricoesItens());
        inv.remover(0);
        assertEquals("Item 2, Item 3, Item 4", inv.getDescricoesItens());
    }

    @Test 
    public void conferirItemComMaiorQuantidade(){
        Inventario inv = new Inventario();
        Item item1 = new Item(1,"Item 1");
        Item item2 = new Item(2,"Item 2");
        Item item3 = new Item(5,"Item 3");
        Item item4 = new Item(3,"Item 4");
        Item item5 = new Item(6,"Item 5");
        inv.adicionar(item1);
        assertEquals(item1, inv.getItemComMaiorQuantidade());
        inv.adicionar(item2);
        assertEquals(item2, inv.getItemComMaiorQuantidade());
        inv.adicionar(item3);
        assertEquals(item3, inv.getItemComMaiorQuantidade());
        inv.adicionar(item4);
        assertEquals(item3, inv.getItemComMaiorQuantidade());
        inv.adicionar(item5);
        assertEquals(item5, inv.getItemComMaiorQuantidade());
    }

    @Test
    public void conferirBuscaPorDescricao(){
        Inventario inv = new Inventario();

        Item item1 = new Item(1,"Item 1");
        Item item2 = new Item(2,"Item 2");
        inv.adicionar(item1);
        inv.adicionar(item2);

        assertEquals(item1, inv.buscar("Item 1"));
        assertEquals(item2, inv.buscar("Item 2"));

    }

    @Test
    public void buscarComOInventarioVazio(){
        Inventario inv = new Inventario();
        assertNull(inv.buscar("Teste"));
    }

    @Test
    public void buscarDescricaoQueNaoExiste(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(2, "Vem Ser DBC"));
        assertNull(inv.buscar("Teste"));
    }

    @Test
    public void confereInversaoDaPosicaoDosElementos(){
        Inventario inv = new Inventario();
        Item item1 = new Item(1,"Espada");
        Item item2 = new Item(2,"Escudo");
        Item item3 = new Item(3,"Flashbang");
        inv.adicionar(item1);
        inv.adicionar(item2);
        inv.adicionar(item3);

        ArrayList<Item> comparacao = new ArrayList<>();
        comparacao.add(item3);
        comparacao.add(item2);
        comparacao.add(item1);

        assertEquals(comparacao, inv.inverter());
    }

    @Test
    public void ordenarItensAscendente(){
        Inventario inv = new Inventario();
        Item item1 = new Item(1,"Espada");
        Item item2 = new Item(5,"Escudo");
        Item item3 = new Item(3,"Flashbang");
        Item item4 = new Item(4,"Hend Grenade");

        inv.adicionar(item1);
        inv.adicionar(item2);
        inv.adicionar(item4);
        inv.adicionar(item3);

        inv.ordenarItens(TipoOrdenacao.ASC);

        ArrayList<Item> esperado = new ArrayList<>();
        esperado.add(item1);
        esperado.add(item3);
        esperado.add(item4);
        esperado.add(item2);

        assertEquals("Espada, Flashbang, Hend Grenade, Escudo", inv.getDescricoesItens());
        assertEquals(esperado, inv.getItens());
    }

    @Test
    public void ordenarItensDescendente(){
        Inventario inv = new Inventario();
        Item item1 = new Item(1,"Espada");
        Item item2 = new Item(5,"Escudo");
        Item item3 = new Item(3,"Flashbang");
        Item item4 = new Item(4,"Hend Grenade");

        inv.adicionar(item1);
        inv.adicionar(item2);
        inv.adicionar(item4);
        inv.adicionar(item3);

        ArrayList<Item> esperado = new ArrayList<>();
        esperado.add(item2);
        esperado.add(item4);
        esperado.add(item3);
        esperado.add(item1);

        inv.ordenarItens(TipoOrdenacao.DESC);
        assertEquals(esperado, inv.getItens());
        assertEquals("Escudo, Hend Grenade, Flashbang, Espada", inv.getDescricoesItens());
    }

    @Test
    public void unirInventarios(){
        Inventario inv = new Inventario();
        Inventario inv2 = new Inventario();

        inv.adicionar(new Item(2, "Cabeça"));
        inv.adicionar(new Item(6, "Pé"));
        inv2.adicionar(new Item(4, "Joelho"));
        inv2.adicionar(new Item(3, "Ombro"));

        Inventario resultado = inv.unir(inv2);
        assertEquals("Cabeça, Ombro, Joelho, Pé", resultado.getDescricoesItens());
    }

    @Test
    public void unirInventariosSemItensEmUmOuOutro(){
        Inventario inv = new Inventario();
        Inventario inv2 = new Inventario();

        inv.adicionar(new Item(2, "Cabeça"));
        inv.adicionar(new Item(6, "Pé"));

        Inventario res1 = inv.unir(inv2);
        Inventario res2 = inv2.unir(inv);

        assertEquals(res1.getDescricoesItens(), res2.getDescricoesItens());

        assertEquals(2, res1.getQtdItens());
        assertEquals(2, res2.getQtdItens());
    }

    @Test
    public void interseccaoEntreInventarios(){
        Inventario inv = new Inventario();
        Inventario inv2 = new Inventario();

        inv.adicionar(new Item(2, "Cabeça"));
        inv.adicionar(new Item(6, "Pé"));
        inv.adicionar(new Item(4, "Joelho"));
        inv2.adicionar(new Item(4, "Joelho"));
        inv2.adicionar(new Item(3, "Ombro"));
        inv2.adicionar(new Item(6, "Pé"));

        Inventario resultado = inv.cruzar(inv2);
        assertEquals("Joelho, Pé", resultado.getDescricoesItens());

    }

    @Test
    public void diferenciarOutroInventario(){
        Inventario inv = new Inventario();
        Inventario inv2 = new Inventario();

        Item m4a1 = new Item(1, "M4A1");
        Item m4a4 = new Item(2, "M4A4");
        Item ak47 = new Item(3, "Ak-47");
        Item mk74 = new Item(4, "Mk74");

        inv.adicionar(m4a1);
        inv.adicionar(m4a4);
        inv.adicionar(ak47);

        inv2.adicionar(m4a4);
        inv2.adicionar(m4a1);
        inv2.adicionar(mk74);

        assertEquals("Ak-47", inv.diferenciar(inv2).getDescricoesItens());

        assertEquals("Mk74", inv2.diferenciar(inv).getDescricoesItens());

    }

    @Test
    public void testarBubbleSortASC(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(2,"dois"));
        inv.adicionar(new Item(3,"tres"));
        inv.adicionar(new Item(6,"seis"));
        inv.adicionar(new Item(1,"um"));
        inv.adicionar(new Item(7,"sete"));
        inv.adicionar(new Item(9,"nove"));
        inv.adicionar(new Item(4,"quatro"));

        inv.ordenarItensBubble(TipoOrdenacao.ASC);

        assertEquals("um, dois, tres, quatro, seis, sete, nove", inv.getDescricoesItens());
    }

    @Test
    public void testarBubbleSortParcialmenteOrdenadoASC(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(1,"um"));
        inv.adicionar(new Item(2,"dois"));
        inv.adicionar(new Item(3,"tres"));
        inv.adicionar(new Item(4,"quatro"));
        inv.adicionar(new Item(7,"sete"));
        inv.adicionar(new Item(6,"seis"));
        inv.adicionar(new Item(9,"nove"));

        inv.ordenarItensBubble(TipoOrdenacao.ASC);

        assertEquals("um, dois, tres, quatro, seis, sete, nove", inv.getDescricoesItens());
    }

    @Test
    public void testarBubbleSortInvertidoASC(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(7,"sete"));
        inv.adicionar(new Item(6,"seis"));
        inv.adicionar(new Item(5,"cinco"));
        inv.adicionar(new Item(4,"quatro"));
        inv.adicionar(new Item(3,"tres"));
        inv.adicionar(new Item(2,"dois"));
        inv.adicionar(new Item(1,"um"));

        inv.ordenarItensBubble(TipoOrdenacao.ASC);

        assertEquals("um, dois, tres, quatro, cinco, seis, sete", inv.getDescricoesItens());
    }

    @Test
    public void testarBubbleSortTotalmenteOrdenadoASC(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(1,"um"));
        inv.adicionar(new Item(2,"dois"));
        inv.adicionar(new Item(3,"tres"));
        inv.adicionar(new Item(4,"quatro"));
        inv.adicionar(new Item(5,"cinco"));
        inv.adicionar(new Item(6,"seis"));
        inv.adicionar(new Item(7,"sete"));
        
        inv.ordenarItensBubble(TipoOrdenacao.ASC);

        assertEquals("um, dois, tres, quatro, cinco, seis, sete", inv.getDescricoesItens());
    }
    
    @Test
    public void testarBubbleStortTotalmenteOrdenadoDESC(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(7,"sete"));
        inv.adicionar(new Item(6,"seis"));
        inv.adicionar(new Item(5,"cinco"));
        inv.adicionar(new Item(4,"quatro"));
        inv.adicionar(new Item(3,"tres"));
        inv.adicionar(new Item(2,"dois"));
        inv.adicionar(new Item(1,"um"));

        inv.ordenarItensBubble(TipoOrdenacao.DESC);

        assertEquals("sete, seis, cinco, quatro, tres, dois, um", inv.getDescricoesItens());
    }
    
    @Test
    public void testarBubbleSortTotalmenteInvertidoDESC(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(1,"um"));
        inv.adicionar(new Item(2,"dois"));
        inv.adicionar(new Item(3,"tres"));
        inv.adicionar(new Item(4,"quatro"));
        inv.adicionar(new Item(5,"cinco"));
        inv.adicionar(new Item(6,"seis"));
        inv.adicionar(new Item(7,"sete"));
        
        inv.ordenarItensBubble(TipoOrdenacao.DESC);

        assertEquals("sete, seis, cinco, quatro, tres, dois, um", inv.getDescricoesItens());
    }
    
    @Test
    public void testarBubbleSortAleatorioDESC(){
        Inventario inv = new Inventario();
        inv.adicionar(new Item(2,"dois"));
        inv.adicionar(new Item(3,"tres"));
        inv.adicionar(new Item(6,"seis"));
        inv.adicionar(new Item(1,"um"));
        inv.adicionar(new Item(7,"sete"));
        inv.adicionar(new Item(9,"nove"));
        inv.adicionar(new Item(4,"quatro"));

        inv.ordenarItensBubble(TipoOrdenacao.DESC);

        assertEquals("nove, sete, seis, quatro, tres, dois, um", inv.getDescricoesItens());
    }
}
