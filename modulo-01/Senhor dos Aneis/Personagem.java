public abstract class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double quantidadeVida;
    protected int exp;

    //public enum Tipo{
    //    Elfo, ElfoVerde
    //    }

    {
        inventario = new Inventario();
        this.status = Status.RECEM_CRIADO;
        this.exp = 0;
    }

    protected Personagem(String nome){
        this.nome = nome;
    }

    protected boolean estaVivo(){
        return (this.status != Status.MORTO);
    }

    //Getters

    protected String getNome(){
        return this.nome;
    }

    protected Status getStatus(){
        return this.status;
    }

    protected double getVida(){
        return this.quantidadeVida;
    }

    protected Inventario getInventario(){
        return this.inventario;
    }

    protected int getExperiencia(){
        return this.exp;
    }

    //Setters

    protected void setNome(String nome){
        this.nome = nome;
    }

    //Outros metodos

    protected boolean ganharItem(Item item){
        return this.inventario.adicionar(item);
    }

    protected boolean perdeItem(Item item){
        return this.inventario.remover(item);
    }

    protected void ganharExp(int pts){
        this.exp = this.exp + pts;
    }

    protected void sofrerDano(double dano){
        this.status = Status.SOFREU_DANO;
        if(this.quantidadeVida > dano){
            this.quantidadeVida -= dano;
        } else { 
            this.quantidadeVida = 0;
            this.status = Status.MORTO;
        }
    }

    protected boolean podeAtirarFlecha(){
        return this.podeAtirarFlechaPorDescricao("Flecha","Arco");
    }

    protected boolean podeAtirarFlechaDeVidro(){
        return this.podeAtirarFlechaPorDescricao("Flecha de Vidro", "Arco de Vidro");
    }

    protected boolean podeAtirarFlechaPorDescricao(String descricaoFlecha, String descricaoArco){
        Item flecha = this.inventario.buscar(descricaoFlecha);
        Item arco = this.inventario.buscar(descricaoArco);
        return (this.getStatus() != Status.MORTO) && (arco != null && flecha != null) && (flecha.getQuantidade() > 0 && arco.getQuantidade() > 0);
    }

    protected boolean podeAtacarComEspada(){
        Item espada = this.inventario.buscar("Espada de Galvorn");
        return (espada != null);
    }

    public int getQtdFlechas(){
        Item flecha = this.inventario.buscar("Flecha"); 
        if(flecha != null) return flecha.getQuantidade();
        return 0;
    }

    //Demais Metodos

    public abstract String imprimirResumo();

    //Dry - Don't Repeat Yourself
}