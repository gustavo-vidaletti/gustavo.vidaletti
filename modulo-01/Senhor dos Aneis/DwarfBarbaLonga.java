public class DwarfBarbaLonga extends Dwarf{
    public DwarfBarbaLonga(String nome){
        super(nome);
    }

    protected void sofrerDano(double dano){
        if(DadoD6.apostarUmTerco()) super.sofrerDano(dano);
    }
}
