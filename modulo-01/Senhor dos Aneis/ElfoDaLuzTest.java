import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    @Test
    public void confereRegraEspadadaParOuImpar(){
        ElfoDaLuz edl = new ElfoDaLuz("Feanor");

        edl.atacarComEspada(new Dwarf("Farlum"));

        assertEquals(79.0, edl.getVida() ,1e-8);

        edl.atacarComEspada(new Dwarf("Orlom"));

        assertEquals(89.0, edl.getVida() ,1e-8);
    }
}
