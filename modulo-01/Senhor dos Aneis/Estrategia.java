import java.util.*;

/**
 * Escreva a descrição da interface Estrategia aqui.
 * 
 * @author (seu nome) 
 * @version (número da versão ou data)
 */

public interface Estrategia
{
    ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);
}
