public class ElfoNoturno extends Elfo {
    public ElfoNoturno(String nome){
        super(nome);
    }
    
    public void atirarFlecha(Dwarf alvo){
        this.atirarFlechaGanharExp(alvo,3,"Flecha", "Arco");
        this.sofrerDano(15);
    }
}
