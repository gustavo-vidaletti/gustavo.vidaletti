import java.util.HashMap;
import java.util.ArrayList;

public class ExercitoDeElfos {
    //HashMap<Status, Elfo> alistados;
    HashMap<Status, ArrayList<Elfo> > porStatus;
    public ExercitoDeElfos(){
        this.porStatus = new HashMap<>();
    }

    public boolean adicionar(ElfoNoturno en){
        return this.alistar(en);
    }

    public boolean adicionar(ElfoVerde ev){
        return this.alistar(ev);
    }
    
    private boolean alistar(Elfo elfo){
        ArrayList<Elfo> arrayDoStatus = this.porStatus.getOrDefault(elfo.getStatus(), new ArrayList<Elfo>());
        boolean adicionou = arrayDoStatus.add(elfo);
        this.porStatus.put(elfo.getStatus(), arrayDoStatus);
        return adicionou;
    }

    public ArrayList<Elfo> getArrayElfosStatus(Status status){
        return this.porStatus.get(status);
    }
}
