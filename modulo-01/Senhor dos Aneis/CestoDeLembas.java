public class CestoDeLembas{

    private int qtd = 0;
    public CestoDeLembas(int qtd){
        if(qtd > 100) qtd = 100;
        else if(qtd <= 0) qtd = 0;
        this.qtd = qtd;
    }

    //Getters

    public int getQtd(){ 
        return this.qtd;
    }
    
    public boolean taVazio(){
        return this.getQtd() == 0;
    }
    //Setters
    
    /**
     * Este método permite adicionar Lembas ao seu cesto.
     * Porém, como temos um máximo de 100 lembas,
     */
    public int adicionarLembas(int qtd){
        if(qtd < 0) return 0;
        this.qtd += qtd;
        int sobra = 0;
        if(this.qtd > 100){
            sobra = this.qtd - 100;
            this.qtd = 100;
        }
        return sobra;
    }

    public boolean retirarLembas(int qtd){
        if(this.qtd >= qtd){
            this.qtd -= qtd;
            return true;
        }
        return false;
    }

    //Dry - Don't Repeat Yourself

    public boolean podeDividirEmPares(){
        for(int i = 2; i <= 50; i = i + 2){
            for(int j = 2; j <= 50; j = j + 2){
                if(i + j == this.qtd) return true;
            }
        }
        return false;
    }
}