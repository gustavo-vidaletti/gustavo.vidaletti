public class TradutorParaIngles implements Tradutor{
    
    public TradutorParaIngles(){
    }
    
    public String traduzir(String textoEmPortugues){
        switch(textoEmPortugues.toUpperCase()){
            case "SIM": return "Yes";
            case "OBRIGADO": return "Thanks";
            case "NAO":
            case "NÃO": return "No";
            default: return null;
        }
    }
    
    public boolean ehIgual(String a, String b){
        return a.equalsIgnoreCase(b);
    }
}