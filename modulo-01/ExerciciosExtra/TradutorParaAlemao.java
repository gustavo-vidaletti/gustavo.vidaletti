public class TradutorParaAlemao implements Tradutor {
    public String traduzir(String textoEmPortugues){
        switch(textoEmPortugues){
            case "Sim": return "Ja";
            case "Obrigado": return "Danke";
            case "Não": return "Nein";
            default: return null;
        }
    }
}
