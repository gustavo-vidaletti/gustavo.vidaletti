import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest
{
    @Test
    public void verificarConsultar(){
        AgendaContatos ac = new AgendaContatos();
        ac.adicionar("Mithrandir", "444444");
        ac.adicionar("Bernardo", "555555");

        assertEquals("555555", ac.consultarPorNome("Bernardo"));
        assertEquals("444444", ac.consultarPorNome("Mithrandir"));
    }
    
    @Test
    public void verificarCSV(){
        AgendaContatos ac = new AgendaContatos();
        ac.adicionar("Mithrandir", "444444");
        ac.adicionar("Bernardo", "555555");
        String separator = System.lineSeparator();
        assertEquals("Bernardo;555555" + separator +"Mithrandir;444444" + separator, ac.csv());

    }
}
