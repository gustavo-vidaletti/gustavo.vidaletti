import java.util.*;

public class AgendaContatos{
    private TreeMap<String, Contato> contatos;
    public AgendaContatos(){
        this.contatos = new TreeMap<>();
    }

    public void adicionar(String nome,String numero){
        Contato contato = new Contato(nome, numero);
        contatos.put(nome,contato);
    }

    public String consultarPorNome(String nome){
        return this.contatos.get(nome).getNumero();
    }

    public String consultarPorNumero(String numero){
        String i;
        for (HashMap.Entry<String, Contato> par : contatos.entrySet()) {
            if(par.getValue().getNumero().equals(numero)) return par.getKey();
        }
        return "";
    }    

    public String csv(){
        StringBuilder agenda = new StringBuilder();
        String separator = System.lineSeparator();
        for(Contato c: this.contatos.values()){
            agenda.append(c + separator);
        }
        return agenda.toString();
    }
}