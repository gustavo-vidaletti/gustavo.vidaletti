public class Contato{
    private String nome,sobrenome, numero;
    
    public Contato(String nome, String numero){
        this.nome = nome;
        this.numero = numero;    
    }
        
    public boolean equals(Object obj){
        Contato contato = (Contato) obj;
        return contato.getNumero().equals(this.getNumero());
    }
    
    public String getNome(){ return this.nome;}
    
    public String getNumero() { return this.numero;}

    public String toString(){
        return this.getNome() + ";" + this.getNumero();
    }
}