public class TraduzirParaEspanhol implements Tradutor{
    public String traduzir(String textoEmPortugues){
        switch(textoEmPortugues){
            case "Sim": return "Si";
            case "Obrigado": return "Gracias";
            case "Não": return "No";
            default: return null;
        }
    }
}