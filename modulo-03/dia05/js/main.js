console.log( "Bem vindo" );
// var id = prompt("Digite o id do pokemon")

// var titulo = document.getElementById('titulo')

var pokeApi = new PokeApi()
var interpretaJson = pokeApi.buscar( 4 )
  .then( pokemonVindoDaApi => {
    let poke = new Pokemon( pokemonVindoDaApi )
    renderizaPokemonNaTela( poke )
  } )

function renderizaPokemonNaTela( pokemon ) {
  var dadosPokemon = document.getElementById( 'dadosPokemon' )
  var nome = dadosPokemon.querySelector( '#nome' )
  var imgPokemon = dadosPokemon.querySelector( '#imagem' )

  nome.innerText = pokemon.nome
  imgPokemon.src = pokemon.thumbUrl
  // imgPokemon.style.transform = 'scale(1.5)'
}

async function rodar() {
  const pokemonServidor = await pokeApi.buscar( 25 )
  const poke = new Pokemon( pokemonServidor )
  renderizaPokemonNaTela( poke )
}
