class Pokemon {
  constructor( objVindoDaApi ) {
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this._altura = objVindoDaApi.height
  }

  get altura() {
    return this._altura * 10;
  }
}
