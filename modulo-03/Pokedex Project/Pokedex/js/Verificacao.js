/* eslint-disable linebreak-style */
/* eslint-disable no-alert */
// eslint-disable-next-line no-unused-vars
class Verificacao {
  verificarIdPokemon( id, idAtivo ) {
    if ( isNaN( id ) ) {
      // eslint-disable-next-line no-undef
      toastr.error( 'Digite um ID Válido', 'Erro' )
      return false
    }
    if ( id < 1 || id > 802 ) {
      // eslint-disable-next-line no-undef
      toastr.error( 'Digite um ID entre 1 e 802', 'Erro' )
      return false
    }
    if ( id === idAtivo ) {
      // eslint-disable-next-line no-undef
      // toastr.info( 'Este pokemon está na tela!' )
      return false
    }
    return true
  }
}
