// eslint-disable-next-line no-unused-vars
class Tela {
  mostrarTexto( localNaPokedex, texto, queryNome ) {
    const campo = localNaPokedex.querySelector( queryNome )
    campo.innerText = texto
  }

  apagarTodosElementosDeTipo( queryTipo ) {
    const elementos = document.querySelectorAll( queryTipo )
    elementos.forEach.call( elementos, ( elemento ) => {
      elemento.parentNode.removeChild( elemento )
    } )
  }

  mostrarImagem( localNaPokedex, pokemon, queryTipo ) {
    const imgPokemon = localNaPokedex.querySelector( queryTipo )
    imgPokemon.src = pokemon.thumbUrl
    imgPokemon.alt = pokemon.nome
  }

  mostrarTipos( localNaPokedex, tipos ) {
    const listaTipos = localNaPokedex.querySelector( '#tipos' )
    tipos.forEach( ( tipo ) => {
      const li = document.createElement( 'li' )
      li.className = 'tipo-nome'
      const txtLI = document.createTextNode( `${ tipo }` )

      li.appendChild( txtLI )
      listaTipos.appendChild( li )
    } )
  }

  mostrarEstatisticas( localNaPokedex, estatisticas ) {
    const listaEstatisticas = localNaPokedex.querySelector( '#estatisticas' )

    estatisticas.forEach( ( estatistica ) => {
      const li = document.createElement( 'li' )
      li.className = 'estatistica-nome'
      const txtLI = document.createTextNode( `${ estatistica.nome }` )
      const span = document.createElement( 'span' )
      span.className = 'estatistica-porcentagem'
      const txtSpan = document.createTextNode( `: ${ estatistica.percent }%` )

      li.appendChild( txtLI )
      span.appendChild( txtSpan )
      li.appendChild( span )

      listaEstatisticas.appendChild( li )
    } )
  }

  setIdPokeNaTela( id ) {
    document.getElementById( 'idPokemon' ).value = id
  }

  renderizarPokemonNaTela( pokemon ) {
    const dadosPokemonLeft = document.getElementById( 'dadosPokemon-left' )
    const dadosPokemonRight = document.getElementById( 'dadosPokemon-right' )
    this.apagarTodosElementosDeTipo( '.estatistica-nome' )
    this.apagarTodosElementosDeTipo( '.tipo-nome' )
    this.mostrarImagem( dadosPokemonLeft, pokemon, '.img-poke' )
    this.mostrarTexto( dadosPokemonRight, pokemon.nome, '.nome' )
    this.mostrarTexto( dadosPokemonRight, pokemon.id, '.id' )
    this.mostrarTexto( dadosPokemonRight, pokemon.altura, '.altura' )
    this.mostrarTexto( dadosPokemonRight, pokemon.peso, '.peso' )
    this.mostrarTipos( dadosPokemonRight, pokemon.tipos )
    this.mostrarEstatisticas( dadosPokemonRight, pokemon.estatisticas )
  }

  abrirPokedex() {
    const elemento = document.getElementById( 'tampinha' )
    elemento.className += ' rodar'
    setTimeout( () => {
      elemento.src = 'img/aberto.png'
      elemento.style.cursor = 'default'
      elemento.className += ' segunda'
      setTimeout( () => {
        elemento.style.opacity = 0;
        document.getElementById( 'dadosPokemon-right' ).style.display = 'block'
        setTimeout( () => {
          elemento.style.display = 'none'
        }, 1000 )
      }, 2050 )
    }, 1200 )

    setTimeout( () => {
      document.getElementById( 'cont-poke' ).style.marginLeft = '118px'
      document.getElementById( 'cont-poke' ).style.position = 'initial'
    }, 300 )
  }

  piscarBolota() {
    let ligado = false;
    const idInterval = setInterval( () => {
      ligado = !ligado;
      document.getElementById( 'bolotinha' ).style.opacity = ( ligado ) ? 1 : 0
    }, 250 )

    setTimeout( () => {
      clearInterval( idInterval )
      document.getElementById( 'bolotinha' ).style.opacity = 0
    }, 3300 )
  }

  piscarBolinhas() {
    let ligado = 1;

    setInterval( () => {
      if ( ligado > 6 ) {
        ligado = 1
      }
      document.getElementById( 'vermelha' ).style.opacity = ( ligado === 1 ) ? 1 : 0
      document.getElementById( 'amarela' ).style.opacity = ( ligado === 3 ) ? 1 : 0
      document.getElementById( 'verde' ).style.opacity = ( ligado === 5 ) ? 1 : 0

      ligado += 1
    }, 1000 )
  }
}
