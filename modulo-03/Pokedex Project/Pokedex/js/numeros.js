/* eslint-disable linebreak-style */
// eslint-disable-next-line no-unused-vars
class Numeros {
  constructor() {
    // eslint-disable-next-line no-undef
    this.armazenamentoLocal = new ArmazenamentoLocal()
    this.arraySorteados = this.armazenamentoLocal.buscarArray( 'arrayDeNumerosRandomicosJaSorteados' )
    this.maxPoke = 802
  }

  gerarNumeroRandomico() {
    // Codigo creditado ao site
    // https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive( min, max ) {
      const minimo = Math.ceil( min )
      const maximo = Math.floor( max )
      return Math.floor( Math.random() * ( maximo - minimo + 1 ) ) + min
    }

    let numero = getRandomIntInclusive( 1, this.maxPoke )
    while ( this.arraySorteados.includes( numero ) ) {
      if ( this.arraySorteados.length === this.maxPoke ) return -1
      numero = getRandomIntInclusive( 1, this.maxPoke )
    }
    this.salvarNumeroRandomicoNoArray( numero )
    return numero;
  }

  salvarNumeroRandomicoNoArray( numero ) {
    this.arraySorteados.push( numero )
    this.armazenamentoLocal.salvarArray( 'arrayDeNumerosRandomicosJaSorteados', this.arraySorteados )
  }
}
