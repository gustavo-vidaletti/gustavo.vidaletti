// eslint-disable-next-line no-unused-vars
class ArmazenamentoLocal {
  salvarArray( nome, array ) {
    // localStorage.setItem( nome, JSON.stringify( array ) )
    localStorage[nome] = JSON.stringify( array )
  }

  buscarArray( nome ) {
    // let local = localStorage.getItem( nome )
    let local = localStorage[nome]
    if ( local ) local = JSON.parse( local )
    return local || []
  }
}
