/* eslint-disable no-alert */
class Pokedex { // eslint-disable-line no-unused-vars
  constructor() {
    this.pokeAPI = new PokeApi()
    this.tela = new Tela() // eslint-disable-line no-undef
    this.numeros = new Numeros() // eslint-disable-line no-undef
    this.verificacao = new Verificacao() // eslint-disable-line no-undef
    this.armazenamentoLocal = new ArmazenamentoLocal() // eslint-disable-line no-undef
    this.tela.piscarBolinhas()
    this.idAtivo = -1
    this.pokemonsJaBuscados = this.armazenamentoLocal.buscarArray( 'pokemons' )
  }

  async buscar( id = this.numeros.gerarNumeroRandomico() ) {
    if ( !this.verificacao.verificarIdPokemon( id, this.idAtivo ) ) {
      return;
    }

    this.tela.setIdPokeNaTela( id )
    this.idAtivo = id;

    let poke = this.pokemonsJaBuscados.find( ( p ) => p.id === parseInt( id ) )
    if ( !poke ) {
      const pokeServidor = await this.pokeAPI.buscar( id )
      poke = new Pokemon( pokeServidor )
	  this.pokemonsJaBuscados.push( poke )
	  this.armazenamentoLocal.salvarArray( 'pokemons', this.pokemonsJaBuscados )
	}
    this.tela.piscarBolota()
    this.tela.renderizarPokemonNaTela( poke )
  }
}
