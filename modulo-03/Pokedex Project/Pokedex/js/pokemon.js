class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objVindoDaApi ) {
    this.id = objVindoDaApi.id;
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this._altura = objVindoDaApi.height
    this._peso = objVindoDaApi.weight
    this._estatisticas = objVindoDaApi.stats
    this.tipos = objVindoDaApi.types.map( ( tipo ) => this.tradutor( tipo.type.name ) )
  }

  tradutor( nome ) {
    // return {
    //   speed: 'Velocidade',
    //   'special-defense': 'Defesa especial',
    // }[nome] || nome

    switch ( nome ) {
      // ESTATISTICAS
      case 'speed':
        return 'Velocidade'
      case 'special-defense':
        return 'Defesa especial'
      case 'special-attack':
        return 'Ataque especial'
      case 'defense':
        return 'Defesa'
      case 'attack':
        return 'Ataque'
      case 'hp':
        return 'Vida'
      case 'accuracy':
        return 'Precisão'
      case 'evasion':
        return 'Evasão'
        // TIPOS
      case 'normal':
        return 'Normal'
      case 'fighting':
        return 'Lutador'
      case 'flying':
        return 'Voador'
      case 'poison':
        return 'Venenoso'
      case 'ground':
        return 'Terra'
      case 'rock':
        return 'Pedra'
      case 'bug':
        return 'Inseto'
      case 'ghost':
        return 'Fantasma'
      case 'steel':
        return 'Metálico'
      case 'fire':
        return 'Fogo'
      case 'water':
        return 'Água'
      case 'grass':
        return 'Planta'
      case 'electric':
        return 'Elétrico'
      case 'psychic':
        return 'Psíquico'
      case 'ice':
        return 'Gelo'
      case 'dragon':
        return 'Dragão'
      case 'dark':
        return 'Noturno'
      case 'fairy':
        return 'Fada'
      case 'unknown':
        return 'Desconhecido'
      case 'shadow':
        return 'Sombrio'
      default:
        return nome
    }
  }

  get altura() {
    return this._altura * 10;
  }

  get peso() {
    return this._peso / 10;
  }

  get estatisticas() {
    return this._estatisticas.map( e => ( {
      nome: this.tradutor( e.stat.name ),
      percent: e.base_stat,
    } ) )
  }
}
