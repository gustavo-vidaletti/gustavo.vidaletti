const Mensagens = {
  SUCESSO: {
    TIPO: 'sucesso',
    TITULO: 'Sucesso!',
    REGISTRO_NOTA: 'Registramos sua nota!'
  },
  ERRO: {
    TIPO: 'erro',
    TITULO: 'Erro!',
    NOTA_INVALIDA: 'Informar uma nota válida (entre 1 e 5)',
    CAMPO_OBRIGATORIO: '* obrigatório'
  }
}

export default Mensagens
