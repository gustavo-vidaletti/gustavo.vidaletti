/* eslint-disable no-extend-native */
Array.prototype.ordenarSerie = function(){
  return this.sort( ( a, b )  => {
    const temporadaA = a.temporada
    const temporadaB = b.temporada
    const episodioA = a.ordemEpisodio
    const episodioB = b.ordemEpisodio
    if(temporadaA === temporadaB){
      return (episodioA < episodioB) ? -1 : (episodioA > episodioB) ? 1 : 0
    } else {
      return (temporadaA < temporadaB) ? -1 : 1
    }
  } )
}

Array.prototype.ordenarPorEstreia = function() {
  return this.sort( ( a, b ) => {
    const dataA = a.dataEstreia
    const dataB = b.dataEstreia
    return dataA.getTime() - dataB.getTime()
  } )
}

Array.prototype.ordenarPorNome = function() {
  return this.sort( ( a, b ) => {
    return a.nome.localeCompare( b.nome )
  } )
}

Array.prototype.ordenarPorDuracao = function() {
 return this.sort( ( a, b ) => {
   return a.duracao - b.duracao
 } )
}


