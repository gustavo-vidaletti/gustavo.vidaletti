import axios from 'axios'
import Episodio from '../models/episodio';

export default class ReactMirrorAPI {

  // TODOS OS EPISODIOS
  get todosEpisodios() {
    return axios.get('http://localhost:9000/api/episodios')
  }

  // GET EPISODIO PELO ID
  episodio( id ) {
    return axios.get( `http://localhost:9000/api/episodios/${ parseInt( id ) }/`)
  }

  // DETALHES DE UM EPISODIO
  detalhesDoEpisodio( id ) {
    return axios.get( `http://localhost:9000/api/episodios/${ parseInt( id ) }/detalhes` )
  }

  episodioFULL( id ) {
    const promessa = new Promise( (resolve, reject) => {
    this.episodio( id ).then( episodio => {
      this.avaliacao( id ).then( avaliacao => {
        this.detalhesDoEpisodio( id ).then( detalhes => {
          const e = episodio.data
          const f = avaliacao.data[0]
          const g = detalhes.data[0]          
            resolve(
              // TODO: criar novo construtor para essas formatações
              new Episodio(
                e.id,
                e.nome,
                e.duracao,
                e.temporada,
                e.ordemEpisodio,
                e.thumbUrl,
                (f) ? f.qtdVezAssistido : 0,
                (f) ? f.nota : 0,
                (g) ? g.notaImdb : '',
                (g) ? g.sinopse : '',
                (g) ? g.dataEstreia : '',
              )
            )
          } )
        } )
      } )
    } )
    return promessa
  }

  //TODOS OS DETALHES
  get detalhes() {
    return axios.get(`http://localhost:9000/api/detalhes/`)
  }

  // TODAS AS AVALIACOES
  get avaliacoes() {
    return axios.get(`http://localhost:9000/api/avaliacoes/`)
  }

  //AVALIACOES POR ID
  avaliacao( id ){
    return axios.get(`http://localhost:9000/api/avaliacoes?episodioId=${ id }`)
  }

  // REGISTRAR Avaliação - NOTA + QtdVezesAssistido
  async registrarNota( { nota, qtdVezAssistido, episodioId } ) {
    const av = await axios.get(`http://localhost:9000/api/avaliacoes?episodioId=${ parseInt( episodioId ) }`)
    if( av.data.length > 0 ) {     
      const idAvaliacao = av.data[0].id
      return axios.put(`http://localhost:9000/api/avaliacoes/${ idAvaliacao }`, { nota, qtdVezAssistido, episodioId } )
    } else {
      return axios.post(`http://localhost:9000/api/avaliacoes/`, { nota, qtdVezAssistido, episodioId } )
    }
  }
}
