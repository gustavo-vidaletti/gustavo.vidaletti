import React from 'react';
import PropTypes from 'prop-types'
import './meuBotao.css';
import { Link } from 'react-router-dom'

const MeuBotao = ( { cor, texto, aoClicar, link, dadosNavegacao, desabilitado  } ) => 
  <button 
  className={`btn ${ cor }`}
  onClick={ aoClicar }
  disabled={ desabilitado }
  >
  {/* { link && (
    <Link className="link-sem-formatacao" to={ link }>{ texto }</Link>
  ) }
  { !link && texto } */}
  { (link) ? (<Link className="link-sem-formatacao" to={ { pathname: link, state: dadosNavegacao } } >{ texto }</Link>) : ( texto ) }
  </button>

export default MeuBotao
  
  MeuBotao.propTypes = {
    texto: PropTypes.string.isRequired,
    cor: PropTypes.oneOf( [ 'verde', 'azul', 'vermelho' ] ),
    aoClicar: PropTypes.func,
    link: PropTypes.string,
    dadosNavegacao: PropTypes.object,
  }
  
  MeuBotao.defaultProps = {
    cor: 'azul'
  }
