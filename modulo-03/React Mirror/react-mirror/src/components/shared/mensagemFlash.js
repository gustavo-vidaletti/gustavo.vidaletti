import React, { Component } from 'react'
import './mensagemFlash.css'
import PropTypes from 'prop-types'

export default class MensagemFlash extends Component {

  constructor( props ) {
    super( props )
    this.idsTimeouts = []
    this.animacao = ''
  }

  fechar = () => {
    this.props.atualizarMensagem( false )
  }

  limparTimeouts() {
    this.idsTimeouts.forEach( clearTimeout )
  }

  // a casa vai cair
  componentWillUnmount() {
    this.limparTimeouts()
  }

  componentDidUpdate( prevProps ) {
    const { deveExibirMensagem, segundos } = this.props
    if ( prevProps.deveExibirMensagem !== deveExibirMensagem ) {
      this.limparTimeouts()
      // this.idsTimeouts.forEach( c => clearTimeout( c ) )
      // for (var i = 0; i < this.idsTimeouts.length; i++) clearTimeout( this.idsTimeouts[ i ] )
      // for (var id of this.idsTimeouts ) clearTimeout( id )
      const novoIdTimeout = setTimeout( () => {
        this.fechar()
      }, segundos * 1000 )
      this.idsTimeouts.push( novoIdTimeout )
    }
  }

  
  render() {
    function seguntosToString( n ) {
      return {
        1: 'um',
        2: 'dois',
        3: 'tres',
        4: 'quatro',
        5: 'cinco',
      }[ n ] || 'tres'
    }

    const { deveExibirMensagem, mensagem, titulo, segundos, tipo } = this.props
    if ( this.animacao || deveExibirMensagem ) {
      this.animacao = deveExibirMensagem ? 'fade-in' : 'fade-out'
    }
    // return <span onClick={ this.fechar } className={ `flash ${ cor } ${ this.animacao } ${ deveExibirMensagem ? '' : 'invisivel' }` }>{ mensagem }</span>
    return (
      <>
      <div className={`flash ${ tipo } ${ this.animacao } ` } onClick={ this.fechar }>
        <div className='msg-titulo'>{titulo}</div>
        <div className='msg-msg'>{mensagem}</div>
        { deveExibirMensagem && (
        <div className={`barrinha ${tipo} desaparecer ${ seguntosToString( segundos )}`}></div>
        ) }
      </div>
      </>
    )
  }
}

// https://reactjs.org/docs/typechecking-with-proptypes.html
MensagemFlash.propTypes = {
  atualizarMensagem: PropTypes.func.isRequired,
  deveExibirMensagem: PropTypes.bool.isRequired,
  mensagem: PropTypes.string.isRequired,
  titulo: PropTypes.string.isRequired,
  tipo: PropTypes.oneOf( [ 'sucesso', 'perigo', 'erro', 'info' ] ),
  segundos: PropTypes.number,
}

MensagemFlash.defaultProps = {
  cor: 'info',
  segundos: 3
}
