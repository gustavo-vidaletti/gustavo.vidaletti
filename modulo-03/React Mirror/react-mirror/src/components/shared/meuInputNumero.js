import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './meuInputNumero.css';

export default class MeuInputNumero extends Component {
  
  perderFoco = evt => {
    const { obrigatorio, atualizarValor } = this.props
    const valor = evt.target.value
    const erro = obrigatorio && !valor
    atualizarValor( { valor, erro } )
  }
  
  render() {
    const { placeholder, visivel, mensagemCampo, deveExibirErro, obrigatorio } = this.props
    return visivel ? (
      <React.Fragment>
      {
        mensagemCampo && <span>{ mensagemCampo }</span>
      }
        <div className="lugar-do-input">
      {
        // <input type="number" className={ deveExibirErro ? 'erro' : '' } placeholder={ placeholder } onBlur={ this.perderFoco } />
        <input
         type='number'
         className={`inputz ${ (obrigatorio) ? 'obrigatorio' : '' }`}
         placeholder={ placeholder }
         onBlur={ this.perderFoco }
       ></input>
      }
      {
        deveExibirErro && <span className="mensagem-erro">* obrigatório</span>
      }
      </div>
    </React.Fragment>
  ) : null
}
}

MeuInputNumero.propTypes = {
  visivel: PropTypes.bool,
  placeholder: PropTypes.string,
  atualizarErro: PropTypes.func,
  deveExibirErro: PropTypes.bool.isRequired,
  obrigatorio: PropTypes.bool,
  mensagemCampo: PropTypes.string,
}

MeuInputNumero.defaultProps = {
  obrigatorio: false,
  deveExibirErro: false,
}
