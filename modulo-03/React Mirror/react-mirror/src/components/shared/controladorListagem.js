import React, { Component } from 'react'
import './controladorListagem.css'

export default class ControladorListagem extends Component {

  constructor ( props ) {
    super( props )
    this.state = {
      botaoSelecionado: this.props.botaoSelecionado,
      inverso: this.props.inverso
    }
  }

  // TODO: https://reactjs.org/docs/react-component.html#static-getderivedstatefromprops
  componentWillReceiveProps( props ) {
    this.setState( {
      botaoSelecionado: props.botaoSelecionado,
      inverso: props.inverso
    } )
  }

  clicou = ( nr ) => {
    if( parseInt( this.state.botaoSelecionado ) === nr){
      this.props.inverter()
    } else {
      this.props.ordenar( nr )
    }
  }

  render(){
    const { botaoSelecionado, inverso} = this.state
    return (
      <>
      <div className="bloco-cotrole">
      <i onClick={ this.clicou.bind( this, 1) } className={`fas fa-sort-alpha-${( ( botaoSelecionado !== 1 ) || (botaoSelecionado === 1 && !inverso ) ) ? `down` : `up` } ${(botaoSelecionado === 1) ? ' selecionado' : ''} `} ></i>
      <i onClick={ this.clicou.bind( this, 2) } className={`fas fa-sort-numeric-${( ( botaoSelecionado !== 2 ) || (botaoSelecionado === 2 && !inverso ) ) ? `down` : `up` } ${(botaoSelecionado === 2) ? ' selecionado' : ''} `} ></i>
      <i onClick={ this.clicou.bind( this, 3) } className={`fas fa-sort-amount-${( ( botaoSelecionado !== 3 ) || (botaoSelecionado === 3 && !inverso ) ) ? `down` : `up` } ${(botaoSelecionado === 3) ? ' selecionado' : ''} `} ></i>
      </div>
      </>
    )
  }
}
