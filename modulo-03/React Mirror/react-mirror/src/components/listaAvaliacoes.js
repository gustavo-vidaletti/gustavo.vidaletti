import React, { Component } from 'react'
import Loader from './shared/loader';
import EpisodioListagemUi from './episodioListagemUi';
import './listaAvaliacoes.css'

export default class ListaAvaliacoes extends Component {
  constructor( props ) {
    super( props )
    this.state = {
      lista: this.props.listaEpisodios.epsAvaliados,
      loader: false,
    }
  }

  render() {
    const { loader } = this.state
    if ( loader ) return (
      <Loader />
    )
    if ( this.state.lista.length === 0) return (
      <>
        <h1>Lista de Avaliações</h1>
        <h5>Nenhuma avaliação até o momento</h5>
      </>
    )

    return (
    <>
    <div className="container">
      <h1>Lista de Avaliações</h1>
      <EpisodioListagemUi lista={ this.state.lista } />
    </div>
  </>
  ) }
}
