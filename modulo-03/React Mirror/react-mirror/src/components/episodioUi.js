import React from 'react'

// https://reactjs.org/docs/components-and-props.html
const EpisodioUi = props => {
  const { episodio } = props
  return (
    <React.Fragment>
      <h2 className="margem-menor">{ episodio.temporadaEpisodio } - { episodio.nome }</h2>
      <h4 className="margem-menor">Duração: { episodio.duracaoEmMin }</h4>
      <h5 className="margem-menor">{ (episodio.qtdVezesAssistido) ? `Assistido: ${ episodio.qtdVezesAssistido } Vez${( episodio.qtdVezesAssistido === 1 ) ? '' : 'es'}` : 'Não Assistido' }</h5>
      <img src={ episodio.thumbUrl } alt={ episodio.nome } width="700px"></img>
      <span>{ (episodio.nota) ? `Nota: ${ episodio.nota }` : ' Não Avaliado' }</span>
     </React.Fragment>
  )
}

export default EpisodioUi
