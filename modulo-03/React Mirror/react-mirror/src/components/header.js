import React, { Component } from 'react';
import './header.css'
import { Link } from 'react-router-dom'
import Logo from '../img/logo-sem-brilho.png'

export default class Header extends Component {

  render(){

    return (
      <>
      <header className="header-mirror">
        <div className="logo">
          <Link to='/'>
            <img src={ Logo } alt="Black Mirror" className="header-logo"/>
          </Link>
        </div>

        <div className="navegacao">
          <ul>
            <li>
              <Link to='/'>Página Inicial</Link>
            </li>
            <li>
              <Link to='/avaliacoes'>Avaliações</Link>
            </li>
            <li>
              <Link to='/episodios'>Lista de Episódios</Link>
            </li>
          </ul>
        </div>
      </header>
      </>
    )
  }
}
