import React, { Component } from 'react';
import EpisodioUi from '../components/episodioUi'
import MeuInputNumero from '../components/shared/meuInputNumero'
import MeuBotao from '../components/shared/meuBotao'
import MensagemFlash from '../components/shared/mensagemFlash'
import Mensagens from '../constants/mensagens'
import Loader from './shared/loader';

export default class PaginaInicial extends Component {
  constructor( props ) {
    super( props )

    this.arrayBuscados = []
    this.arrayProximos = []

    this.state = {
      loader: true,
      episodio: '',
      deveExibirMensagem: false,
      mensagem: '',
      titulo: '',
      deveExibirErro: false,
      botoesDesabilitados: false
    }
  }
  
  componentDidMount() {
    this.listaEpisodios = this.props.listaEpisodios
        setTimeout( () => {
          this.setState(
            {
              episodio: this.listaEpisodios.episodioAleatorio,
              loader: false
            }
          )
        }, 300 )
  }

  atualizarMensagem = devoExibir => {
    this.setState( {
      deveExibirMensagem: devoExibir
    } )
  }

  proximo = () => {
    this.arrayBuscados.push( this.state.episodio.nome )
    let episodio
    if (this.arrayProximos.length){
      episodio = this.listaEpisodios.buscarPorNome( this.arrayProximos.pop() )
    } else {
      episodio = this.listaEpisodios.episodioAleatorio  
    }
    this.setState( {
      episodio,
      deveExibirMensagem: false
    } )
  }

  anterior = () => {
    if(this.arrayBuscados.length === 0) return
    this.arrayProximos.push( this.state.episodio.nome )
    const ep = this.listaEpisodios.buscarPorNome( this.arrayBuscados.pop() )
    this.setState( {
      episodio: ep,
      deveExibirMensagem: false
    } )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio,
      botoesDesabilitados: true
    } )
  }

  exibeMensagem = ( { tipo, titulo, mensagem } ) => {
    this.setState( {
      tipo, titulo, mensagem, deveExibirMensagem: true
    } )
  }

  registrarNota = ( { valor, erro } ) => {
    this.setState( {
      deveExibirErro: erro,
      botoesDesabilitados: false

    } )

    if ( erro ) {
      return 
    }
    
    const { episodio } = this.state
    let tipo, titulo, mensagem
    if ( episodio.validarNota( valor ) ) {
      episodio.avaliar( valor ).then( () => {
        tipo = Mensagens.SUCESSO.TIPO
        titulo = Mensagens.SUCESSO.TITULO
        mensagem = Mensagens.SUCESSO.REGISTRO_NOTA
        this.exibeMensagem( { tipo, titulo, mensagem } )
      } )
    } else {
      tipo = Mensagens.ERRO.TIPO
        titulo = Mensagens.ERRO.TITULO
      mensagem = Mensagens.ERRO.NOTA_INVALIDA
      this.exibeMensagem( { tipo, titulo, mensagem } )
    }
  }

  render() {
    const { episodio, deveExibirMensagem, titulo, mensagem, tipo, deveExibirErro, loader, botoesDesabilitados } = this.state
    if(loader) return (
      <>
      <Loader />
      </>
    )
    
    return (
      <>
        <MensagemFlash 
        atualizarMensagem={ this.atualizarMensagem } 
        tipo={ tipo } 
        deveExibirMensagem={ deveExibirMensagem } 
        titulo={ titulo }
        mensagem={ mensagem } 
        segundos={ 5 } />

        { episodio && <EpisodioUi episodio={ episodio } /> }
        
        <MeuInputNumero placeholder="1 a 5"
          mensagemCampo="Qual sua nota para este episódio?"
          obrigatorio={ ( episodio.qtdVezesAssistido > 0 && episodio.nota === 0) }
          atualizarValor={ this.registrarNota }
          deveExibirErro={ deveExibirErro }
          visivel={ episodio && episodio.assistido }/>

        <div className="botoes">
          <MeuBotao 
            texto="Anterior"
            cor="azul"
            aoClicar={ this.anterior }
            desabilitado={ botoesDesabilitados }
          />

          <MeuBotao
            texto="Próximo"
            cor="azul"
            aoClicar={ this.proximo }
            desabilitado={ botoesDesabilitados }
          />
          <MeuBotao
            texto="Já Assisti"
            cor="verde"
            aoClicar={ this.marcarComoAssistido }
          />
          {/* <MeuBotao 
            texto="Ver Notas"
            cor="vermelho"
            link="/avaliacoes"
            dadosNavegacao={ { listaEpisodios: this.listaEpisodios } }
          /> */}
        </div>
      </>
    )
  }
}
