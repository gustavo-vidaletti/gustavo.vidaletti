import React from 'react'
import './episodioListagemUi.css'
import { Link } from 'react-router-dom'

const EpisodioListagemUi = props => {

  const { lista } = props
  return (
   <>
    {
      lista.map( ( ep ) => { 
        return (
          <Link className="no-link" to={`/episodio/${ ep.id }`} key={ep.id}>
          <div className="bloco-ep" style={ { background: `url(${ep.thumbUrl})` } }>
          <div className="escuro-ep">
          <div className="nome-ep">{ep.nome}</div>
          <div className="nota-ep">{ep.temporadaEpisodio}</div>
          <div className="barra-baixo">
          { /* TODO: componente estrelas */ }
          <i className={ `${( parseInt( ep.nota ) >= 1 ) ? 'fas ' : 'far '} fa-star`}></i>
          <i className={ `${( parseInt( ep.nota ) >= 2 ) ? 'fas ' : 'far '} fa-star`}></i>
          <i className={ `${( parseInt( ep.nota ) >= 3 ) ? 'fas ' : 'far '} fa-star`}></i>
          <i className={ `${( parseInt( ep.nota ) >= 4 ) ? 'fas ' : 'far '} fa-star`}></i>
          <i className={ `${( parseInt( ep.nota ) >= 5 ) ? 'fas ' : 'far '} fa-star`}></i>
          </div>
          </div>
          </div>
         </Link>
        )
      } ) 
    } 
   </> 
  )
  
}

export default EpisodioListagemUi

/*
<div className="bloco-ep">
<h4 className="titulo-ep">{`${ep.temporadaEpisodio} - ${ep.nome}`}</h4>
<div className="bloco-labels">
<span className="label-ep">{(ep.qtdVezesAssistido > 0) ? `Assistiu: ${ep.qtdVezesAssistido} vez${(ep.qtdVezesAssistido !== 1)? 'es' : '' } `: 'Não Assistiu' }</span>
{ ep.qtdVezesAssistido > 0 && (
<span className="label-ep">{`Nota: ${ep.nota}`}</span>
) }
</div>
</div>
*/
