import React, { Component } from 'react';
import './campoDeBusca.css'

export default class CampoDeBusca extends Component {
  render() {
    return (
      <>
      <div className="campo-de-busca">
        <input 
          type="text"
          placeholder="Buscar"
          onBlur={ this.props.buscar }
          >
        </input>

        <button
        onClick={ this.props.limparFiltro }
        >Limpar Filtro</button>
      </div>
      </>
    )
  }
}
