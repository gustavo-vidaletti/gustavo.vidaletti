import React, { Component } from 'react'
import ReactMirrorAPI from '../api/reactMirrorAPI';
import Loader from './shared/loader';
import './detalheEpisodio.css'

export default class DetalheEpisodio extends Component {

  constructor( props ) {
    super( props )
    this.reactMirrorAPI = new ReactMirrorAPI()
    this.state ={
      loader: true,
      episodio: ''
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.reactMirrorAPI.episodioFULL( id ).then( ep => {
      const episodio = ep
      this.setState( {
        loader: false,
        episodio
      } )
    } )
  }

  render() {
    const { loader, episodio } = this.state
    if( loader ) return (
      <>
      <Loader />
      </>
    )
    return (
      <>
      <h2 className="titulo">{ episodio.temporadaEpisodio } - { episodio.nome }</h2>
      <img src={ episodio.thumbUrl } alt={ episodio.nome } width="700px"></img>
      <div className="bloco-lista">
        <span className="bolder">Sinopse:</span>
        <p>
        { episodio.sinopse }
        </p>
        <ul className="lista-detalhe">
          <li>
            <span className="bolder">Duração: </span> { episodio.duracaoEmMin }
          </li>
          <li>
            <span className="bolder">{ (episodio.qtdVezesAssistido) ? 'Assistido: ' : 'Não Assistido ' }</span> 
            { ` ${ episodio.qtdVezesAssistido } vez${( episodio.qtdVezesAssistido === 1 ) ? '' : 'es'}` }
          </li>
          <li>
          <span className="bolder">Estreia:</span>
            { ` ${new Date(episodio.dataEstreia).toLocaleDateString('pt-br')}`}
          </li>
          <li>
          <span className="bolder">Nota:</span>
            {/* TODO: passar formatação para modelo */}
            { (episodio.nota) ? ` ${ parseFloat( episodio.nota ).toFixed(1) }` : ' Não Avaliado' }
          </li>
          <li>
          <span className="bolder">Nota IMDB:</span>
            { (episodio.notaIMDB) ? ` ${ parseFloat( episodio.notaIMDB / 2.0 ).toFixed(1)  }` : 'Indisponível' }
          </li>
        </ul>
      </div>
      </>
    )
  }
}

