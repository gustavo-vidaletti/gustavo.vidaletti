import React, { Component } from 'react';
import Loader from './shared/loader';
import EpisodioListagemUi from './episodioListagemUi'
import '../utils/array-prototype'
import ControladorListagem from './shared/controladorListagem';
import CampoDeBusca from './campoDeBusca';



export default class ListagemEpisodios extends Component {
  constructor( props ) {
    super( props )
    // this.reactMirrorAPI = new ReactMirrorAPI()
    this.listaEpisodios = this.props.listaEpisodios
    this.state = {
      loader: true,
      lista: this.listaEpisodios.todosEpisodios.ordenarPorEstreia(),
      botaoSelecionado: 2,
      inverso: false,
    }
  }

  componentDidMount() {    
      this.setState( {
        loader: false
      } )
  }

  inverter = () => {
    this.setState( {
      lista: this.state.lista.reverse(),
      inverso: !this.state.inverso
    } )
  }

  ordenar = ( id ) => {
    let lista = []
    switch( id ) {
      case 1: lista = this.state.lista.ordenarPorNome()
        break;
      case 2: lista = this.state.lista.ordenarPorEstreia()
        break;
      case 3: lista = this.state.lista.ordenarPorDuracao()
        break
      default: // Nothing?
    }
    this.setState( {
      lista: lista,
      botaoSelecionado: id,
      inverso: false,
    } )
  }

  limparFiltro = () => {
    this.setState(
      {
        lista: this.listaEpisodios.todosEpisodios.ordenarPorEstreia(),
        botaoSelecionado: 2,
        inverso: false,
      }
    )
  }
  
  buscar = ( evt ) => {
    // TODO: Buscar filtrando na lista ou mudar na lista com o server    
    const lista = this.state.lista.filter( ep => {
      const array = ep.sinopse.toLowerCase().match( evt.target.value.toLowerCase() )
      return ( ( array ) && ( array.length > 0 ) )

    } )
    this.setState( {
      lista: lista
      }
    )
    
  }

  render() {
    const { loader } = this.state
    if ( loader ) return (
      <Loader />
    )

    return (
    <>
      <h1>Lista de Episódios</h1>
      <EpisodioListagemUi lista={ this.state.lista } />

      <CampoDeBusca
      limparFiltro={ this.limparFiltro }
      buscar={ this.buscar }
      />
      <ControladorListagem
        botaoSelecionado={ this.state.botaoSelecionado } 
        inverso={ this.state.inverso }  
        inverter={ this.inverter }
        ordenar={ this.ordenar }
      />
    </>
    )
  }
}
