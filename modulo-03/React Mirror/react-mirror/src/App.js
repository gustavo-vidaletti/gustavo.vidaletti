import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';
import PaginaInicial from './components/paginaInicial';
import ListaAvaliacoes from './components/listaAvaliacoes';
import Header from './components/header'
import ListaEpisodios from './models/listaEpisodios';
import Loader from './components/shared/loader';
import ReactMirrorAPI from './api/reactMirrorAPI';
import ListagemEpisodios from './components/listagemEpisodios';
import DetalheEpisodio from './components/detalheEpisodio';


class App extends Component {
  constructor( props ) {
    super( props )
    this.reactMirrorAPI = new ReactMirrorAPI()
    this.state = {
      loader:true,
    }
  }

  componentDidMount() {
    // TODO: utilizar Promise.all
    this.reactMirrorAPI.todosEpisodios.then( dados => {
      this.reactMirrorAPI.avaliacoes.then( avaliacoes => {
        this.reactMirrorAPI.detalhes.then( detalhes => {
          this.listaEpisodios = new ListaEpisodios( dados.data , avaliacoes.data, detalhes.data )
          this.setState( {
            loader: false
          } )
        } )
      } )
    } )
  }

  render() {
    if(this.state.loader) return <Loader />
    return (
      <>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossOrigin="anonymous"></link>
      <div className="App">
        <Router>
          <>
              <Route path="/" component={ Header }/>
              <div className="header-fix">
              <Route path="/" exact component={ () => <PaginaInicial listaEpisodios={ this.listaEpisodios }/> } />
              <Route path="/avaliacoes" component={ () => <ListaAvaliacoes listaEpisodios={ this.listaEpisodios }/> } />
              <Route path="/episodios" component={ () => <ListagemEpisodios listaEpisodios={ this.listaEpisodios }/> } />
              <Route path="/episodio/:id" component={ DetalheEpisodio }/>

              </div>
          </>
        </Router>
      </div>
      </>
    );
  }
}

export default App;
