import Episodio from "./episodio";
import ReactMirrorAPI from "../api/reactMirrorAPI";

// inspiração: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#Getting_a_random_integer_between_two_values
// essa função não está sendo exportada, logo ela é "privada"
function _sortear(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

export default class ListaEpisodios {
  constructor ( episodiosDoServidor, avaliacoes, detalhes ) {
    // this.todos = episodiosDoServidor.map( e => new Episodio(e.id, e.nome, e.duracao, e.temporada, e.ordemEpisodio, e.thumbUrl, e.qtdVezesAssistido, e.nota ) )
    this.reactMirrorAPI = new ReactMirrorAPI()
    this.avaliacoes = avaliacoes    
    this.detalhes = detalhes
    this.todos = episodiosDoServidor.map( e => {
      const f = this.avaliacoes.find( av => av.episodioId === e.id )
      const g = this.detalhes.find( det => det.episodioId === e.id )
      return new Episodio(
        e.id,
        e.nome,
        e.duracao,
        e.temporada,
        e.ordemEpisodio,
        e.thumbUrl,
        (f) ? f.qtdVezAssistido : 0,
        (f) ? f.nota : 0,
        (g) ? g.notaImdb : 0,
        (g) ? g.sinopse : '',
        (g) ? g.dataEstreia : '',
        ) 
      } 
    )    
  }

  get episodioAleatorio() {
    const indice = _sortear( 0, this.todos.length )
    return this.todos[ indice ]
  }

  get todosEpisodios() {
    return this.todos
  }

  get epsAvaliados() {
    return this.todos.filter( e => e.nota > 0 )
  }

  buscarPorNome( nome ) {
    return this.todos.find( (e) => e.nome === nome)
  }

  buscarPorID( id ) {
    return this.todos.find( (e) => e.id === id )
  }

  marcarComoAssistido( episodio ) {
    const episodioParaMarcar = this.todos.find( e => e.nome === episodio.nome )
    episodioParaMarcar.assistido = true
    episodioParaMarcar.qtdVezesAssistido += 1
  }
}
