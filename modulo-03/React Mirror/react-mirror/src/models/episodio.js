import '../utils/number-prototypes.js'
import ReactMirrorAPI from '../api/reactMirrorAPI'

export default class Episodio {
  constructor(id, nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido, nota, notaIMDB, sinopse, dataEstreia) {
    this.reacMirrorAPI = new ReactMirrorAPI()
    this.id = id
    this.nome = nome
    this.duracao = duracao
    this.temporada = temporada
    this.ordemEpisodio = ordemEpisodio
    this.thumbUrl = thumbUrl
    this.qtdVezesAssistido = qtdVezesAssistido || 0
    this.nota = nota || 0
    this.notaIMDB = notaIMDB
    this.sinopse = sinopse
    this.dataEstreia = new Date( dataEstreia )    
    // C#: this.qtdVezesAssistido = qtdVezesAssistido ?? 0
  }

  validarNota( nota ) {
    nota = parseInt( nota )
    //return 1 <= nota && nota <= 5
    return nota.estaEntre( 1, 5 )
  }

  avaliar( nota ) {
    this.nota = parseInt( nota )
    this.assistido = true
    return this.reacMirrorAPI.registrarNota( { nota: this.nota, qtdVezAssistido: this.qtdVezesAssistido, episodioId: this.id })
  }

  quantidadeAssistido( qtd ){
    this.qtdVezesAssistido = qtd
  }

  get duracaoEmMin() {
    return `${String(this.duracao).padStart(2, '0')}min`
  }

  get temporadaEpisodio() {
    return `S${ this.temporada.toString().padStart(2, '0') }E${ this.ordemEpisodio.toString().padStart(2, '0') }`
  }
}
