import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';

class App extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio

    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( {
      episodio
    } )
  }

  toggleAssistido() {
    const { episodio } = this.state
    this.listaEpisodios.toggleAssistido( episodio )
    this.setState( {
      episodio
    } )
  }
  
  render() {
    const { episodio } = this.state
    return (
      <div className="App">
        <header className="App-header">
          <h2>{ episodio.nome }</h2>
          <h4>{ (episodio.assistido) ? 'Assistido' : 'Não Assistido' }</h4>
          <img src={episodio.thumbUrl} alt={episodio.nome}></img>
          {/* <p>Olá Mundo! { new Date().toLocaleString( 'pt-br' ) }</p> */}
          <button onClick={ this.sortear.bind( this ) }>Próximo</button>
          <button onClick={ this.toggleAssistido.bind( this ) }>{( episodio.assistido ) ? 'Marcar como \'Não Assistido\'' : 'Marcar como \'Assistido\''}</button>
        </header>
      </div>
    );
  }
}

export default App;
