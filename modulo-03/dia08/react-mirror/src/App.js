import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './components/episodioUi';
import MensagemFlash from './components/shared/mensagemFlash';
import MeuInputNumero from './components/shared/meuInputNumero';

class App extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    this.arrayBuscados = []
    this.arrayProximos = []
    this.refBarra = React.createRef()
    this.refMsg = React.createRef()
    // definindo estado inicial de um componente
    let ep = this.listaEpisodios.episodioAleatorio
    this.state = {
      episodio: ep,
      deveExibirMensagem: false,
    }
  }

  proximo = () => {
    this.arrayBuscados.push( this.state.episodio.nome )
    let episodio
    if (this.arrayProximos.length){
      episodio = this.listaEpisodios.buscarPorNome( this.arrayProximos.pop() )
    } else {
      episodio = this.listaEpisodios.episodioAleatorio  
    }
    this.setState( {
      episodio,
    } )
  }

  anterior = () => {
    if(this.arrayBuscados.length === 0) return
    this.arrayProximos.push( this.state.episodio.nome )
    const ep = this.listaEpisodios.buscarPorNome( this.arrayBuscados.pop() )
    this.setState( {
      episodio: ep
    } )
  }

  toggleAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.toggleAssistido( episodio )
    this.setState( {
      episodio
    } )
  }
  
  apagarMensagem = () => {
    this.appMsn = undefined
  }

  registrarNota = ( evt ) => {
    const { episodio } = this.state
    let tipo = 'sucesso'
    let msg = 'Nota Alterada'
    let tit = 'Sucesso!'
    if( parseInt( evt.target.value ) > 5 ) {
      evt.target.value = 5
      tit = "Erro!"
      msg = "Nota máxima: 5"
      tipo = "erro"
    } else if ( parseInt( evt.target.value ) < 1 ) {
      evt.target.value = 1
      tit = "Erro!"
      msg = "Nota mínima: 1"
      tipo = "erro"
    }
    episodio.avaliar( evt.target.value )
    this.setState( { 
      episodio,
    } )

    this.appMsn = ( 
      <MensagemFlash
      titulo={tit} 
      mensagem={msg} 
      tipo={tipo}
      tempoEmSegundos={5}
      referenciaBarrinha = {this.refBarra}
      referenciaMensagem = {this.refMsg}
      apagarMensagem = { this.apagarMensagem.bind( this ) }
      />
    )
  }

  registrarQuantidadeDeVezesQueAssistiuOEpisodio = (evt) => {
    const { episodio } = this.state
    episodio.quantidadeAssistido( evt.target.value )
    this.setState( { 
      episodio
    } )
  }

  gerarCampoNaTela = () => {
    const { episodio } = this.state
    if(this.state.episodio.assistido){
     return (
       <div>
        <div>
         <h5 className="margem-menor">Quantas vezes você assistiu esse episódio?</h5>
         <MeuInputNumero 
          placeholder='Surpreenda-me'
          type='number'
          onChange={ this.registrarQuantidadeDeVezesQueAssistiuOEpisodio.bind( this )} 
          value={ episodio.qtdVezesAssistido }
         />
        </div>
        <div>
         <h5 className="margem-menor">Qual sua nota para esse episódio?</h5>
         <MeuInputNumero
          placeholder='1 à 5'
          type='number'
          onChange={ this.registrarNota.bind( this ) } 
          value={ episodio.nota }
          // obrigatorio={true}
         />
        </div>
       </div>
     ) }
    }
    
    render() {
      const { episodio } = this.state
      return (
      <div className="App" ref='app'>
        { this.appMsn }
        <header className="App-header">
          <EpisodioUi episodio={ episodio } />
          <div className="div-dos-botoes">
           <button className="botao" onClick={ this.anterior.bind( this ) }>Anterior</button>
            <button className="botao" onClick={ this.proximo.bind( this ) }>Próximo</button>
           <button className="botao" onClick={ this.toggleAssistido.bind( this ) }>{( episodio.assistido ) ? 'Marcar como \'Não Assistido\'' : 'Marcar como \'Assistido\''}</button>
         </div>
          { this.gerarCampoNaTela() }
        </header>
      </div>
    );
  }
}

export default App;
