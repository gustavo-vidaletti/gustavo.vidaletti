import React, { Component } from 'react'

export default class EpisodioUi extends Component {
  render(){
    const { episodio } = this.props
   return (
    <div>
      <h2 className="margem-menor">{episodio.nome} - { episodio.temporadaEpisodio }</h2>
      <h4 className="margem-menor">Duração: { episodio.duracaoEmMinutos }</h4>
      <h4 className="margem-menor">{ (episodio.assistido) ? 'Assistido' : 'Não Assistido' }</h4>
      <img src={episodio.thumbUrl} alt={episodio.nome}></img>
    </div>

   )
  }
}

/*
  // https://reactjs.org/docs/components-and-props.html
const EpisodioUi = props => {
  const { episodio } = props
  return (
    <React.Fragment>
      <h2>{ episodio.nome }</h2>
      <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
      <span>Já assisti? { episodio.assistido ? 'sim' : 'não' }, { episodio.qtdVezesAssistido } ve(zes)</span>
      <span>{ episodio.duracaoEmMin }</span>
      <span>{ episodio.temporadaEpisodio }</span>
      <span>{ episodio.nota || 'sem nota' }</span>
    </React.Fragment>
  )
}

export default EpisodioUi
*/
