import React, { Component } from 'react';
import './mensagemFlash.css'
import PropTypes from 'prop-types'

export default class MensagemFlash extends Component {
constructor( props ){
  super( props )
  this.timeOutID = []
}
  apagarMensagem = ( tempo = 0, referencia) => {
    this.apagarTimeOuts()
    this.timeOutID.push(setTimeout( () => {
      referencia.current.style.opacity = 0
 
      this.timeOutID.push( setTimeout( () => {
        referencia.current.style.display = 'none'
        this.props.apagarMensagem()
      }, 700 ) )
      
    }, tempo ) )
  }

  apagarTimeOuts(){
    this.timeOutID.forEach( n => clearTimeout(n) )
  }

  render() {
    const { titulo, mensagem, tipo = 'sucesso', tempoEmSegundos = 3 } = this.props
    const { referenciaBarrinha, referenciaMensagem } = this.props
    const tempoEmMilis = tempoEmSegundos * 1000

    this.apagarMensagem( tempoEmMilis, referenciaMensagem )
    //Remover estilo anterior
    setTimeout( () =>  referenciaMensagem.current.removeAttribute( 'style' ) , 50)
    setTimeout( () =>  referenciaBarrinha.current.removeAttribute( 'style' ) , 50)
    setTimeout( () => {
      referenciaBarrinha.current.style.transition = `width ${ parseInt( tempoEmSegundos ) }s linear`
      referenciaBarrinha.current.style.width = 0
    }, 60 )
    return (
      <>
      <div className={`mensagem ${tipo}`} ref={ referenciaMensagem } onClick={ this.apagarMensagem.bind(this, 0, referenciaMensagem) }>
        <div className='msg-titulo'>{titulo}</div>
        <div className='msg-msg'>{mensagem}</div>
        <div className={`barrinha ${tipo}`} ref={ referenciaBarrinha }></div>
      </div>
      </>
    )
  }
}

MensagemFlash.propTypes = {
  mensagem: PropTypes.string.isRequired,
  titulo: PropTypes.string.isRequired,
  tipo: PropTypes.string,
  tempoEmSegundos: PropTypes.number,
  referenciaMensagem: PropTypes.any.isRequired,
  referenciaBarrinha: PropTypes.any.isRequired,

}
