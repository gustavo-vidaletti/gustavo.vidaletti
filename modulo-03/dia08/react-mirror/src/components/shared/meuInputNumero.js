import React, { Component } from 'react';
import './meuInputNumero.css';
export default class MensagemFlash extends Component {
  
  render() {
    const { type, placeholder, obrigatorio} = this.props
    const ehObrigatorio = ( obrigatorio )? 'obrigatorio': ''
    return (
       <span className={` ${ehObrigatorio} `}>
       <input
        className={`inputz ${ehObrigatorio}`}
       type={type}
       placeholder={ placeholder }
       onChange={ this.props.onChange }
       value={ this.props.value }
      ></input>
      </span>
    )
  }
}
