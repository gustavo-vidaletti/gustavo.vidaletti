export default class Episodio {
  constructor ( nome, duracao, temporada, ordemEpisodio, thumbUrl, assistido = false, qtdVezesAssistido = 0, nota = 0) {
    this.nome = nome
    this.duracao = duracao
    this.temporada = temporada
    this.ordemEpisodio = ordemEpisodio
    this.thumbUrl = thumbUrl
    this.assistido = assistido
    this.qtdVezesAssistido = qtdVezesAssistido
    this.nota = nota
  }

  avaliar( nota ) {
    this.nota = parseInt(nota)
  }

  validarNota = ( nota ) => {
    nota = parseInt( nota )
    return nota.estaEntre( 1, 5 )
  }

  quantidadeAssistido( qtd ){
    this.qtdVezesAssistido = qtd
  }

  get duracaoEmMinutos() {
    return `${String(this.duracao).padStart(2, '0')}min`
  }

  get temporadaEpisodio(){
    return  `S${String(this.temporada).padStart(2, '0')}E${String(this.ordemEpisodio).padStart(2, '0')}`
  }
}

// eslint-disable-next-line no-extend-native
Number.prototype.estaEntre = function( min, max ){
    return  min <= this && this <= max
}
