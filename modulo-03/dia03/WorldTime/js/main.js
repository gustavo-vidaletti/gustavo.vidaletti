moment.locale( 'pt-br' );
var loopTempo;
var idsIntervalos = []

function inicializarRelogios() {
  [ {
      id: 'alemanha',
      timezone: 'Europe/Berlin',
      nome: 'Alemanha'
    },
    {
      id: 'brasil',
      timezone: 'America/Sao_Paulo',
      nome: 'Brasil'
    },
    {
      id: 'finlandia',
      timezone: 'Europe/Helsinki',
      nome: 'Finlândia'
    },
    {
      id: 'franca',
      timezone: 'Europe/Paris',
      nome: 'França'
    },
    {
      id: 'grecia',
      timezone: 'Europe/Athens',
      nome: 'Grécia'
    },
    {
      id: 'islandia',
      timezone: 'Atlantic/Reykjavik',
      nome: 'Islândia'
    },
    {
      id: 'japao',
      timezone: 'Asia/Tokyo',
      nome: 'Japão'
    },
    {
      id: 'libano',
      timezone: 'Asia/Beirut',
      nome: 'Líbano'
    },
    {
      id: 'mexico',
      timezone: 'America/Mexico_City',
      nome: 'México'
    },

  ].forEach( pais => {
    idsIntervalos.push( setInterval( () => {
      atualizarHorario( pais.id, pais.timezone )
      piscar( pais.id )
    }, 1000 ) )
    document.getElementById( `nome-${pais.id}` ).innerText = pais.nome
    atualizarHorario( pais.id, pais.timezone )
  } )
}
inicializarRelogios()

function clicou( pais, cidade, timeZone ) {
  this.pararLoops();
  abrirModal()
  escutarTeclado()

  let tz = moment.tz( timeZone )
  document.getElementById( 'titulo' ).innerHTML = `${pais} - ${cidade}`
  document.getElementById( 'data' ).innerHTML = tz.format( 'L' );
  document.getElementById( 'dois-pontos-1' ).innerHTML = ":";
  document.getElementById( 'dois-pontos-2' ).innerHTML = ":";
  document.getElementById( 'textinho' ).innerHTML = textinho( timeZone );

  this.idsIntervalos.push( setInterval( () => {
    this.atualizarHorario( false, timeZone )
    this.piscar()
  }, 500 ) );
  this.atualizarHorario( false, timeZone )
  this.piscar()
}

function textinho( timeZone ) {
  let tz = moment.tz( timeZone )
  let saoPaulo = moment.tz( 'America/Sao_Paulo' )
  let horaBrasil = parseInt( saoPaulo.format( 'Z' ) )
  let horaMundo = parseInt( tz.format( 'Z' ) )
  //Tem XXX horas a menos|mais que brasília
  let dif = Math.abs( horaMundo - horaBrasil );
  let plural = ( dif !== 1 ) ? "s" : ""
  if ( horaMundo > horaBrasil ) {
    return `Tem ${dif} hora${plural} a mais que São Paulo`
  } else if ( horaMundo < horaBrasil ) {
    return `Tem ${dif} hora${plural} a menos que São Paulo`
  } else {
    return `Se encontra no mesmo fuso horário que São Paulo`
  }
}


function atualizarHorario( id = false, timeZone ) {
  let tz = moment.tz( timeZone )

  let h = tz.format( 'HH' );
  let m = tz.format( 'mm' );

  if ( id ) {
    document.getElementById( `horas-${id}` ).innerHTML = h;
    document.getElementById( `minutos-${id}` ).innerHTML = m;
    document.getElementById( `segundos-${id}` ).innerHTML = tz.format( 'ss' );

  } else {
    document.getElementById( 'horas' ).innerHTML = h;
    document.getElementById( 'minutos' ).innerHTML = m;
    document.getElementById( 'segundos' ).innerHTML = tz.format( 'ss' );

    if ( h === 23 && m === 59 || ( h === 0 && m === 0 ) ) {
      document.getElementById( 'data' ).innerHTML = tz.format( 'L' );
    }
  }
}

function escutarTeclado() {
  document.addEventListener( 'keydown', function( event ) {
    if ( event.keyCode === 27 ) {
      fecharModal()
    }

  } );
}

function abrirModal() {
  document.getElementById( "escurito" ).style.display = 'flex';
  document.getElementById( "modalzito" ).style.display = 'flex';
  document.getElementById( "modalzito" ).style.opacity = '1';

  window.setTimeout( () => {
    document.getElementById( "escurito" ).style.opacity = '1';
    document.getElementById( "modalzito" ).style.top = 'calc(50% - 30vh)';
  }, 50 )
}

function fecharModal() {
  if ( !taPausado ) {
    this.inicializarRelogios()
  } else {
    this.pararLoops()
  }



  document.getElementById( "escurito" ).style.opacity = '0'
  document.getElementById( "modalzito" ).style.top = '-60vh'

  window.setTimeout( () => {
    document.getElementById( "escurito" ).style.display = 'none'
    document.getElementById( "modalzito" ).style.opacity = '0'
    document.getElementById( "modalzito" ).style.display = 'none'
  }, 500 )

}

var aparecendo = true
var pisca

function piscar( id = false ) {
  aparecendo = !aparecendo;
  idsIntervalos.push( setInterval( () => {
    if ( id ) {
      document.getElementById( `dois-pontos-1-${id}` ).style.opacity = ( aparecendo ) ? '1' : '0'
      document.getElementById( `dois-pontos-2-${id}` ).style.opacity = ( aparecendo ) ? '1' : '0'
    } else {
      document.getElementById( 'dois-pontos-1' ).style.opacity = ( aparecendo ) ? '1' : '0'
      document.getElementById( 'dois-pontos-2' ).style.opacity = ( aparecendo ) ? '1' : '0'
    }
  } ) );
}

function pararLoops() {
  this.idsIntervalos.forEach( id => {
    clearInterval( id );
  } )
}

var todosRelogios = false

function toggleTodosRelogios( interruptor = !todosRelogios ) {
  let array = document.getElementsByClassName( 'escuro-bandeira' )

  this.todosRelogios = interruptor;

  let olhinho = document.getElementById( "olho" );

  if ( interruptor ) {
    olhinho.classList.add( "fa-eye-slash" );
    olhinho.classList.remove( "fa-eye" );
  } else {
    olhinho.classList.add( "fa-eye" );
    olhinho.classList.remove( "fa-eye-slash" );

  }

  for ( let i = 0; i < array.length; i++ ) {
    if ( interruptor ) {
      array[ i ].style.opacity = 1;
    } else {
      array[ i ].removeAttribute( 'style' );
    }
  }

}

var taPausado = false;

function togglePause( interruptor = !taPausado ) {

  this.taPausado = interruptor;
  let controle = document.getElementById( "controle" );

  if ( interruptor ) {
    controle.classList.add( "fa-play" );
    controle.classList.remove( "fa-pause" );
    this.pararLoops();
  } else {
    controle.classList.add( "fa-pause" );
    controle.classList.remove( "fa-play" );
    this.inicializarRelogios();
  }
}
