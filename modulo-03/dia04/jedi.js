// function Jedi(nome, sabre){
//   this.nome = nome
//   this.sabre = sabre
// }

// Jedi.prototype.atacarComSabre = function(){
//   console.log(`${this.nome} atacou com sabre ${this.sabre.toLowerCase()}`);
// }

class Jedi {
  constructor( nome, sabre ) {
    this.nome = nome
    this.sabre = sabre
  }

  atacarComSabre() {
    console.log( "BIND!" )
    // let funcao = function(){
    //   console.log(`${this.nome} atacou com sabre ${this.sabre.toLowerCase()}`)
    // }
    //funcao = funcao.bind(this)
    setTimeout( function() {
      console.log( `${this.nome} atacou com sabre ${this.sabre.toLowerCase()}` )
    }.bind( this ), 1000 )
  }

  atacarComSelf() {
    console.log( "SELF!" )
    var self = this
    setTimeout( () => {
      console.log( `${self.nome} atacou com sabre ${self.sabre.toLowerCase()}` )
    }, 1000 )
  }

  atacarComArrowFunctions() {
    console.log( "ARROW!" );

    setTimeout( () => {
      console.log( `${this.nome} atacou com sabre ${this.sabre.toLowerCase()}` )
    }, 200 )
  }

}


var luke = new Jedi( "Luke", "Azul" )
var windu = new Jedi( "Mace Windu", "Rosa" )
var yoda = new Jedi( "Yoda", "Verde" )
