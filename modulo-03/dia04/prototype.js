var bacomAVida = function( uppercase = false ) {
  var texto = this + " Bacon!!!"
  return uppercase ? texto.toUpperCase() : texto
}

String.prototype.bacon = bacomAVida
Boolean.prototype.bacon = bacomAVida

var a = 'Gustavo';

console.log( a.bacon() );
console.log( a.bacon( true ) );

var b = true;
console.log( b.bacon() );
