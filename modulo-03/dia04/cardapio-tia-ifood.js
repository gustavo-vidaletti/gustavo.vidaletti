function cardapioTiaIFood( veggie = true, comLactose = true ) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]

  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' )
  }

  cardapio = cardapio.concat( [
    'pastel de carne',
    'empada de legumes marabijosa'
  ] )

  if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    const indiceEnroladinho = cardapio.indexOf( 'enroladinho de salsicha' )
    cardapio.splice( indiceEnroladinho, 1 )
    const indicePastelCarne = cardapio.indexOf( 'pastel de carne' )
    cardapio.splice( indicePastelCarne, 1 )
  }

  return cardapio;
}

//console.log(cardapioTiaIFood()) 
// esperado: [ 'cuca de uva', 'pastel de queijo', 'empada de legumes marabijosa' ]
