var luke = {
  nome: "Luke Skywalker",
  idade: 23,
  temAsDuasMaos: false,
  campoQueNaoQueremos: "afudê"
}

let txt = JSON.stringify( luke, ( campo, valor ) => {
  if ( campo === 'campoQueNaoQueremos' ) {
    return undefined;
  }
  return valor
} )

let obj = JSON.parse( txt )
