let executaIssoQuandoCarregar

window.nome = "Global NAME";
window.idade = "Global AGE";

var luke = {
  nome: "Luke Skywalker",
  idade: 23,
  imprimirInformacoes: function( force = false, sister = false ) {
    // if(force)console.log("irmã: " + sister);
    // if(sister)console.log("Força: " + force);
    return `${this.nome}: ${this.idade} anos`
  }
}

let outraFunc = luke.imprimirInformacoes;

console.log( "SEM NADA: " + outraFunc() );


// Call - "apontamento do this", depois os parametros
console.log( "CALL: " + outraFunc.call( luke, 45, "Princesa leia" ) ); // <--- APONTANDO PRO LUKE
// Apply - passa o "apontamento do this", depois os parametros em um array
console.log( "APPLY: " + outraFunc.apply( luke, [ 45, "Princesa leia" ] ) ); // <--- APONTANDO PRO LUKE
//Bind - 
var bindFunc = outraFunc.bind( luke )
console.log( "BIND: " + bindFunc( 56, 'leia?' ) );



setTimeout( () => {
  console.log( `dentro do setTimeout: ${this.nome}` );

}, 800 )





console.log( luke.imprimirInformacoes() );
