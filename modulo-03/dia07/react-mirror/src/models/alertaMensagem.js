/* eslint-disable react/react-in-jsx-scope */
import React from 'react';

export default class alertaMensagem {
  apagarMensagem( tempo = 0, referencia) {
    setTimeout( () => {
      referencia.current.style.opacity = 0

      setTimeout( () => {
        referencia.current.style.display = 'none'
      }, 700)
      
    }, tempo )
  }
  
  sucesso( mensagem, titulo, tempo, referencia, referenciaBarrinha ) {
    const tempoEmMilis = tempo * 1000
    this.apagarMensagem( tempoEmMilis, referencia, referenciaBarrinha )
    setTimeout( () =>  referencia.current.removeAttribute( 'style' ) , 50)
    setTimeout( () =>  referenciaBarrinha.current.removeAttribute( 'style' ) , 50)
    setTimeout( () => {
      referenciaBarrinha.current.style.transition = 'width 5s linear'
      referenciaBarrinha.current.style.width = 0
    }, 80 )
    return (
      <div className='mensagem sucesso' ref={referencia}>
        <div className='msg-titulo'>{titulo}</div>
        <div className='msg-msg'>{mensagem}</div>
        <div className='barrinha' ref={referenciaBarrinha}></div>
      </div>
    )
}
}
