import React, { Component } from 'react';
import './App.css';
import './models/alertaMensagem.css';
import ListaEpisodios from './models/listaEpisodios';
import AlertaMensagem from './models/alertaMensagem';

class App extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    this.alertaMensagem = new AlertaMensagem()
    this.arrayBuscados = []
    this.arrayProximos = []
    this.appMsn = undefined
    this.elementoMensagemRef = React.createRef()
    this.elementoBarrinhaRef = React.createRef()
    this.timeOutID = 0
    // definindo estado inicial de um componente
    let ep = this.listaEpisodios.episodioAleatorio
    this.state = {
      episodio: ep,
    }
  }

  proximo = () => {
    clearTimeout( this.timeOutID )
    this.arrayBuscados.push( this.state.episodio.nome )
    let episodio
    if (this.arrayProximos.length){
      episodio = this.listaEpisodios.buscarPorNome( this.arrayProximos.pop() )
    } else {
      episodio = this.listaEpisodios.episodioAleatorio  
    }
    this.setState( {
      episodio,
    } )
  }

  anterior = () => {
    if(this.arrayBuscados.length === 0) return
    this.arrayProximos.push( this.state.episodio.nome )
    const ep = this.listaEpisodios.buscarPorNome( this.arrayBuscados.pop() )
    this.setState( {
      episodio: ep
    } )
  }

  toggleAssistido = () => {
    clearTimeout( this.timeOutID )
    this.appMsn = undefined
    const { episodio } = this.state
    this.listaEpisodios.toggleAssistido( episodio )
    this.setState( {
      episodio
    } )
  }

  registrarNota = (evt) => {
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState( { 
      episodio
    } )
    this.appMsn = this.alertaMensagem.sucesso('Nota registrada!', 'Sucesso!', 5, this.elementoMensagemRef, this.elementoBarrinhaRef)
    this.timeOutID = setTimeout( () => this.appMsn = undefined, 5020 )
  }

  registrarQuantidadeDeVezesQueAssistiuOEpisodio = (evt) => {
    const { episodio } = this.state
    episodio.quantidadeAssistido( evt.target.value )
    this.setState( { 
      episodio
    } )
  }

  gerarCampoNaTela = () => {
    const { episodio } = this.state
    if(this.state.episodio.assistido){
     return (
       <div>
        <div>
         <h5 className="margem-menor">Quantas vezes você assistiu esse episódio?</h5>
         <input type="number" placeholder="quero só ver" onChange={this.registrarQuantidadeDeVezesQueAssistiuOEpisodio.bind( this )} value={ episodio.qtdVezesAssistido }></input>
        </div>
        <div>
         <h5 className="margem-menor">Qual sua nota para esse episódio?</h5>
         <input type="number" placeholder="1 a 5" onChange={ this.registrarNota.bind( this ) } value={ episodio.nota }></input>
        </div>
       </div>
     ) }
    }
    
    render() {
      const { episodio } = this.state
      return (
      <div className="App" ref='app'>
      {/* {this.alertaMensagem.info('Nota Registrada!', 'Sucesso!', 5)} */}
        <header className="App-header">
          <h2 className="margem-menor">{episodio.nome} - { episodio.temporadaEpisodio }</h2>
          <h4 className="margem-menor">Duração: { episodio.duracaoEmMinutos }</h4>
          <h4 className="margem-menor">{ (episodio.assistido) ? 'Assistido' : 'Não Assistido' }</h4>
          <img src={episodio.thumbUrl} alt={episodio.nome}></img>
          <div className="div-dos-botoes">
          <button className="botao" onClick={ this.anterior.bind( this ) }>Anterior</button>
          <button className="botao" onClick={ this.proximo.bind( this ) }>Próximo</button>
          <button className="botao" onClick={ this.toggleAssistido.bind( this ) }>{( episodio.assistido ) ? 'Marcar como \'Não Assistido\'' : 'Marcar como \'Assistido\''}</button>
          { this.gerarCampoNaTela() }
          { this.appMsn }
          </div>
        </header>
      </div>
    );
  }
}

export default App;
