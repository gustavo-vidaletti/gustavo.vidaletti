const series = JSON.parse( '[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]' )
console.log( 'Séries: ' )
console.log( series )


Array.prototype.invalidas = function() {
  return this.filter( ( s ) => {
    const date = new Date()
    const verificaAno = s.anoEstreia <= date.getFullYear()
    let verificaAtributos = true
    for ( const atributo in s ) {
      if ( !s[ atributo ] ) {
        verificaAtributos = false
        break;
      }
    }
    // Object.values(this).some( s => s )
    return !verificaAno || !verificaAtributos
  } )
}

Array.prototype.invalidasBernardo = function() {
  const invalidas = this.filter( serie => {
    const algumCampoInvalido = Object.values( serie ).some( v => v === null || typeof v === 'undefined' )
    const anoEstreiaInvalido = serie.anoEstreia > new Date().getFullYear()
    return algumCampoInvalido || anoEstreiaInvalido
  } )

  return `Séries inválidas: ${ invalidas.map( s => s.titulo ).join( ' - ' ) }`
}

console.log( 'Series Inválidas: ' )
console.log( series.invalidas() )

Array.prototype.filtrarPorAno = function( ano ) {
  return this.filter( ( s ) => {
    return ( s.anoEstreia >= ano )
  } )
}

console.log( 'Filtrar Por Ano: 2015 ' )
console.log( series.filtrarPorAno( 2015 ) )

Array.prototype.procurarPorNome = function( nome ) {
  encontrou = false;
  this.forEach( ( serie ) => {
    let achou = serie.elenco.find( n => nome === n )
    if ( achou ) {
      encontrou = true
    }
  } )
  return encontrou
}

console.log( 'Estou em alguma série?' );
console.log( 'Andrew Lincoln: ' + series.procurarPorNome( 'Andrew Lincoln' ) )
console.log( 'Gustavo Lodi Vidaletti: ' + series.procurarPorNome( 'Gustavo Lodi Vidaletti' ) )
console.log( 'Ana Cristina Ferreira Lodi: ' + series.procurarPorNome( 'Ana Cristina Ferreira Lodi' ) )

Array.prototype.mediaDeEpisodios = function() {
  let soma = 0
  this.forEach( ( serie ) => {
    soma += serie.numeroEpisodios
  } )

  return parseFloat( soma / this.length )
}

Array.prototype.mediaBernardo = function() {
  // return parseFloat( this.reduce( ( acc, elem ) => acc + elem.numeroEpisodios, 0 ) / this.length )
  // OU
  return parseFloat( this.map( s => s.numeroEpisodios ).reduce( ( acc, elem ) => acc + elem, 0 ) / this.length )
}

console.log( 'Media de episódios: ' + series.mediaDeEpisodios() );

Array.prototype.totalSalarios = function( indice ) {
  function imprimirBRLOneLine( valor ) {
    return parseFloat( valor.toFixed( 2 ) ).toLocaleString( 'pt-BR', {
      style: 'currency',
      currency: 'BRL'
    } )
  }
  const salarioDirecao = 100000 * this[ indice ].diretor.length
  const salarioAtores = 40000 * this[ indice ].elenco.length
  return imprimirBRLOneLine( parseFloat( salarioAtores + salarioDirecao ) )
}

console.log( `custo de GOT: ${ series.totalSalarios( 1 ) }` )

console.log( 'Buscas' );

Array.prototype.queroGenero = function( genero ) {
  return this.filter( ( serie ) => {
    return Boolean( serie.genero.find( ( g ) => g === genero ) )
  } )
}

console.log( 'Genero:' );
console.log( 'Terror: ' );
console.log( series.queroGenero( 'Terror' ) );
console.log( 'CAOS: ' );
console.log( series.queroGenero( 'Caos' ) );

console.log( 'Titulo:' );

Array.prototype.queroTitulo = function( titulo ) {
  let palavrasTitulo = titulo.split( ' ' )
  return this.filter( ( serie ) => {
    let palavrasSerie = serie.titulo.split( ' ' )
    return Boolean( palavrasSerie.find( ( palavra ) => palavra.includes( palavrasTitulo ) ) )
  } )
}

console.log( 'The:' );
console.log( series.queroTitulo( 'The' ) );

Array.prototype.ordenarPeloUltimoNome = function() {
  this.sort( ( a, b ) => {
    arrayA = a.split( ' ' )
    arrayB = b.split( ' ' )
    return arrayA[ arrayA.length - 1 ].localeCompare( arrayB[ arrayB.length - 1 ] )
  } )
}

Array.prototype.creditos = function( indice ) {
  const elemento = document.querySelector( '.creditos' )
  elemento.removeAttribute( 'style' )
  elemento.style.transition = 'top 24s'
  elemento.innerHTML = ""
  this[ indice ].elenco.ordenarPeloUltimoNome()
  this[ indice ].diretor.ordenarPeloUltimoNome()
  arrayParaInner = []

  arrayParaInner.push( `<h1 id="nome-serie">${this[indice].titulo}</h1>` )
  arrayParaInner.push( '<h2>Diretores</h2>' )
  arrayParaInner.push( '<div class="diretores">' )
  // aqui
  this[ indice ].diretor.forEach( ( d ) => {
    arrayParaInner.push( `<div>${d}</div>` )
  } )
  arrayParaInner.push( '</div>' )
  arrayParaInner.push( '<h2>Elenco</h2>' )
  arrayParaInner.push( '<div class="elenco">' )
  // aqui
  this[ indice ].elenco.forEach( ( d ) => {
    arrayParaInner.push( `<div>${d}</div>` )
  } )
  arrayParaInner.push( '</div>' )


  arrayParaInner.forEach( ( s ) => {
    elemento.innerHTML += s
  } )

  setTimeout( () => {
    elemento.style.top = '-100vh'
    setTimeout( () => {
      elemento.removeAttribute( 'style' )
    }, 14000 )
  }, 1000 )
}

String.prototype.matchesNomeAbreviado = function() {
  let verifica = this.match( '\\s+[a-zA-Z]{1}\.\\s+' )
  return ( verifica ) ? Boolean( verifica[ 0 ] ) : undefined
}

String.prototype.letraAbreviada = function() {
  if ( this.matchesNomeAbreviado() ) return this.match( '\\s+[a-zA-Z]{1}\.\\s+' )[ 0 ].charAt( 1 )
}

Array.prototype.serieComTodosNomesAbreviados = function() {
  return this.filter( ( serie ) => {
    return !serie.elenco.find( s => !s.matchesNomeAbreviado() )
  } )
}

Array.prototype.mostrarPalavraSecreta = function() {
  let palavra = "#"
  let array = this.serieComTodosNomesAbreviados()
  array[ 0 ].elenco.forEach( ( s ) => {
    palavra = palavra.concat( s.letraAbreviada() )
  } )
  return palavra
}

console.log( "Mostrar PALAVRA SECRETA" );
console.log( series.mostrarPalavraSecreta() );
