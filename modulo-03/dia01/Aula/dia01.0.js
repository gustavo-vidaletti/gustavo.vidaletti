var cores = [ "verde", "vermelho", "azul" ]

function imprimirNoConsoleF( e ) {
  console.log( e );
}

var imprimirNoConsole = ( e ) => {
  console.log( e )
}

cores.forEach( function( e ) {
  imprimirNoConsoleF( e )
} )

cores.forEach( e => imprimirNoConsole( e ) );

// document.write("Oi")
