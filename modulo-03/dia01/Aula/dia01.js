// var hello = "\"hello\"";
var hell = 'Hell'
var hello = `${hell}o`

function somar( a, b ) {
  return a + b
}

function imprimirOi() {
  log( "Oi" )
}

/* imprimirOi !== imprimirOi()
 * um é a referência para a função
 * o outro é a chamada da função 
 */

var somarFn = ( a, b ) => a + b

//Pega a referência da memoria
var apontaParaFuncao = somar

var resultado = apontaParaFuncao( 1, 2 )
var resultadoFn = somarFn( 1, 2 )



console.log( "Normal" + resultado );
console.log( `Fn: ${resultadoFn}` );
