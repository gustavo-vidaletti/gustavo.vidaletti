// var tipo = {raio, tipoCalculo}

// IIFE - USAR ; antes da linha!!!!!!!!! 
// USANDO "~" ou '!' antes da funçao, acaba com a necessidade
!(function(texto = "Default"){ //Setando um valor pré-definido
    console.log(`Lista De ${texto}!`);    
})("Exercícios"); //AQUI JA CHAMA A FUNCAO ANONIMA

~(function(){
    console.log(`Caso De ${arguments[0]}!`);    
})("Teste"); //AQUI JA CHAMA A FUNCAO ANONIMA

function calcularCirculo(obj) {
    let raio = obj.raio
    let tipo = obj.tipoCalculo

    switch (tipo) {
        //II * R²
        case 'A' || 'a': return Math.PI * Math.pow(raio, 2);
        //2 * II * R
        case 'C' || 'c': return 2 * Math.PI * raio;

        default: return 0
    }
}


function naoBissexto(ano) {
    var multiploDe400 = (ano % 400 === 0)
    var multiploDe4 = (ano % 4 === 0)
    var multiploDe100 = (ano % 100 === 0)
    var multiploDe100EMultiploDe400 = (multiploDe100 && multiploDe400)

    return !((multiploDe400) || ((multiploDe4) && (!multiploDe100EMultiploDe400)))
}


function somarPares(array) {
    var soma = 0;
    for (var i = 0; i < array.length; i++) {
        if (i % 2 === 0) soma = soma + array[i];
    }
    return soma;
}


function adicionar(x) {
    return function somar(y) {
        return x + y
    }
}

// HIPSTER!!!
var adicionarOneLine = op1 => op2 => op1 + op2;


// function imprimirBRL(valor) {
//     let inteiro = parseInt(valor)
//     let quebrado = parseFloat(valor - inteiro).toFixed(2)
//     let quebradoInteiro = Math.abs(parseInt(quebrado * 100))

//     if (quebradoInteiro >= 100) {
//         if (inteiro >= 0) inteiro++;
//         else inteiro--;
//         quebradoInteiro = 0;
//     }

//     if (quebradoInteiro < 10) {
//         quebradoInteiro = "0" + quebradoInteiro
//     }

//     inteiro = inteiro.toString();

//     let comPontos = ""
//     let cont = 0
//     for (let i = inteiro.length - 1; i >= 0; i--) {
//         if (cont === 3) {
//             cont = 0;
//             comPontos = "." + comPontos;
//         }
//         comPontos = inteiro.charAt(i) + comPontos
//         cont++
//     }
//     return `R$ ${comPontos},${quebradoInteiro}`;
// }


function imprimirBRLOneLine(valor) {
    return "R$ " + parseFloat(valor).toFixed(2).toLocaleString('pt-br')
}


/*
console.log("ANOS BISSEXTOS: ");
for(let i = 1600; i < 2019; i++){
    if(!naoBissexto(i)) console.log("Ano: " + i);
}
*/

// SOMAR PARES
// console.log(adicionar(5)(5));
