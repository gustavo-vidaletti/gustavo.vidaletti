var moedas = function () {
  function imprimirMoeda(params) {
    const { valor, separadorDecimal, separadorMilhar, colocarMoeda, colocarNegativo } = params

    let inteiro = parseInt(valor)
    let decimalInteiro = decimalInteiroDoNumero(valor)

    if (decimalInteiro >= 100) {
      if (inteiro >= 0) inteiro++;
      else inteiro--;
      decimalInteiro = 0;
    }

    if (decimalInteiro < 10) {
      decimalInteiro = "0".concat(decimalInteiro)
    }
    let negativo = (inteiro < 0);
    inteiro = Math.abs(inteiro).toString();

    let inteiroFinal = ""

    let cont = 0
    for (let i = inteiro.length - 1; i >= 0; i--) {
      if (cont === 3) {
        cont = 0;
        inteiroFinal = separadorMilhar.concat(inteiroFinal);
      }
      inteiroFinal = inteiro.charAt(i).concat(inteiroFinal)
      cont++
    }

    let formatado = inteiroFinal + separadorDecimal + decimalInteiro;

    if (negativo) return `${colocarNegativo(formatado)}`
    else return `${colocarMoeda(formatado)}`

    // if (antes) return `${inteiroFinal}${separadorDecimal}${decimalInteiro}`;
    // else return `${inteiroFinal}${separadorDecimal}${decimalInteiro} ${unidadeMonetaria} `;
  }
  function arredondar(valor, precisao = 2) {
    let fator = Math.pow(10, precisao)
    let resultado = Math.ceil(valor * fator) / fator
    return resultado;
  }

  function decimalDoNumero(valor) {
    return parseFloat(valor - parseInt(valor))
  }

  function decimalInteiroDoNumero(valor) {
    let decimal = decimalDoNumero(valor)
    let decimalFix = (decimal * 100).toFixed(5)
    decimal = (Math.ceil(decimalFix) / 100)
    return Math.abs(parseInt(decimal * 100))
  }
  return {
    imprimirBRL: function (valor) {
      let obj = {
        valor: valor,
        separadorMilhar: ".",
        separadorDecimal: ",",
        colocarMoeda: numeroFormatado => "R$ " + numeroFormatado,
        colocarNegativo: numeroFormatado => "-R$ " + numeroFormatado,
      }
      return imprimirMoeda(obj);
    },

    imprimirGBP: function (valor) {
      let obj = {
        valor: valor,
        separadorMilhar: ",",
        separadorDecimal: ".",
        colocarMoeda: numeroFormatado => "£ " + numeroFormatado,
        colocarNegativo: numeroFormatado => "-£ " + numeroFormatado,
      }
      return imprimirMoeda(obj);
    },

    imprimirFR: function (valor) {
      let obj = {
        valor: valor,
        separadorMilhar: ".",
        separadorDecimal: ",",
        colocarMoeda: numeroFormatado => numeroFormatado + " €",
        colocarNegativo: numeroFormatado => "-" + numeroFormatado + " €",
      }
      return imprimirMoeda(obj);
    },

    imprimirDolar: function (valor) {
      let obj = {
        valor: valor,
        unidadeMonetaria: "US$",
        separadorMilhar: ",",
        separadorDecimal: ".",
        colocarMoeda: numeroFormatado => "US$ " + numeroFormatado,
        colocarNegativo: numeroFormatado => "US$ (" + numeroFormatado + ")",
      }
      return imprimirMoeda(obj);
    },
    imprimirBRLOneLine: function (valor) {
      return "R$ " + parseFloat(valor).toFixed(2).toLocaleString('pt-br')
    }
  }
}()
