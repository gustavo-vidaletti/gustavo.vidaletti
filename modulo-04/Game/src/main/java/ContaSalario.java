
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "CONTA_SALARIO")
@PrimaryKeyJoinColumn(name = "id_conta")
public class ContaSalario extends Conta10{
    
    public ContaSalario(){
        super.setTipo(ContaType.SALARIO);
    }

    @Column(name = "cnpj_pagador")
    private Integer cnpjPagador;
}
