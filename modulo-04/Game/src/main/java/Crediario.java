
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "CREDIARIO")
@SequenceGenerator(allocationSize = 1, name = "CREDIARIO_SEQ", sequenceName = "CREDIARIO_SEQ")
public class Crediario {
    
    @Id
    @GeneratedValue(generator = "CREDIARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToOne(mappedBy = "crediario")
    @JoinColumn(name = "id_cliente")
    private Cliente3 cliente;
    
}
