
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "PLATINUM_TABELAO")
@DiscriminatorValue("PLATINUM")
public class PlatinumTabelao extends CartaoTabelao {
    @Column(name = "platinum_limite")
    private Integer platinumLimite;
}
