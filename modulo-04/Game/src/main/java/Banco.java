
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "banco")
@Inheritance(strategy = InheritanceType.JOINED)
public class Banco {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "banco_seq", sequenceName= "banco_seq")
    @GeneratedValue(generator = "banco_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
}
