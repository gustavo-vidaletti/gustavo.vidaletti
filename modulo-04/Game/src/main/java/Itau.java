
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "itau")
@PrimaryKeyJoinColumn(name = "id_banco")
public class Itau extends Banco {
    
    @Column(name = "meta_emprestimos")
    private Integer metaEmprestimos;
    
}
