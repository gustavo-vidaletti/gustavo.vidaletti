
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "CHARMELEON")
@SequenceGenerator(allocationSize = 1, name = "CHARMELEON_SEQ", sequenceName = "CHARMELEON_SEQ")
public class Charmeleon {
    
    @Id
    @GeneratedValue(generator = "CHARMELEON_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToMany(mappedBy = "charmeleon", cascade = CascadeType.ALL)
    private List<AtaqueEspecial> ataques;
    
    
}
