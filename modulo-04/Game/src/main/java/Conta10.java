
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "CONTA")
@Inheritance(strategy = InheritanceType.JOINED)
public class Conta10 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "conta_seq", sequenceName= "conta_seq")
    @GeneratedValue(generator = "conta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Enumerated(EnumType.STRING)
    private ContaType tipo;

    @Column(name = "cpf")
    private Integer cpf;
    
    public ContaType getTipo() {
        return tipo;
    }

    public void setTipo(ContaType tipo) {
        this.tipo = tipo;
    }
    
    
}
