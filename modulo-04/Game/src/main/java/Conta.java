
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONTA")
@SequenceGenerator(allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
public class Conta {
    @Id
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name = "CONTA_PRODUTO",
            joinColumns = {
                @JoinColumn(name = "ID_CONTA")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "ID_PRODUTO")
            }
            )
    private List<Produto> produtos;
    
}
