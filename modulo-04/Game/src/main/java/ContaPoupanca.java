
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "CONTA_POUPANCA")
@PrimaryKeyJoinColumn(name = "id_conta")
public class ContaPoupanca extends Conta10{
    public ContaPoupanca(){
        super.setTipo(ContaType.POUPANCA);
    }
    
    @Column(name = "dia_rendimento")
    private Integer diaRendimento;
    
    
}
