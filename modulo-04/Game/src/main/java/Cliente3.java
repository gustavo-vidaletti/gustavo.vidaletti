
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "CLIENTE3")
@SequenceGenerator(allocationSize = 1, name = "CLIENTE3_SEQ", sequenceName = "CLIENTE3_SEQ")
public class Cliente3 {
    @Id
    @GeneratedValue(generator = "CLIENTE3_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_crediario")
    private Crediario crediario;
    
}
