
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE2")
@SequenceGenerator(allocationSize = 1, name = "CLIENTE2_SEQ", sequenceName = "CLIENTE2_SEQ")
public class Cliente2 {
    @Id
    @GeneratedValue(generator = "CLIENTE2_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany( mappedBy = "clientes")
    private List<Loja> lojas;
    
}
