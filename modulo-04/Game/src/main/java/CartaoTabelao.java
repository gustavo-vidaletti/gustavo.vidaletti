
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gustavo.vidaletti
 */
@Entity
@Table(name = "Cartao_TABELAO")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo_cartao",discriminatorType = DiscriminatorType.STRING)
public class CartaoTabelao {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "conta_seq", sequenceName= "conta_seq")
    @GeneratedValue(generator = "conta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name = "dia_vencimento")
    private Integer diaVencimento;
}
