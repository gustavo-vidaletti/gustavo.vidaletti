/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gustavo.vidaletti
 */
public class Connector {

    private static Connection conn;

    public static Connection connect() {
        try {
            if ( conn != null && conn.isValid( 10 ) ) {
                return conn;
            }
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "lotr", "lotr");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro de Conexão", ex);
        }/* finally{
          System.out.println("Terminou");
        }*/
        return conn;
    }
}
