package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.PersonagemJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.PersonagemDTO;

public abstract class PersonagemDAO<T extends PersonagemJoin> extends AbstractDAO<T> {
    
    public T parseFrom(PersonagemDTO dto, Usuario usuario, Class<T> clazz){
        T entity = null;
        try {
            entity = clazz.newInstance();
            entity.setId(dto.getId());
            entity.setNome(dto.getNome());
            entity.setUsuario(usuario);

        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return entity;
    }
}
