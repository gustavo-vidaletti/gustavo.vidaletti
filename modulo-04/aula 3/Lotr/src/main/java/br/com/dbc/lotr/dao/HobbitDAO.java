package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.PersonagemDTO;

public class HobbitDAO extends PersonagemDAO<HobbitJoin> {

    public HobbitJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        HobbitJoin ent = super.parseFrom(dto, usuario, HobbitJoin.class);
        ent.setDanoHobbit(dto.getDanoHobbit());
        return ent;
    }
    
    protected Class<HobbitJoin> getEntityClass(){
        return HobbitJoin.class;
    }
}
