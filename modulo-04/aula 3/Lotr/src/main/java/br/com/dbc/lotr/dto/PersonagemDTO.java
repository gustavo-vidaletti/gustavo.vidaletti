package br.com.dbc.lotr.dto;

import br.com.dbc.lotr.entity.RacaType;

public class PersonagemDTO {

  private Integer id;
  private String nome;
  private RacaType raca;
  private Double danoElfo;
  private Double danoHobbit;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public RacaType getRaca() {
    return raca;
  }

  public void setRaca(RacaType raca) {
    this.raca = raca;
  }

  public Double getDanoElfo() {
    return danoElfo;
  }

  public void setDanoElfo(Double danoElfo) {
    this.danoElfo = danoElfo;
  }

  public Double getDanoHobbit() {
    return danoHobbit;
  }

  public void setDanoHobbit(Double danoHobbit) {
    this.danoHobbit = danoHobbit;
  }
}
