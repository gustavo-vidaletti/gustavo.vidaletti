package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.PersonagemDTO;

public class ElfoDAO extends PersonagemDAO<ElfoJoin> {

    public ElfoJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        ElfoJoin ent = super.parseFrom(dto, usuario, ElfoJoin.class );
        ent.setDanoElfo(dto.getDanoElfo());
        return ent;
    }
    
    protected Class<ElfoJoin> getEntityClass(){
        return ElfoJoin.class;
    }
}
