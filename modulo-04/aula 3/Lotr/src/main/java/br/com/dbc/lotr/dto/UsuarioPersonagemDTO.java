package br.com.dbc.lotr.dto;

import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;

public class UsuarioPersonagemDTO {

  //Usuario

  private Integer idUsuario;
  private String nome;
  private String apelido;
  private String senha;
  private Long cpf;

  // Endereco
  private EnderecoDTO endereco;

  //Personagem
  private PersonagemDTO personagemDTO;

  public Integer getIdUsuario() {
    return idUsuario;
  }

  public void setId(Integer id) {
    this.idUsuario = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getApelido() {
    return apelido;
  }

  public void setApelido(String apelido) {
    this.apelido = apelido;
  }

  public String getSenha() {
    return senha;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }

  public Long getCpf() {
    return cpf;
  }

  public void setCpf(Long cpf) {
    this.cpf = cpf;
  }

  public EnderecoDTO getEndereco() {
    return endereco;
  }

  public void setEndereco(EnderecoDTO endereco) {
    this.endereco = endereco;
  }

  public PersonagemDTO getPersonagemDTO() {
    return personagemDTO;
  }

  public void setPersonagemDTO(PersonagemDTO personagemDTO) {
    this.personagemDTO = personagemDTO;
  }
}
