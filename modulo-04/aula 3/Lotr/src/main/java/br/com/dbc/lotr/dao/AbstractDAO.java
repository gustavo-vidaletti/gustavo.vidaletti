package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.AbstractEntity;
import br.com.dbc.lotr.entity.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public abstract class AbstractDAO <E extends AbstractEntity> {
  public void criar(E entity){
    Session session = HibernateUtil.getSession();
    session.save(entity);
//        MUDAR O VOID PRA E
//        entity = (E) session.merge(entity);
//        return entity;
  }

  public void atualizar(E entity){
    criar(entity);
  }
  
  protected abstract Class<E> getEntityClass();
  
  public void remover(Integer id){
    Session session = HibernateUtil.getSession();
    session.createQuery("delete from " + getEntityClass().getSimpleName() + " where id = " + id).executeUpdate();
  }

  public void remover(E entity){
    remover(entity.getId());
  }

  public E buscar(Integer id){
    Session session = HibernateUtil.getSession();
    return (E) session.createQuery("select e from " +
            getEntityClass().getSimpleName() + " e" +
            " where id = " + id).uniqueResult();
  }

  public List<E> listar(){
    Session session = HibernateUtil.getSession();
    return session.createCriteria(getEntityClass()).list();
  }

//    public abstract E parseFrom(UsuarioPersonagemDTO dto);
}
