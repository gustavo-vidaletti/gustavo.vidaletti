package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "HOBBIT_PER_CLASS")
@DiscriminatorValue("HOBBIT")
public class HobbitPerClass extends PersonagemPerClass{

    public HobbitPerClass() {
        super.setRaca(RacaType.HOBBIT);
    }
    
    
    @Column(name = "DANO_HOBBIT")
    private Double danoHobbit;

    public Double getDanoHobbit() {
        return danoHobbit;
    }

    public void setDanoHobbit(Double danoHobbit) {
        this.danoHobbit = danoHobbit;
    }
    
    
}
