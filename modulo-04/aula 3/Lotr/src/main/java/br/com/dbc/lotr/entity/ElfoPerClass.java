package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ELFO_PER_CLASS")
@DiscriminatorValue("ELFO")
public class ElfoPerClass extends PersonagemPerClass{

    public ElfoPerClass() {
        super.setRaca(RacaType.ELFO);
    }
    
    @Column(name = "DANO_ELFO")
    private Double danoElfo;

    public Double getDanoElfo() {
        return danoElfo;
    }

    public void setDanoElfo(Double danoElfo) {
        this.danoElfo = danoElfo;
    }
    
}
