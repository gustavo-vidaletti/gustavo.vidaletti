package br.com.dbc.lotr.entity;

import javax.persistence.*;

@Entity
@Table(name = "PERSONAGEM_JOIN")
@Inheritance(strategy = InheritanceType.JOINED)
public class PersonagemJoin extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_JOIN_SEQ", sequenceName = "PERSONAGEM_JOIN_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_JOIN_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    private String nome;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    @Enumerated(EnumType.STRING)
    private RacaType raca;

    //Getters and Setters

    protected RacaType getRaca() {
        return raca;
    }

    protected void setRaca(RacaType raca) {
        this.raca = raca;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
