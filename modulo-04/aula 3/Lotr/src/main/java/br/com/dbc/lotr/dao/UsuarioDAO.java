package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;

public class UsuarioDAO extends AbstractDAO<Usuario> {

  public Usuario parseFrom(UsuarioPersonagemDTO dto){
    Usuario usuarioEntity = null;

    if(dto.getIdUsuario() != null){
      usuarioEntity = buscar(dto.getIdUsuario());
    }
    if( usuarioEntity == null){
      usuarioEntity = new Usuario();
    }
    usuarioEntity.setNome(dto.getNome());
    usuarioEntity.setApelido(dto.getApelido());
    usuarioEntity.setSenha(dto.getSenha());
    usuarioEntity.setCpf(dto.getCpf());

    //Endereco
    EnderecoDTO enderecoDTO = dto.getEndereco();
    Endereco enderecoEntity = new Endereco();
    if(dto.getEndereco().getId() != null){
      enderecoEntity.setId(enderecoDTO.getId());
      int indexEndereco = usuarioEntity.getEnderecos().indexOf(enderecoEntity);
      if(indexEndereco >= 0){
        enderecoEntity = usuarioEntity.getEnderecos().get(indexEndereco);
      }
    }
    enderecoEntity.setLogradouro(enderecoDTO.getLogradouro());
    enderecoEntity.setNumero(enderecoDTO.getNumero());
    enderecoEntity.setBairro(enderecoDTO.getBairro());
    enderecoEntity.setCidade(enderecoDTO.getCidade());
    enderecoEntity.setComplemento(enderecoDTO.getComplemento());

    if(!usuarioEntity.getEnderecos().contains(enderecoEntity)){
      usuarioEntity.getEnderecos().add(enderecoEntity);
    }
    if(!enderecoEntity.getUsuarios().contains(usuarioEntity)){
      enderecoEntity.pushUsuarios(usuarioEntity);
    }
    return usuarioEntity;
  }
  
  protected Class<Usuario> getEntityClass(){
    return Usuario.class;
  }
}
