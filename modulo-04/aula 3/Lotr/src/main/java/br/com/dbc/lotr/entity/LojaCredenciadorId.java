package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LojaCredenciadorId {
    @Column(name = "ID_LOJA")
    private Integer idLoja;
    
    @Column(name = "ID_CREDENCIADOR")
    private Integer idCredenciador;
    
    public LojaCredenciadorId() {
    }
    
    public LojaCredenciadorId(Integer idLoja, Integer idCredenciador) {
        this.idCredenciador = idCredenciador;
        this.idLoja = idLoja;
    }
}
