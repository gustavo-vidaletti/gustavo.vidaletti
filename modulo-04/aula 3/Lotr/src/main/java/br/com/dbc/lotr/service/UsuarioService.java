package br.com.dbc.lotr.service;

import br.com.dbc.lotr.dao.UsuarioDAO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.Usuario;
import org.hibernate.Transaction;

import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioService {

  private static final UsuarioDAO USUARIO_DAO = new UsuarioDAO();
  private static final PersonagemService PERSONAGEM_SERVICE = new PersonagemService();
  private static final Logger LOG = Logger.getLogger(UsuarioService.class.getName());

  public void cadastrarUsuarioEPersonagem(UsuarioPersonagemDTO dto){
    boolean started = HibernateUtil.beginTransaction();
    Transaction transaction = HibernateUtil.getSession().getTransaction();
    try {
      Usuario usuario = USUARIO_DAO.parseFrom(dto);
      USUARIO_DAO.criar(usuario);

      PERSONAGEM_SERVICE.salvarPersonagem(dto.getPersonagemDTO(), usuario);

      if (started) {
        transaction.commit();
      }
    } catch (Exception e){
      transaction.rollback();
      LOG.log(Level.SEVERE, e.getMessage(), e);
    }
  }
}
