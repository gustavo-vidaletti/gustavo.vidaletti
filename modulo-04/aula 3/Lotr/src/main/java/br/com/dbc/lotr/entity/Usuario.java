package br.com.dbc.lotr.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "USUARIO")
public class Usuario extends AbstractEntity{
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "usuario_seq", sequenceName= "usuario_seq")
    @GeneratedValue(generator = "usuario_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;
    
    private String apelido;
    
    private String senha;
    
    private Long cpf;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "usuario_endereco", 
            joinColumns = {
                @JoinColumn( name = "id_usuario")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_endereco")}    
    )
    private List<Endereco> enderecos = new ArrayList<Endereco>();

    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
    private List<Contato> contatos = new ArrayList<Contato>();

    @OneToMany(mappedBy = "usuario")
    private List<PersonagemJoin> personagens = new ArrayList<PersonagemJoin>();

    public void pushPersonagens(PersonagemJoin... personagem){
        this.personagens.addAll(Arrays.asList(personagem));
    }

    public List<PersonagemJoin> getPersonagens() {
        return personagens;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }
    
    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void pushEnderecos(Endereco... enderecos) {
        this.enderecos.addAll(Arrays.asList(enderecos));
    }
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return nome + " {" +
                "\n\tid: " + id +
                "\n\tapelido: '" + apelido + '\'' +
                "\n\tsenha: '" + senha + '\'' +
                "\n\tcpf: " + cpf +
                "\n}";
    }
}
