package br.com.dbc.lotr.entity;

import java.io.Serializable;

public abstract class AbstractEntity implements Serializable {
  public abstract Integer getId();
}
