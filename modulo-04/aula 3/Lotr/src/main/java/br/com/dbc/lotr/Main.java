package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import org.hibernate.Criteria;
import org.hibernate.Session;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

  private static final Logger LOG = Logger.getLogger(Main.class.getName());

  public static void main(String[] args) {
    UsuarioService usuarioService = new UsuarioService();
    UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
    dto.setNome("Gustavo");
    dto.setApelido("Gu");
    dto.setCpf(123l);
    dto.setSenha("123");

    EnderecoDTO enderecoDTO = new EnderecoDTO();
    enderecoDTO.setLogradouro("Rua Liberdade");
    enderecoDTO.setNumero(433);
    enderecoDTO.setBairro("Rio Branco");
    enderecoDTO.setCidade("Porto Alegre");
    enderecoDTO.setComplemento("apto 702");

    dto.setEndereco(enderecoDTO);

    PersonagemDTO personagemDTO = new PersonagemDTO();
    personagemDTO.setRaca(RacaType.ELFO);
    personagemDTO.setNome("Elfo do Gustavo");
    personagemDTO.setDanoElfo(123d);

    dto.setPersonagemDTO(personagemDTO);

    usuarioService.cadastrarUsuarioEPersonagem(dto);

    System.exit(0);
  }


  public static void oldMain(String[] args) {
    Session session = null;
    Transaction transaction = null;

    try{
      session = HibernateUtil.getSession();
      transaction = session.beginTransaction();

      Usuario usuario = new Usuario();
      usuario.setNome("Gustavo");
      usuario.setApelido("Gu");
      usuario.setCpf(1235l);
      usuario.setSenha("1234");

      Endereco end = new Endereco();
      end.setLogradouro("Rua Liberdade");
      end.setBairro("Rio Branco");
      end.setCidade("Porto Alegre");
      end.setNumero(433);
      end.setComplemento("Apto 702");

      Endereco end2 = new Endereco();
      end2.setLogradouro("Rua Casemiro de Abreu");
      end2.setBairro("Rio Branco");
      end2.setCidade("Porto Alegre");
      end2.setNumero(100);

      TipoContato email = new TipoContato();
      email.setNome("email");
      email.setQuantidade(4);

      Contato contato = new Contato();
      contato.setTipoContato(email);
      contato.setUsuario(usuario);
      contato.setValor("vidaletti@email.com");

      usuario.pushContatos(contato);
      usuario.pushEnderecos(end);
      usuario.pushEnderecos(end2);
      session.save(usuario);

      ElfoTabelao elfoTabelao = new ElfoTabelao();
      elfoTabelao.setDanoElfo(100d);
      elfoTabelao.setNome("Legolas");

      session.save(elfoTabelao);

      HobbitTabelao hobbitTabelao = new HobbitTabelao();
      hobbitTabelao.setDanoHobbit(10d);
      hobbitTabelao.setNome("Frodo");

      session.save(hobbitTabelao);

      ElfoPerClass elfo = new ElfoPerClass();
      elfo.setNome("Legolas Per Class");
      elfo.setDanoElfo(100d);

      session.save(elfo);

      HobbitPerClass hobbitPerClass = new HobbitPerClass();
      hobbitPerClass.setNome("Bilbo");
//            hobbitPerClass.setDanoHobbit(new Double(10));
//            hobbitPerClass.setDanoHobbit(10.0);
      hobbitPerClass.setDanoHobbit(10d);
      session.save(hobbitPerClass);


      ElfoJoin elfoJoin = new ElfoJoin();
      elfoJoin.setNome("Legolas Joinha");
      elfoJoin.setDanoElfo(100d);
      session.save(elfoJoin);

      HobbitJoin hobbitJoin = new HobbitJoin();
      hobbitJoin.setNome("Gimli Joinha");
      hobbitJoin.setDanoHobbit(11d);
      session.save(hobbitJoin);

      /*
       * SELECT usua.NOME as Nome, ender.LOGRADOURO as Rua, ender.BAIRRO as Bairro
       * from usuario usua
       * inner join usuario_endereco ue on ue.ID_USUARIO = usua.ID
       * inner join endereco ender on ender.ID = ue.ID_ENDERECO
       * where lower(ender.BAIRRO) like '%branco%';
       */
      String busca = "%branco%";

      Criteria criteria = session.createCriteria(Usuario.class);

      criteria.createAlias("enderecos","endereco");
      criteria.add(Restrictions.or(
              Restrictions.ilike("endereco.bairro",busca),
              Restrictions.ilike("endereco.cidade", busca)
      ));

      List usuariosDoBairroBranco = criteria.list();
      System.out.println("USUARIOS NO BAIRRO '" + busca + "'");
      usuariosDoBairroBranco.forEach(System.out::println); //( (u) -> System.out.println(u) );

      /*
       * SELECT count(usua.id) as quantidade from usuario usua;
       */
      criteria = session.createCriteria(Usuario.class);
      criteria.createAlias("enderecos","endereco");
      criteria.add(Restrictions.or(
              Restrictions.ilike("endereco.bairro",busca),
              Restrictions.ilike("endereco.cidade", busca)
      ));
      criteria.setProjection(Projections.rowCount());
      System.out.println(String.format("Foram encontrados %s registro(s) com os criterios especificados",
              criteria.uniqueResult()));

      usuariosDoBairroBranco = (List) session.createQuery("" +
              " select u from Usuario u " +
              " join u.enderecos endereco " +
              " where lower(endereco.cidade) like '" + busca + "' " +
              " or lower(endereco.bairro) like '%branco%' " +
              "").list();

      usuariosDoBairroBranco.forEach(System.out::println);

      /*
       * SELECT count(usua.id) as quantidade from usuario usua;
       */
      Long count = (Long)session.createQuery("select count(" /*distinct*/ +"u.id) from Usuario u" +
              " join u.enderecos endereco " +
              " where lower(endereco.cidade) like '%branco%' " +
              " or lower(endereco.bairro) like '%branco%' " +
              "").uniqueResult();

      System.out.println(String.format("Foram encontrados %s registro(s) com os criterios especificados",
              count));

      Long sum = (Long)session.createQuery("select sum(" /*distinct*/ +"endereco.numero) from Usuario u" +
              " join u.enderecos endereco " +
              " where lower(endereco.cidade) like '%branco%' " +
              " or lower(endereco.bairro) like '%branco%' " +
              "").uniqueResult();

      System.out.println(String.format("A soma dos numeros dos enderecos é: %s",
              sum));



      transaction.commit();


    } catch (Exception ex) {
      if(transaction != null)transaction.rollback();
      LOG.log(Level.SEVERE, ex.getMessage(), ex);
      System.exit(1);
    } finally {
      if(session != null)session.close();
      System.exit(0);
    }
  }
}
