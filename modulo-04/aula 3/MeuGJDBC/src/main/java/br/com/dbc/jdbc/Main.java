/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gustavo.vidaletti
 */
public class Main {
    
    public static void main(String[] args) {
        Connection conn =  Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO'").executeQuery();
            
            if(!rs.next()){
                System.out.println("Criando Tabela USUARIO");
                conn.prepareStatement("CREATE TABLE USUARIO (\n" +
                    "    ID NUMBER NOT NULL PRIMARY KEY,\n" +
                    "    NOME VARCHAR(100) NOT NULL,\n" +
                    "    APELIDO VARCHAR(15) NOT NULL,\n" +
                    "    SENHA VARCHAR(15) NOT NULL,\n" +
                    "    CPF NUMBER(11) NOT NULL\n" +
                    ")").execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na criação da tabela usuario", ex);
        }
    }
    
}
