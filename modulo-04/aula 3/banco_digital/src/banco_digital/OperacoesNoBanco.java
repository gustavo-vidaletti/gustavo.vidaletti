package banco_digital;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OperacoesNoBanco {
    private static Connection conn = Connector.connect();
    
    public static void inserirBanco(String nome){
        try {
            PreparedStatement pst = conn.prepareStatement("insert into banco(id, nome)\n\t" +
                    "values (banco_seq.nextval, ? )");
            pst.setString(1, nome);
//            pst.setInt(2, 4);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OperacoesNoBanco.class.getName()).log(Level.SEVERE, "Erro de Operação", ex);
        }
    }
    
    public static void inserirNovaAgenciaNoBancoDeCodigo(int idBanco){
        try {
            PreparedStatement pst = conn.prepareStatement("insert into agencia(id, id_banco) \n\t" +
                "values(agencia_seq.nextval, ?)");
            pst.setInt(1, idBanco);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OperacoesNoBanco.class.getName()).log(Level.SEVERE, "Erro ao inserir Agencia", ex);
        }           
    }
    
    public static void inserirNovoCliente(String nome, Date dataNascimento, int cpf, int rg, int cnpj, String sexo ){
        try {
            PreparedStatement pst = conn.prepareStatement("insert into cliente( id, nome, data_nascimento, cpf, rg, cnpj, sexo )" +
                    "values(cliente_seq.nextval, ?, ?, ?, ?, ?, ?)");
            pst.setString(1, nome);
//            pst.setString(2, dataNascimento);
            pst.setDate(2, dataNascimento);
            pst.setInt(3, cpf);
            pst.setInt(4, rg);
            pst.setInt(5, cnpj);
            pst.setString(6, sexo);
            
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OperacoesNoBanco.class.getName()).log(Level.SEVERE, "Erro ao inserir Novo Cliente", ex);
        }         
    }
    
    public static void tipoDeConta(String nome, int qtdClientesNaConta){
        try {
            PreparedStatement pst = conn.prepareStatement("insert into tipo_conta( id, nome, qtd_clientes )" +
                    "values(tipo_conta_seq.nextval, ?, ?)");
            pst.setString(1, nome);
            pst.setInt(2, qtdClientesNaConta);
            
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OperacoesNoBanco.class.getName()).log(Level.SEVERE, "Erro ao inserir Tipo de Conta", ex);
        } 
    }
    
    public static void novaConta(int idAgencia, int idTipoConta, int saldo){
        try {
            PreparedStatement pst = conn.prepareStatement("insert into conta( id, id_agencia, id_tipo_conta, saldo)" +
                    "values(conta_seq.nextval, ?, ?, ?)");
            pst.setInt(1, idAgencia);
            pst.setInt(2, idTipoConta);
            pst.setInt(3, saldo);
            
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OperacoesNoBanco.class.getName()).log(Level.SEVERE, "Erro ao inserir Conta", ex);
        } 
    }
    
    public static void tipoMovimentacao(String nome, String descricao){
        try {
            PreparedStatement pst = conn.prepareStatement("insert into tipo_movimentacao( id, nome, descricao)" +
                    "values(tipo_movimentacao_seq.nextval, ?, ?)");
            pst.setString(1, nome);
            pst.setString(2, descricao);
            
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OperacoesNoBanco.class.getName()).log(Level.SEVERE, "Erro ao inserir Conta", ex);
        } 
    }
    
    public static void novaMovimentacao(int idTipoMovimentacao, int idContaOrigem, int idContaDestino, int valor, Date data){
        try {
            PreparedStatement pst = conn.prepareStatement("insert into movimentacao( id, id_tipo_movimentacao, id_conta_origem, id_conta_destino, valor, data_hora)" +
                    "values(movimentacao_seq.nextval, ?, ?, ?, ?, ?)");
            pst.setInt(1, idTipoMovimentacao);
            pst.setInt(2, idContaOrigem);
            pst.setInt(3, idContaDestino);
            pst.setInt(4, valor);
            pst.setDate(5, data);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OperacoesNoBanco.class.getName()).log(Level.SEVERE, "Erro ao inserir Conta", ex);
        } 
    }
    
    public static void insereClienteNaConta(int idCliente, int idConta){
        try {
            PreparedStatement pst = conn.prepareStatement("insert into conta_cliente( id, id_cliente, id_conta)" +
                    "values(conta_cliente_seq.nextval, ?, ?)");
            pst.setInt(1, idCliente);
            pst.setInt(2, idConta);
            
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OperacoesNoBanco.class.getName()).log(Level.SEVERE, "Erro ao inserir Conta", ex);
        } 
    }
}
