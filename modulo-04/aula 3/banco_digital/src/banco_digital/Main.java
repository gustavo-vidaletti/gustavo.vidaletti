package banco_digital;

import java.sql.Connection;
import java.sql.Date;


public class Main {
    public static void main(String[] args) {
        // TODO code application logic here
        // Connection conn = Connector.connect();
        OperacoesNoBanco.inserirBanco("Banco do Brasil");
        OperacoesNoBanco.inserirBanco("Itau");
        OperacoesNoBanco.inserirNovaAgenciaNoBancoDeCodigo(1);
        OperacoesNoBanco.inserirNovaAgenciaNoBancoDeCodigo(2);
        OperacoesNoBanco.inserirNovoCliente("Gustavo", new Date(1996, 9, 21), 00011122233, 1111222233, 0, "M");
        OperacoesNoBanco.inserirNovoCliente("Ronaldo", new Date(1990, 12, 14), 00011122234, 1111222234, 0, "M");
        OperacoesNoBanco.inserirNovoCliente("Alexandra", new Date(1992, 12, 15), 00011122235, 1111222235, 0, "F");
        OperacoesNoBanco.inserirNovoCliente("Carolina", new Date(1995, 8, 2), 00011122236, 1111222236, 0, "F");
        OperacoesNoBanco.tipoDeConta("Pessoa Fisica", 1);
        OperacoesNoBanco.tipoDeConta("Conjunta", 2);
        OperacoesNoBanco.novaConta(1, 1, 100);
        OperacoesNoBanco.novaConta(1, 1, 100);
        OperacoesNoBanco.novaConta(2, 1, 100);
        OperacoesNoBanco.novaConta(2, 1, 100);
        OperacoesNoBanco.insereClienteNaConta(1, 1);
        OperacoesNoBanco.insereClienteNaConta(2, 2);
        OperacoesNoBanco.insereClienteNaConta(3, 3);
        OperacoesNoBanco.insereClienteNaConta(4, 4);
    }
    
}
