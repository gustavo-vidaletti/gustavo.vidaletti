create sequence banco_seq
start with 1
increment by 1;

create sequence agencia_seq
start with 1
increment by 1;

create sequence tipo_usuario_seq
start with 1
increment by 1;

create sequence usuario_seq
start with 1
increment by 1;

create sequence tipo_conta_seq
start with 1
increment by 1;

create sequence conta_seq
start with 1
increment by 1;

create sequence credito_pre_aprovado_seq
start with 1
increment by 1;

create sequence emprestimo_seq
start with 1
increment by 1;

create sequence emprestimo_aprovado_seq
start with 1
increment by 1;

create sequence tipo_movimentacao_seq
start with 1
increment by 1;


create sequence movimentacao_seq
start with 1
increment by 1;

create sequence endereco_seq
start with 1
increment by 1;

create sequence cliente_seq
start with 1
increment by 1;

create sequence endereco_cliente_seq
start with 1
increment by 1;

create sequence tipo_contato_seq
start with 1
increment by 1;

create sequence contato_seq
start with 1
increment by 1;

create sequence conta_cliente_seq
start with 1
increment by 1;
