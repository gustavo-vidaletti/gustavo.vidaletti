create table banco(
    id number not null primary key,
    nome varchar(100) not null
);

create table agencia(
    id number not null primary key,
    id_banco number not null references banco(id)
);

create table tipo_usuario(
    id number not null primary key,
    cargo varchar(100) not null
);

create table usuario(
    id number not null primary key,
    nome varchar(100) not null,
    id_tipo_usuario number not null references tipo_usuario(id),
    id_banco number not null references banco(id)
);

create table tipo_conta(
    id number not null primary key,
    nome varchar(100) not null,
    qtd_clientes number not null
);

create table conta(
    id number not null primary key,
    id_agencia number not null references agencia(id),
    id_tipo_conta number not null references tipo_conta(id),
    saldo number default 0 not null
);

create table credito_pre_aprovado(
    id number not null primary key,
    id_conta number not null references conta(id),
    valor number not null
);

create table emprestimo(
    id number not null primary key,
    id_gerente number references usuario(id),
    id_gerente_geral number references usuario(id),
    id_conta number not null references conta(id),
    valor number not null
);

create table emprestimo_aprovado(
    id number not null primary key,
    id_conta number not null references conta(id),
    id_emprestimo number not null references emprestimo(id)
);

create table tipo_movimentacao(
    id number not null primary key,
    nome varchar(100) not null,
    descricao varchar(100) not null
);

create table movimentacao(
    id number not null primary key,
    id_tipo_movimentacao number not null references tipo_movimentacao(id),
    id_conta_origem number references conta(id),
    id_conta_destino number references conta(id),
    valor number default 0 not null,
    data_hora date not null
);

create table endereco(
    id number not null primary key,
    logradouro varchar(100) not null,
    numero number not null,
    complemento varchar(50),
    bairro varchar(50),
    cidade varchar(100)
);

create table cliente(
    id number not null primary key,
    nome varchar(100) not null,
    data_nascimento date,
    cpf number(11),
    rg number(10),
    cnpj number(14),
    sexo char(1)
);

create table conta_cliente(
	id number not null primary key,
	id_conta number not null references conta(id),
	id_cliente number not null references cliente(id)
);

create table endereco_cliente(
    id number not null primary key,
    id_endereco number references endereco(id),
    id_cliente number references cliente(id)
);

create table tipo_contato(
    id number not null primary key,
    nome varchar(100) not null,
    qtd_maxima number default 1 not null
);

create table contato(
    id number not null primary key,
    id_cliente number not null references cliente(id),
    id_tipo_contato number not null references tipo_contato(id),
    valor varchar(100) not null
);