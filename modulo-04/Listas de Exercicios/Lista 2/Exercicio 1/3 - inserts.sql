-- INSERTS
insert into usuario( id, nome, apelido, senha, cpf )
    values ( usuario_seq.nextval, 'Gustavo', 'gusta', '1234', 11122233344 );
insert into usuario( id, nome, apelido, senha, cpf )
    values ( usuario_seq.nextval, 'Ana Cristina', 'tina', '4321', 00011122233 );
insert into usuario( id, nome, apelido, senha, cpf )
    values ( usuario_seq.nextval, 'Carolina', 'carol', '0011', 01010101010 );
insert into usuario( id, nome, apelido, senha, cpf )
    values ( usuario_seq.nextval, 'Antonio', 'tonho', '0012', 01010101011 );

insert into tipo_contato ( id, nome, quantidade )
    values ( tipo_contato_seq.nextval, 'email', 2 );
insert into tipo_contato ( id, nome, quantidade )
    values ( tipo_contato_seq.nextval, 'telefone', 2 );
insert into tipo_contato ( id, nome, quantidade )
    values ( tipo_contato_seq.nextval, 'whatsapp', 2 );
insert into tipo_contato ( id, nome, quantidade )
    values ( tipo_contato_seq.nextval, 'nao vou usar', 4 );

insert into contato ( id, id_usuario, id_tipo_contato, valor)
    values( contato_seq.nextval, 1, 1, 'gustavo@email.com' );
insert into contato ( id, id_usuario, id_tipo_contato, valor)
    values( contato_seq.nextval, 2, 1, 'tina@email.com' );
insert into contato ( id, id_usuario, id_tipo_contato, valor)
    values( contato_seq.nextval, 3, 1, 'carol@email.com' );

insert into endereco ( id, logradouro, numero, complemento, bairro, cidade )
    values( endereco_seq.nextval, 'rua liberdade', 433, 'apto 702', 'Rio Branco', 'Porto Alegre' );
insert into endereco ( id, logradouro, numero, complemento, bairro, cidade )
    values( endereco_seq.nextval, 'rua Cabral', 200, 'apto 201', 'Rio Branco', 'Porto Alegre' );
insert into endereco ( id, logradouro, numero, bairro, cidade )
    values( endereco_seq.nextval, 'rua Casemiro de Abreu', 102, 'Rio Branco', 'Porto Alegre' );
insert into endereco ( id, logradouro, numero, bairro, cidade )
    values( endereco_seq.nextval, 'av goethe', 102, 'Rio Branco', 'Porto Alegre' );

insert into usuario_endereco ( id, id_usuario, id_endereco )
    values( usuario_endereco_seq.nextval, 1, 1 );
insert into usuario_endereco ( id, id_usuario, id_endereco )
    values( usuario_endereco_seq.nextval, 3, 3 );
insert into usuario_endereco ( id, id_usuario, id_endereco )
    values( usuario_endereco_seq.nextval, 3, 3 );
insert into usuario_endereco ( id, id_usuario, id_endereco )
    values( usuario_endereco_seq.nextval, 3, 4 );

insert into cartao_credito ( id, id_usuario, bandeira, numero, nome_escrito, codigo_seguranca, validade )
    values( cartao_credito_seq.nextval, 1, 'visa', 4949484847474646, 'Gustavo', 121, 2210 );
insert into cartao_credito ( id, id_usuario, bandeira, numero, nome_escrito, codigo_seguranca, validade )
    values( cartao_credito_seq.nextval, 2, 'master', 4949484847474644, 'Ana Cristina', 122, 2210 );
insert into cartao_credito ( id, id_usuario, bandeira, numero, nome_escrito, codigo_seguranca, validade )
    values( cartao_credito_seq.nextval, 3, 'visa', 4949484847474645, 'Carolina', 123, 2410 );

insert into item ( id, descricao, qtd_maxima )
    values ( item_seq.nextval, 'flecha', 1000 );
insert into item ( id, descricao, qtd_maxima )
    values ( item_seq.nextval, 'arco', 5 );
insert into item ( id, descricao, qtd_maxima )
    values ( item_seq.nextval, 'flecha de vidro', 1000 );
insert into item ( id, descricao, qtd_maxima )
    values ( item_seq.nextval, 'arco de vidro', 2 );
insert into item ( id, descricao, qtd_maxima )
    values ( item_seq.nextval, 'espada', 1 );
insert into item ( id, descricao, qtd_maxima )
    values ( item_seq.nextval, 'Arco de Aço', 1 );

insert into inventario ( id, qtd_maxima )
    values ( inventario_seq.nextval, 1500 );
insert into inventario ( id, qtd_maxima )
    values ( inventario_seq.nextval, 2000 );
insert into inventario ( id, qtd_maxima )
    values ( inventario_seq.nextval, 2500 );
insert into inventario ( id, qtd_maxima )
    values ( inventario_seq.nextval, 3000 );

insert into material ( id, nome, unidade )
    values ( material_seq.nextval, 'madeira', 3 );
insert into material ( id, nome, unidade )
    values ( material_seq.nextval, 'ferro', 2 );
insert into material ( id, nome, unidade )
    values ( material_seq.nextval, 'cipó', 1 );
insert into material ( id, nome, unidade )
    values ( material_seq.nextval, 'Aço', 1 );

insert into material_item ( id, id_material, id_item, qtd_minima )
    values ( material_item_seq.nextval, 1, 1, 4 );
insert into material_item ( id, id_material, id_item, qtd_minima )
    values ( material_item_seq.nextval, 2, 1, 2 );
insert into material_item ( id, id_material, id_item, qtd_minima )
    values ( material_item_seq.nextval, 3, 2, 1 );
insert into material_item ( id, id_material, id_item, qtd_minima )
    values ( material_item_seq.nextval, 1, 2, 3 );
insert into material_item ( id, id_material, id_item, qtd_minima )
    values ( material_item_seq.nextval, 2, 2, 1 );
insert into material_item ( id, id_material, id_item, qtd_minima )
    values ( material_item_seq.nextval, 4, 4, 1 );


insert into tipo_raca ( id, nome, bonus_vida, bonus_dano )
    values ( tipo_raca_seq.nextval, 'verde', '0', '0' );
insert into tipo_raca ( id, nome, bonus_vida, bonus_dano )
    values ( tipo_raca_seq.nextval, 'noturno', '0', '15' );
insert into tipo_raca ( id, nome, bonus_vida, bonus_dano )
    values ( tipo_raca_seq.nextval, 'da luz', '15', '30' );
insert into tipo_raca ( id, nome, bonus_vida, bonus_dano )
    values ( tipo_raca_seq.nextval, 'da manhã', '15', '30' );

insert into tipo_raca_item ( id, id_tipo_raca, id_item )
    values ( tipo_raca_item_seq.nextval, 1, 1);
insert into tipo_raca_item ( id, id_tipo_raca, id_item )
    values ( tipo_raca_item_seq.nextval, 1, 2);
insert into tipo_raca_item ( id, id_tipo_raca, id_item )
    values ( tipo_raca_item_seq.nextval, 2, 3 );
insert into tipo_raca_item ( id, id_tipo_raca, id_item )
    values ( tipo_raca_item_seq.nextval, 2, 4 );
insert into tipo_raca_item ( id, id_tipo_raca, id_item )
    values ( tipo_raca_item_seq.nextval, 3, 5 );


insert into raca ( id, nome, id_tipo_raca)
    values ( raca_seq.nextval, 'Elfo Verde', 1);
insert into raca ( id, nome, id_tipo_raca)
    values ( raca_seq.nextval, 'Elfo Noturno', 2);
insert into raca ( id, nome, id_tipo_raca)
    values ( raca_seq.nextval, 'Elfo Da Luz', 3);
insert into raca ( id, nome, id_tipo_raca)
    values ( raca_seq.nextval, 'Elfo Da Manhã', 4);

insert into status( id , nome )
    values (status_seq.nextval, 'RECEM CRIADO');
insert into status( id , nome )
    values (status_seq.nextval, 'SOFREU DANO');
insert into status( id , nome )
    values (status_seq.nextval, 'MORTO');
insert into status( id , nome )
    values (status_seq.nextval, 'NAO SEI');

insert into personagem (id, id_usuario, id_status, id_raca, id_inventario, nome)
    values(personagem_seq.nextval, 1, 1, 1, 1, 'guvida');
insert into personagem (id, id_usuario, id_status, id_raca, id_inventario, nome)
    values(personagem_seq.nextval, 2, 1, 2, 2, 'tinalodi');
insert into personagem (id, id_usuario, id_status, id_raca, id_inventario, nome)
    values(personagem_seq.nextval, 3, 1, 3, 3, 'loi');