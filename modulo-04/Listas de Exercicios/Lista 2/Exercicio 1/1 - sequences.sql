-- SEQUENCES
create sequence usuario_seq
start with 1
increment by 1;

create sequence tipo_contato_seq
start with 1
increment by 1;

create sequence contato_seq
start with 1
increment by 1;

create sequence endereco_seq
start with 1
increment by 1;

create sequence usuario_endereco_seq
start with 1
increment by 1;

create sequence cartao_credito_seq
start with 1
increment by 1;

create sequence item_seq
start with 1
increment by 1;

create sequence inventario_seq
start with 1
increment by 1;

create sequence item_inventario_seq
start with 1
increment by 1;

create sequence material_seq
start with 1
increment by 1;

create sequence material_item_seq
start with 1
increment by 1;

create sequence tipo_raca_seq
start with 1
increment by 1;

create sequence tipo_raca_item_seq
start with 1
increment by 1;

create sequence raca_seq
start with 1
increment by 1;

create sequence status_seq
start with 1
increment by 1;

create sequence personagem_seq
start with 1
increment by 1;
