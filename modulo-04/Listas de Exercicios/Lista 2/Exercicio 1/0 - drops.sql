drop sequence usuario_seq;
drop sequence tipo_contato_seq;
drop sequence contato_seq;
drop sequence endereco_seq;
drop sequence usuario_endereco_seq;
drop sequence cartao_credito_seq;
drop sequence item_seq;
drop sequence inventario_seq;
drop sequence item_inventario_seq;
drop sequence material_seq;
drop sequence material_item_seq;
drop sequence tipo_raca_seq;
drop sequence tipo_raca_item_seq;
drop sequence raca_seq;
drop sequence status_seq;
drop sequence personagem_seq;

select * from user_sequences;

-- TABLES

drop table usuario cascade constraints;
drop table tipo_contato cascade constraints;
drop table contato cascade constraints;
drop table endereco cascade constraints;
drop table usuario_endereco cascade constraints;
drop table cartao_credito cascade constraints;
drop table item cascade constraints;
drop table inventario cascade constraints;
drop table item_inventario cascade constraints;
drop table material cascade constraints;
drop table material_item cascade constraints;
drop table tipo_raca cascade constraints;
drop table tipo_raca_item cascade constraints;
drop table raca cascade constraints;
drop table status cascade constraints;
drop table personagem cascade constraints;