-- TABELAS
create table usuario (
    id number not null primary key,
    nome varchar(100) not null,
    apelido varchar(15) not null,
    senha varchar(15) not null,
    cpf number(11) not null
);

create table tipo_contato (
    id number not null primary key,
    nome varchar(100) default '' not null,
    quantidade number default 1 not null
    );

create table contato (
    id number not null primary key,
    id_usuario number not null references usuario(id) on delete cascade,
    id_tipo_contato number not null references tipo_contato(id) on delete cascade,
    valor varchar(100) not null
);

create table endereco (
    id number not null primary key,
    logradouro varchar(100) not null,
    numero number not null,
    complemento varchar(100) default 'casa',
    bairro varchar(100) not null,
    cidade varchar(100) not null
);

create table usuario_endereco (
    id number not null primary key,
    id_usuario number not null references usuario(id) on delete cascade,
    id_endereco number not null references endereco(id) on delete cascade
);

create table cartao_credito (
    id number not null primary key,
    id_usuario number not null references usuario(id) on delete cascade,
    bandeira varchar(50) not null,
    numero number(16) not null,
    nome_escrito varchar(100),
    codigo_seguranca number(3) not null,
    validade number(4) not null
);

create table item (
    id number not null primary key,
    descricao varchar(100) not null,
    qtd_maxima number default 1 not null
);

create table inventario (
    id number not null primary key,
    qtd_maxima number default 2 not null
);

create table item_inventario (
    id number not null primary key,
    id_inventario number not null references inventario(id) on delete cascade,
    id_item number not null references item(id) on delete cascade,
    quantidade number default 1 not null
);

create table material (
    id number not null primary key,
    nome varchar(100) not null,
    unidade number not null
);

create table material_item (
    id number not null primary key,
    id_material number not null references material(id) on delete cascade,
    id_item number not null references item(id) on delete cascade,
    qtd_minima number default 1 not null

);

create table tipo_raca (
    id number not null primary key,
    nome varchar(100) not null,
    bonus_vida number default 0 not null,
    bonus_dano number default 0 not null
);

create table tipo_raca_item (
    id number not null primary key,
    id_tipo_raca number not null references tipo_raca(id) on delete cascade,
    id_item number not null references item(id) on delete cascade
);

create table raca (
    id number not null primary key,
    nome varchar(100) not null,
    limitador number default 1 not null,
    vida_inicio number default 100 not null,
    dano_inicio number default 0 not null,
    id_tipo_raca number not null references tipo_raca(id) on delete cascade
);

create table status (
    id number not null primary key,
    nome varchar(100) not null
);

create table personagem (
    id number not null primary key,
    id_usuario number not null references usuario(id) on delete cascade,
    id_status number not null references status(id) on delete cascade,
    id_raca number not null references raca(id) on delete cascade,
    id_inventario number not null references inventario(id) on delete cascade,
    nome varchar(100) not null,
    vida number default 100 not null,
    dano number default 0 not null,
    xp number default 0 not null
);
