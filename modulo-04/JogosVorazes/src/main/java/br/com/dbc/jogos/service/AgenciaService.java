package br.com.dbc.jogos.service;

import br.com.dbc.jogos.dao.AbstractDAO;
import br.com.dbc.jogos.dao.AgenciaDAO;
import br.com.dbc.jogos.entity.Agencia;
import br.com.dbc.jogos.entity.HibernateUtil;
import org.hibernate.Session;

public class AgenciaService {
  
  protected Class<Agencia> getEntityClass(){ return Agencia.class; }
  
  private static final AgenciaDAO AGENCIA_DAO = new AgenciaDAO();
  
  public Agencia buscarPorId(Integer id){
//    return super.findById(id);
    return AGENCIA_DAO.buscar(id);
  }

}
