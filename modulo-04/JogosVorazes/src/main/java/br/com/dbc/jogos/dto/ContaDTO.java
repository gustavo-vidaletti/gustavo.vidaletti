package br.com.dbc.jogos.dto;

import br.com.dbc.jogos.entity.Cliente;

public class ContaDTO {
  
  private Integer idConta;
  private String bancoConta;
  private String agenciaConta;
  private String numeroConta;
  private ClienteDTO clienteConta;
  
  public Integer getIdConta() {
    return idConta;
  }
  
  public void setIdConta(Integer idConta) {
    this.idConta = idConta;
  }
  
  public String getBancoConta() {
    return bancoConta;
  }
  
  public void setBancoConta(String bancoConta) {
    this.bancoConta = bancoConta;
  }
  
  public String getAgenciaConta() {
    return agenciaConta;
  }
  
  public void setAgenciaConta(String agenciaConta) {
    this.agenciaConta = agenciaConta;
  }
  
  public String getNumeroConta() {
    return numeroConta;
  }
  
  public void setNumeroConta(String numeroConta) {
    this.numeroConta = numeroConta;
  }
  
  public ClienteDTO getClienteConta() {
    return clienteConta;
  }
  
  public void setClienteConta(ClienteDTO clienteConta) {
    this.clienteConta = clienteConta;
  }
}
