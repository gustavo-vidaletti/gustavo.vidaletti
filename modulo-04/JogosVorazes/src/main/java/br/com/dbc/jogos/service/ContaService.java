package br.com.dbc.jogos.service;

import br.com.dbc.jogos.dao.ClienteDAO;
import br.com.dbc.jogos.dao.ContaDAO;
import br.com.dbc.jogos.dto.ContaDTO;
import br.com.dbc.jogos.entity.Conta;

public class ContaService {
  
  private static final ContaDAO CONTA_DAO = new ContaDAO();
  public void deletarConta(ContaDTO dto){
    CONTA_DAO.delete(dto.getIdConta());
  }
  
  public void salvarConta (ContaDTO dto){
    Conta conta = CONTA_DAO.from(dto);
    CONTA_DAO.saveOrUpdate(conta);
    dto.setIdConta(conta.getId());
  }
}
