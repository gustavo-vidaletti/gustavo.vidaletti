/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.service;

import br.com.dbc.jogos.dao.EnderecoDAO;
import br.com.dbc.jogos.dto.EnderecoDTO;
import br.com.dbc.jogos.entity.Cliente;
import br.com.dbc.jogos.entity.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Hibernate;
import org.hibernate.Session;

/**
 *
 * @author tiago
 */
public class EnderecoService {

    private static final Logger LOG = Logger.getLogger(EnderecoService.class.getName());
    public static final EnderecoDAO ENDERECO_DAO = new EnderecoDAO();

    public void salvarEndereco(EnderecoDTO dto, Cliente cliente) {
        Session session = HibernateUtil.getSession(true);
        try {
            ENDERECO_DAO.saveOrUpdate(ENDERECO_DAO.from(dto, cliente));
            HibernateUtil.commitTransaction();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            session.getTransaction().rollback();
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    public void deletarPorId(Integer id){
        Session session = HibernateUtil.getSession(true);
        ENDERECO_DAO.delete(id);
    }

}
