/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.entity;

import javax.persistence.*;

/**
 *
 * @author tiago
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Conta extends BaseEntity {
    
    @Id
    @SequenceGenerator(allocationSize = 1, sequenceName = "CONTA_SEQ", name = "CONTA_SEQ")
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(nullable = false, length = 50)
    private String banco;
    
    @Column(nullable = false, length = 10)
    private String agencia;
    
    @Column(nullable = false, length = 10)
    private String numero;
    
    @OneToOne(mappedBy = "conta")
    @JoinColumn(name = "id_cliente", nullable = false)
    private Cliente cliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    
    
}
