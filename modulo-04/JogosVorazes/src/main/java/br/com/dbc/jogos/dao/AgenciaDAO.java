package br.com.dbc.jogos.dao;

import br.com.dbc.jogos.entity.Agencia;


public class AgenciaDAO extends AbstractDAO<Agencia> {
  
  protected Class<Agencia> getEntityClass() {
    return Agencia.class;
  }
  
  public Agencia buscar(Integer id){
    return super.findById(id);
  }
}
