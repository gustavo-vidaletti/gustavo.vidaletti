package br.com.dbc.jogos.dao;

import br.com.dbc.jogos.dto.ContaDTO;
import br.com.dbc.jogos.entity.Cliente;
import br.com.dbc.jogos.entity.Conta;

import java.util.Optional;

public class ContaDAO extends AbstractDAO<Conta> {
  protected static final ClienteDAO CLIENTE_DAO = new ClienteDAO();
  protected Class<Conta> getEntityClass() {
    return Conta.class;
  }
  
  public Conta from(ContaDTO dto){
    Conta c = Optional
            .ofNullable(findById(dto.getIdConta()))
            .orElseGet(() -> new Conta());
    c.setAgencia(dto.getAgenciaConta());
    c.setBanco(dto.getBancoConta());
    c.setCliente(CLIENTE_DAO.from(dto.getClienteConta()));
    c.setNumero(dto.getNumeroConta());
    
    return c;
  }
}
