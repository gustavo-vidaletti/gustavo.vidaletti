package br.com.dbc.jogos.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

public class Agencia extends BaseEntity{
  
  @Id
  @SequenceGenerator(allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")
  @GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
  private Integer id;
  
  //Getters and Setters
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
}
