/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.dao;

import br.com.dbc.jogos.dto.EnderecoDTO;
import br.com.dbc.jogos.entity.Cliente;
import br.com.dbc.jogos.entity.Endereco;
import java.util.Optional;

/**
 *
 * @author tiago
 */
public class EnderecoDAO extends AbstractDAO<Endereco> {

    @Override
    protected Class<Endereco> getEntityClass() {
        return Endereco.class;
    }

    public Endereco from(EnderecoDTO dto, Cliente c) {
        Integer idEndereco = dto.getIdEndereco();
        Endereco e = c.getEnderecos()
                .stream()
                .filter(ue -> ue.getId().equals(idEndereco))
                .findFirst()
                .orElseGet(() -> Optional
                .ofNullable(findById(idEndereco))
                .orElse(new Endereco()));
//        if(dto.getIdEndereco() != null) e.setId(dto.getIdEndereco());
        e.setLogradouro(dto.getLogradouroEndereco());
        e.setNumero(dto.getNumeroEndereco());
        e.setBairro(dto.getBairroEndereco());
        e.setCidade(dto.getCidadeEndereco());
        e.setCliente(c);
        return e;
    }

}
