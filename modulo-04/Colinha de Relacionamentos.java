//Entidade:
	@Entity
	@Table(name = "TIPO_CONTATO") //se necessario for
	@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TipoContato.class)
	//@Inheritance(strategy = InheritanceType.JOINED) SE ELA FOR EXTENDIDA
	//@PrimaryKeyJoinColumn(name = "id_endereco") SE ELA EXTENDER ALGO
	public class TipoContato {}

//Id:
	@Id
	@SequenceGenerator(allocationSize = 1, name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
	
//One to one:
	//Lado mais forte:
		@OneToOne(cascade = CascadeType.MERGE)
		@JoinColumn(name = "id_tipo_contato")
		private TipoContato tipoContato;
	
	//Lado mais fraco:
		@OneToOne(mappedBy = "tipoContato")
		//@JoinColumn(name = "...") -> se precisar estar no outro tb
		private Contato contato;
		
		
//Many to one:
	@ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
	
	
//One to many:
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.MERGE)
    private List<Contato> contatos = new ArrayList<Contato>();


//Many to many:
	//LADO QUE TIVER QUE ADICIONAR COM AQUILO
		@ManyToMany(cascade = CascadeType.MERGE)
		@JoinTable( name = "usuario_endereco", 
            joinColumns = {
                @JoinColumn( name = "id_usuario" ) },
            inverseJoinColumns = {
                @JoinColumn( name = "id_endereco") }    
    )
    private List<Endereco> enderecos = new ArrayList<Endereco>();
	
	//Lado mais fraco:
		@ManyToMany(mappedBy = "enderecos")
		private List<Usuario> usuarios = new ArrayList<Usuario>();
	
	
//Heranca - JOIN
	//PAI
		@Entity
		@Inheritance(strategy = InheritanceType.JOINED)
		public abstract class Endereco {}

	//FILHO
		@Entity
		@Table(name = "endereco_comercial")
		@PrimaryKeyJoinColumn(name = "id_endereco")
		public class EnderecoComercial extends Endereco {}