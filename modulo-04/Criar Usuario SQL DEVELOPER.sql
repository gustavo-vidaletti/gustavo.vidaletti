alter session set "_ORACLE_SCRIPT"=true;

CREATE USER lotr
IDENTIFIED BY lotr;

GRANT create session,create table,create view TO lotr WITH ADMIN OPTION;

SELECT USERNAME FROM DBA_USERS;